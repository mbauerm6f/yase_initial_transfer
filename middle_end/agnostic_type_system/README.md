# The Agnostic Type System (ATS) package of the YASE framework
## About ATS:

This package contains a simulator agnostic type wrapper for compiler front ends.
It allows to couple native C++ simulation structures directly with a differently structured scenario domain model via callback functions. 
This type wrappers contain type information, which enables a static type system. 
It allows to ensure type safe operations, expressions, type signatures and declarations.
Similar to C++ compile time, these checks are performed while parsing a scenario file format.

The type system is orientated on the C++ and Java type system.
Furthermore additional features (such as subtyping and actors) are included due to OpenSCENARIO2.0 requirements.
## ATS Architecture:

### Supported Types:

Currently the ATS package can support the following type instances:

```
BaseType
  ├─ Primitive
  │  ├─ Bool
  │  ├─ Integer
  │  ├─ Double
  │  └─ String
  └─ UserDefined
     ├─ Enum
     └─ Compound
        ├─ Struct
        └─ Actor
``` 

These types have the following features:
* Each type is inherited from the BaseType.
* Each Type instance must have a clone() method to clone/copy this particular instance.
* All Primitive types and the Enum are a wrapper with callbacks to interfere with a underlying arbitrary simulator.
* UserDefined types (Enum, Struct and Actor) are user defined and must have a unique name (Example: struct MyStruct).
* Compound types can contain nested fields (Example: vehicle.m_id).
* Actors are a special compound type, which gives additional possibilities to interact with the underlying simulator. 

Besides these types, lists and dictionaries are also supported. 
However currently they are not integrated fully in the type system yet. 

### Types as wrapper

The following arbitrary example shows how an integer type is wrapped by the type system. 
The primitive type ```yase::Integer``` requires a getter function, setter function and a clone function. 
The ```yase::Integer``` wrapper now allows to use it in other contexts and type safe operations and expressions can be constructed.

``` C++
// A std::function in which a time (as int) from the underlying simulator is accessed 
yase::Integer::GetFunction demo_sim_time_getter = [world, time_offset]() -> int
{ return world->getSimTime().count()/1000 + time_offset; };

// A std::function in which a time (as int) from the underlying simulator is manipulated
yase::Integer::SetFunction demo_sim_time_setter = [world, time_offset](int new_time) -> void
{ world->getSimTime(new_time*1000 = offset);};

// A std::function which defines what happens if there should be a clone ( = new instance)
// In this example this should be prohibited, as for this example one could say there is only one sim time
yase::Integer::CloneFunction demo_sim_time_cloner = []() -> yase::BaseType::Ptr
{ throw std::runtime_error("Direct clone of demo_sim_time prohibited"); };

// The Integer wrapper consists of all three std::functions
Integer::Ptr demo_sim_time_wrapper = std::make_shared<yase::Integer>(demo_sim_time_getter, demo_sim_time_setter, demo_sim_time_cloner);

```

### Operation

As the ATS type system is a static type system, it is be possible to resolve and check all operations while compiling (= parsing) the scenario file. 
This is done with the help of atomic operations. 
When possible, these operations are orientated on the equivalent C++ operators.
The operations check while compiling for type correctness and return the proper operator function.
As the type safety can be checked while scenario compile time, the functions can be called with minor overhead directly while scenario runtime without further checks.

``` C++
// ### While scenario parsing ###
// Definition of demo operations: Checks type correctness and creates a function to call to due the calculation later
AssignFunction assign_fnc = assignmentOperation(double_1, AssignmentOperator::kDirectAssignment, double_2);
RelationalFunction compare_fnc = relationalOperation(double_1, RelationalOperator::kUnEqual, double_2);

// ### During scenario execution ###
// Example: While runtime these functions can be called and they are evaluated with the current values
// Assigns it at this moment with current values (directly without any type checks, as checked before)
assign_fnc();
// Compares it at this moment with current values (directly without any type checks, as checked before)
if(compare_fnc()) {
   do_something();
}
```
For detailed examples please refer to the unit tests. 

### Expression:

An expression is a sequence of trailing operations, but It respects the order of operations rules.

For detailed examples please refer to the unit tests. 

### TypeTrait

To check type traits, several type trait checking functions exist in the file "type_trait.h".
When possible, these functions are orientated on the equivalent C++ type trait functions.
``` C++
bool isConvertible(const BaseType::Ptr &from, const BaseType::Ptr &to);
```
For detailed examples please refer to the unit tests. 


### Cast
As most types are usually stored as a generic BaseType::Ptr, several cast methods with proper error messaging exist in "cast.h".
``` C++
// Example: Cast a type to a  yase::Integer::Ptr or directly to an C++ int.
int castToCppInteger(const BaseType::Ptr type);
Integer::Ptr castToInteger(const BaseType::Ptr type);
```
For detailed examples please refer to the unit tests. 

### Declaration
For UserDefined types (such as a struct) there exists a Declaration class, which allows to instantiate UserDefined types properly with or without a constructor. 

For detailed examples please refer to the unit tests. 


### TypeSignatures:
For functions and methods there exists a type signature class, which ensures the proper mapping and checking of input arguments. 

For detailed examples please refer to the unit tests. 

## Build and run tests

### Dependencies:
* cmake
* gtest

### How to build:

Create a subfolder "build" and build it:  
``` shell
mkdir build && cd build
cmake ../your/path/to/agnostic_behavior_tree && make && ./agnostic_behavior_tree_test
```

## Possible next enhancements:

YASE is currently under development and will be extended. The following list contains several improvements, which may come next:

Minors: 
* Extending compound data types via composition
* Add Unsigned Integer to types

Medium:
* Add units system as separate type to ATS
* Add Actions 
* Add actions to actors (requires ABT as dependency)
* Allow constraints for any type


In general it is planned to adapt YASE to be (partly) compatible with OpenSCENARIO 2.0. This is under the prerequisite that OpenSCENARIO 2.0 will support holistic scenario modelling in a concrete way besides constraint based scenario generation.