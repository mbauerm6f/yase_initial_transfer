/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include <agnostic_type_system/expression.h>

#include <agnostic_type_system/cast.h>

#include "gtest/gtest.h"

namespace yase
{
    TEST(ExpressionTest, double_arithmetic_operator)
    {
        // Some start values
        yase::Double::Ptr double_1 = std::make_shared<yase::DoubleValue>(0.0);
        yase::Double::Ptr double_2 = std::make_shared<yase::DoubleValue>(1.0);
        yase::BaseType::Ptr int_3 = std::make_shared<yase::IntegerValue>(2);
        yase::Double::Ptr double_4 = std::make_shared<yase::DoubleValue>(3.0);
        yase::BaseType::Ptr int_5 = std::make_shared<yase::IntegerValue>(4);

        // Define arithmetic operations
        yase::ArithmeticExpression expression_1(double_1);
        expression_1.append(yase::ArithmeticOperator::kAddition, double_2)
            .append(yase::ArithmeticOperator::kAddition, int_3)
            .append(yase::ArithmeticOperator::kAddition, double_4)
            .append(yase::ArithmeticOperator::kAddition, int_5);
        yase::BaseType::Ptr r_value_1 = expression_1.prepareArithmeticFunction();

        yase::ArithmeticExpression expression_2(double_1);
        expression_2.append(yase::ArithmeticOperator::kAddition, double_2)
            .append(yase::ArithmeticOperator::kSubtraction, int_3)
            .append(yase::ArithmeticOperator::kMultiplication, double_4)
            .append(yase::ArithmeticOperator::kDivision, int_5);
        yase::BaseType::Ptr r_value_2 = expression_2.prepareArithmeticFunction();

        yase::ArithmeticExpression expression_3(double_1);
        expression_3.append(yase::ArithmeticOperator::kAddition, double_2)
            .append(yase::ArithmeticOperator::kMultiplication, int_3)
            .append(yase::ArithmeticOperator::kAddition, double_4)
            .append(yase::ArithmeticOperator::kDivision, int_5);
        yase::BaseType::Ptr r_value_3 = expression_3.prepareArithmeticFunction();

        // Change the values after expressions were defined
        double_1->setFunction()(7.07);
        double_2->setFunction()(13.37);
        yase::castToInteger(int_3)->setFunction()(2);
        double_4->setFunction()(1.23);
        yase::castToInteger(int_5)->setFunction()(5);

        // Test simple addition (two times to see if multiple calls have an effect)
        EXPECT_DOUBLE_EQ(yase::castToCppDouble(r_value_1), 7.07 + 13.37 + 2 + 1.23 + 5);
        EXPECT_DOUBLE_EQ(yase::castToCppDouble(r_value_1), 7.07 + 13.37 + 2 + 1.23 + 5);

        // Test mixed expression
        EXPECT_DOUBLE_EQ(yase::castToCppDouble(r_value_2), 7.07 + 13.37 - 2 * 1.23 / 5);
        EXPECT_DOUBLE_EQ(yase::castToCppDouble(r_value_3), 7.07 + 13.37 * 2 + 1.23 / 5);

        // Operands itself are not allowed to change
        EXPECT_DOUBLE_EQ(double_1->getFunction()(), 7.07);
        EXPECT_DOUBLE_EQ(double_2->getFunction()(), 13.37);
        EXPECT_DOUBLE_EQ(yase::castToCppInteger(int_3), 2);
        EXPECT_DOUBLE_EQ(double_4->getFunction()(), 1.23);
        EXPECT_DOUBLE_EQ(yase::castToCppInteger(int_5), 5);
    }

} // namespace yase
