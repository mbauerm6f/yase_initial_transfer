/*******************************************************************************
* Copyright (c) Max Paul Bauer - XC/AD-EYU1 - Robert Bosch GmbH
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
*******************************************************************************/

#include <agnostic_type_system/type_trait.h>

#include <utility>

#include <gtest/gtest.h>

#include <agnostic_type_system/cast.h>

namespace yase
{
    class TypeTraitTest : public testing::Test
    {
    protected:
        yase::BaseType::Ptr createTestVehicleColorEnum()
        {
            yase::Enum::List vehicle_colors = {"Red", "Green", "Blue"};
            return std::make_shared<yase::UnsyncedEnum>("VehicleColor", vehicle_colors);
        };

        yase::BaseType::Ptr createTestLaneStruct()
        {
            yase::TypeDictionary::Map struct_field_map;
            struct_field_map.insert(std::make_pair("id", std::make_shared<yase::IntegerValue>(0)));
            struct_field_map.insert(std::make_pair("s", std::make_shared<yase::DoubleValue>(0.0)));
            struct_field_map.insert(std::make_pair("t", std::make_shared<yase::DoubleValue>(0.0)));
            return std::make_shared<yase::Struct>("LaneAssignment", struct_field_map);
        };

        yase::BaseType::Ptr createTestVehicleStruct()
        {
            yase::TypeDictionary::Map struct_field_map;
            struct_field_map.insert(std::make_pair("velocity", std::make_shared<yase::DoubleValue>(1.337)));
            struct_field_map.insert(std::make_pair("id", std::make_shared<yase::IntegerValue>(1)));
            struct_field_map.insert(std::make_pair("color", createTestVehicleColorEnum()));
            struct_field_map.insert(std::make_pair("lane", createTestLaneStruct()));

            return std::make_shared<yase::Struct>("Vehicle", struct_field_map);
        };

        yase::BaseType::Ptr createTestInheritedVehicleStruct()
        {
            yase::TypeDictionary::Map struct_field_map;
            struct_field_map.insert(std::make_pair("siren", std::make_shared<yase::BoolValue>(false)));
            struct_field_map.insert(std::make_pair("emergency_lights", std::make_shared<yase::BoolValue>(true)));

            return std::make_shared<yase::Struct>("EmergencyVehicle", struct_field_map, castToStruct(createTestVehicleStruct()));
        };
    };

    TEST_F(TypeTraitTest, subtype_check_for_Primitives)
    {
        yase::BaseType::Ptr test_bool = std::make_shared<yase::BoolValue>(true);
        yase::BaseType::Ptr test_integer = std::make_shared<yase::IntegerValue>(0);
        yase::BaseType::Ptr test_double = std::make_shared<yase::DoubleValue>(0.0);
        yase::BaseType::Ptr test_string = std::make_shared<yase::StringValue>("");

        EXPECT_TRUE(isLhsSubTypeOfRhs(test_bool, test_bool->clone()));
        EXPECT_TRUE(isLhsSubTypeOfRhs(test_integer, test_integer->clone()));
        EXPECT_TRUE(isLhsSubTypeOfRhs(test_double, test_double->clone()));
        EXPECT_TRUE(isLhsSubTypeOfRhs(test_string, test_string->clone()));

        EXPECT_FALSE(isLhsSubTypeOfRhs(test_bool, test_string));
        EXPECT_FALSE(isLhsSubTypeOfRhs(test_integer, test_string));
        EXPECT_FALSE(isLhsSubTypeOfRhs(test_double, test_string));
        EXPECT_FALSE(isLhsSubTypeOfRhs(test_string, test_double));
    }

    TEST_F(TypeTraitTest, subtype_comparision_user_defined_types_enum)
    {
        yase::BaseType::Ptr enum_rgb = std::make_shared<yase::UnsyncedEnum>("RgBColors", yase::Enum::List{"red", "green", "blue"});
        yase::BaseType::Ptr enum_cymk = std::make_shared<yase::UnsyncedEnum>("CmykColors", yase::Enum::List{"cyan", "magenta", "yellow", "black"});
        EXPECT_TRUE(yase::isLhsSubTypeOfRhs(enum_rgb, enum_rgb));
        EXPECT_FALSE(yase::isLhsSubTypeOfRhs(enum_rgb, enum_cymk));
    }

    TEST_F(TypeTraitTest, subtype_check_for_structs)
    {
        yase::BaseType::Ptr color = createTestVehicleColorEnum();
        yase::BaseType::Ptr vehicle = createTestVehicleStruct();
        yase::BaseType::Ptr emergency_vehicle = createTestInheritedVehicleStruct();
        yase::BaseType::Ptr emergency_vehicle_extended = emergency_vehicle->clone();

        EXPECT_TRUE(isLhsSubTypeOfRhs(color, color));

        // Vehicle is a subtype of emergency_vehicle
        EXPECT_TRUE(isLhsSubTypeOfRhs(vehicle, emergency_vehicle));
        EXPECT_TRUE(isLhsSubTypeOfRhs(emergency_vehicle, emergency_vehicle_extended));

        // Emergency_vehicle is not a subtype of emergency_vehicle
        EXPECT_FALSE(isLhsSubTypeOfRhs(emergency_vehicle, vehicle));
    }

} // namespace yase
