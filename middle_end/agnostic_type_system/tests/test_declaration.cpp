/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include <agnostic_type_system/declaration.h>

#include <gtest/gtest.h>

#include <agnostic_type_system/cast.h>

namespace yase
{
    TEST(TypeDeclarationTest, invalid_declarations)
    {
        // Test enum
        yase::Enum::List rgb_colors = {"Red", "Green", "Blue"};
        yase::UserDefined::Ptr test_enum = std::make_shared<yase::UnsyncedEnum>("TestEnum", rgb_colors);

        // Test struct
        yase::TypeDictionary::Map struct_field_map;
        struct_field_map.insert(std::make_pair("some_field", std::make_shared<yase::IntegerValue>(0)));
        yase::UserDefined::Ptr test_struct = std::make_shared<yase::Struct>("TestStruct", struct_field_map);

        // Test ctor signature 1
        yase::TypeSignatureFactory test_ctor_signature_1;
        test_ctor_signature_1.insertInputParam("some_field", std::make_shared<yase::IntegerValue>(0));

        // Test ctor signature 2
        yase::TypeSignatureFactory test_ctor_signature_2;
        test_ctor_signature_2.insertInputParam("some_field", std::make_shared<yase::IntegerValue>(0));
        test_ctor_signature_2.insertInputParam("other_field", std::make_shared<yase::IntegerValue>(0));

        // Test ctor signature 3
        yase::TypeSignatureFactory test_ctor_signature_3;
        test_ctor_signature_3.insertInputParam("some_field", std::make_shared<yase::DoubleValue>(0.));

        // Test enum
        EXPECT_NO_THROW(yase::UserDefinedTypeDeclaration{test_enum};);
        // Invalid, as non compound data type enum must have an empty ctor type signature
        EXPECT_ANY_THROW(yase::UserDefinedTypeDeclaration(test_enum, test_ctor_signature_1););

        // Test compound data type
        EXPECT_NO_THROW(yase::UserDefinedTypeDeclaration{test_struct});
        EXPECT_NO_THROW(yase::UserDefinedTypeDeclaration(test_struct, test_ctor_signature_1));
        // Invalid, as ctor type signature does not match the template struct
        EXPECT_ANY_THROW(yase::UserDefinedTypeDeclaration(test_struct, test_ctor_signature_2));
        // Invalid, as argument type does not match with template type
        EXPECT_ANY_THROW(yase::UserDefinedTypeDeclaration(test_struct, test_ctor_signature_3));
    }

    TEST(TypeDeclarationTest, instantiation)
    {
        // Struct
        yase::TypeDictionary::Map struct_field_map;
        struct_field_map.insert(std::make_pair("some_field", std::make_shared<yase::IntegerValue>(0)));
        yase::UserDefined::Ptr test_struct = std::make_shared<yase::Struct>("TestStruct", struct_field_map);
        // Ctor signature
        yase::TypeSignatureFactory test_ctor_signature;
        test_ctor_signature.insertInputParam("some_field", std::make_shared<yase::IntegerValue>(0));
        // Declration
        yase::UserDefinedTypeDeclaration test_struct_declaration(test_struct, test_ctor_signature);
        // Input arguments
        yase::InputArguments input_args;
        input_args.pushBackArg(std::make_shared<yase::IntegerValue>(707));

        // Test instantiation
        yase::BaseType::Ptr instance = test_struct_declaration.instantiate(input_args);
        EXPECT_EQ(yase::castToCppInteger(yase::castToCompound(instance)->getField("some_field")), 707);
    }

} // namespace yase
