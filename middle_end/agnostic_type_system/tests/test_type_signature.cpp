/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include <agnostic_type_system/type_signature.h>

#include <gtest/gtest.h>

#include <agnostic_type_system/cast.h>

namespace yase
{
    TEST(TypeSignatureTest, input_arguments_valid_arg_pushback)
    {
        // Only positional arguments
        // (int, int, double, double, string)
        yase::InputArguments input_arguments_1;
        EXPECT_NO_THROW(input_arguments_1.pushBackArg(std::make_shared<yase::IntegerValue>(0)));
        EXPECT_NO_THROW(input_arguments_1.pushBackArg(std::make_shared<yase::IntegerValue>(0)));
        EXPECT_NO_THROW(input_arguments_1.pushBackArg(std::make_shared<yase::DoubleValue>(0.0)));
        EXPECT_NO_THROW(input_arguments_1.pushBackArg(std::make_shared<yase::DoubleValue>(0.0)));
        EXPECT_NO_THROW(input_arguments_1.pushBackArg(std::make_shared<yase::StringValue>("")));

        // Only nominal arguments
        // (a: int, b: int, c: double, d: double, e: string)
        yase::InputArguments input_arguments_2;
        EXPECT_NO_THROW(input_arguments_2.pushBackArg(std::make_shared<yase::IntegerValue>(0), "a"));
        EXPECT_NO_THROW(input_arguments_2.pushBackArg(std::make_shared<yase::IntegerValue>(0), "b"));
        EXPECT_NO_THROW(input_arguments_2.pushBackArg(std::make_shared<yase::DoubleValue>(0.0), "c"));
        EXPECT_NO_THROW(input_arguments_2.pushBackArg(std::make_shared<yase::DoubleValue>(0.0), "d"));
        EXPECT_NO_THROW(input_arguments_2.pushBackArg(std::make_shared<yase::StringValue>(""), "e"));

        // Valid mixture of nominal & positional arguments
        // (int, int, double, d: double, e: string) --> positional before nominal
        yase::InputArguments input_arguments_3;
        EXPECT_NO_THROW(input_arguments_3.pushBackArg(std::make_shared<yase::IntegerValue>(0)));
        EXPECT_NO_THROW(input_arguments_3.pushBackArg(std::make_shared<yase::IntegerValue>(0)));
        EXPECT_NO_THROW(input_arguments_3.pushBackArg(std::make_shared<yase::DoubleValue>(0.0), "c"));
        EXPECT_NO_THROW(input_arguments_3.pushBackArg(std::make_shared<yase::DoubleValue>(0.0), "d"));
        EXPECT_NO_THROW(input_arguments_3.pushBackArg(std::make_shared<yase::StringValue>(""), "e"));
    }

    TEST(TypeSignatureTest, input_arguments_invalid_arg_pushback)
    {
        // Duplicate nominal arguments
        // (a: int, a: int) --> duplicate argument_identifer "a"
        yase::InputArguments input_arguments_1;
        input_arguments_1.pushBackArg(std::make_shared<yase::IntegerValue>(0), "a");
        EXPECT_ANY_THROW(input_arguments_1.pushBackArg(std::make_shared<yase::IntegerValue>(0), "a"););

        // Invalid mixture of nominal arguments
        // ( a: int, int, c: double) --> positional after key word argument
        yase::InputArguments input_arguments_2;
        input_arguments_2.pushBackArg(std::make_shared<yase::IntegerValue>(0));
        input_arguments_2.pushBackArg(std::make_shared<yase::IntegerValue>(0), "b");
        EXPECT_ANY_THROW(input_arguments_2.pushBackArg(std::make_shared<yase::DoubleValue>(0.0)););
    }

    TEST(TypeSignatureTest, type_signature_invalid_arg_pushback)
    {
        // Duplicate nominal arguments
        // (a: int, a: int) --> duplicate argument_identifer "a"
        yase::TypeSignatureFactory type_signature_1;
        type_signature_1.insertInputParam("a", std::make_shared<yase::IntegerValue>(0));
        EXPECT_ANY_THROW(type_signature_1.insertInputParam("a", std::make_shared<yase::IntegerValue>(0)););

        // Invalid: non-default argument follows default argument.
        // ( a: int = default, b: int = default, c: double) -->
        yase::TypeSignatureFactory type_signature_2;
        type_signature_2.insertInputParam("a", std::make_shared<yase::IntegerValue>(0), true);
        type_signature_2.insertInputParam("b", std::make_shared<yase::IntegerValue>(0), true);
        EXPECT_ANY_THROW(type_signature_2.insertInputParam("c", std::make_shared<yase::DoubleValue>(0.0)););
    }

    TEST(TypeSignatureTest, matched_arguments_getInputArg)
    {
        // This behavior is allowed as an input argument (First positional and then nominal)
        yase::TypeSignatureFactory type_signature;
        type_signature.insertInputParam("a", std::make_shared<yase::IntegerValue>(0));
        type_signature.insertInputParam("b", std::make_shared<yase::IntegerValue>(0), true);
        type_signature.insertInputParam("c", std::make_shared<yase::DoubleValue>(0.0), true);
        type_signature.insertInputParam("d", std::make_shared<yase::DoubleValue>(0.0), true);
        type_signature.insertInputParam("e", std::make_shared<yase::StringValue>(""), true);

        yase::InputArguments input_arguments;
        input_arguments.pushBackArg(std::make_shared<yase::IntegerValue>(1));

        MatchedArguments matched_arguments = type_signature.matchWith(input_arguments);

        // Valid: Argument identifers are not empty and exist
        EXPECT_EQ(matched_arguments.getInputArg("c")->type_category, yase::TypeCategory::kPrimitiveDouble);
        EXPECT_EQ(matched_arguments.getInputArg("e")->type_category, yase::TypeCategory::kPrimitiveString);
        // Invalid: It is not allowed to access an undefined argument or argument with empty identifier!
        EXPECT_ANY_THROW(matched_arguments.getInputArg(""));
        EXPECT_ANY_THROW(matched_arguments.getInputArg("f"));
    }

    TEST(TypeSignatureTest, input_arguments_invalid_duplicate_input_matches)
    {
        // This behavior is allowed as an input argument (First positional and then nominal)
        yase::TypeSignatureFactory type_signature;
        type_signature.insertInputParam("a", std::make_shared<yase::IntegerValue>(0));
        type_signature.insertInputParam("b", std::make_shared<yase::IntegerValue>(0), true);

        yase::InputArguments input_arguments;
        input_arguments.pushBackArg(std::make_shared<yase::IntegerValue>(1));

        // A type signature can be used infinite times
        EXPECT_NO_THROW(type_signature.matchWith(input_arguments););
        EXPECT_NO_THROW(type_signature.matchWith(input_arguments););
        EXPECT_NO_THROW(type_signature.matchWith(input_arguments););
    }

    TEST(TypeSignatureTest, type_signature_creation_invalid)
    {
        // Invalid, duplicate identifier "b"
        yase::TypeSignatureFactory type_signature;
        type_signature.insertInputParam("a", std::make_shared<yase::IntegerValue>(1));
        type_signature.insertInputParam("b", std::make_shared<yase::IntegerValue>(707));
        EXPECT_ANY_THROW(type_signature.insertInputParam("b", std::make_shared<yase::DoubleValue>(13.37)));
    }

    TEST(TypeSignatureTest, type_signature_matchWith_invalid_1)
    {
        // This behavior is allowed as an input argument (First positional and then nominal)
        yase::TypeSignatureFactory type_signature;
        type_signature.insertInputParam("a", std::make_shared<yase::IntegerValue>(1));
        type_signature.insertInputParam("b", std::make_shared<yase::IntegerValue>(707), true);
        type_signature.insertInputParam("c", std::make_shared<yase::DoubleValue>(13.37), true);

        yase::InputArguments invalid_input_arguments;
        invalid_input_arguments.pushBackArg(std::make_shared<yase::IntegerValue>(1));
        invalid_input_arguments.pushBackArg(std::make_shared<yase::IntegerValue>(707), "c");
        invalid_input_arguments.pushBackArg(std::make_shared<yase::DoubleValue>(13.37), "b");

        // Invalid, as "b" is not an Integer and "c" not and double (as expected)
        // ( 1, c: double = 13.37, b: int = 707) invalid for required: ( a: int, b: int = default, c: double = default)
        EXPECT_FALSE(type_signature.matches(invalid_input_arguments));
        EXPECT_ANY_THROW(type_signature.matchWith(invalid_input_arguments));
    }

    TEST(TypeSignatureTest, type_signature_matchWith_invalid_2)
    {
        yase::TypeSignatureFactory type_signature;
        type_signature.insertInputParam("a", std::make_shared<yase::IntegerValue>(1));
        type_signature.insertInputParam("b", std::make_shared<yase::IntegerValue>(707), true);
        type_signature.insertInputParam("c", std::make_shared<yase::DoubleValue>(13.37), true);

        yase::InputArguments invalid_input_arguments;
        invalid_input_arguments.pushBackArg(std::make_shared<yase::IntegerValue>(1), "not_a");
        invalid_input_arguments.pushBackArg(std::make_shared<yase::IntegerValue>(707), "b");
        invalid_input_arguments.pushBackArg(std::make_shared<yase::DoubleValue>(13.37), "c");

        // Invalid, as "a" is not defined at all
        // ( not_a: int = 1, c: double = 13.37, b: int = 707) invalid for required: ( a: int, b: int = default, c: double = default)
        EXPECT_FALSE(type_signature.matches(invalid_input_arguments));
        EXPECT_ANY_THROW(type_signature.matchWith(invalid_input_arguments));
    }

    TEST(TypeSignatureTest, type_signature_matchWith_valid_1)
    {
        yase::TypeSignatureFactory type_signature;
        type_signature.insertInputParam("a", std::make_shared<yase::IntegerValue>(1));
        type_signature.insertInputParam("b", std::make_shared<yase::IntegerValue>(707), true);
        type_signature.insertInputParam("c", std::make_shared<yase::DoubleValue>(13.37), true);

        yase::InputArguments valid_input_arguments;
        valid_input_arguments.pushBackArg(std::make_shared<yase::IntegerValue>(0));
        valid_input_arguments.pushBackArg(std::make_shared<yase::IntegerValue>(123), "b");
        valid_input_arguments.pushBackArg(std::make_shared<yase::DoubleValue>(3.1416), "c");

        // ( 0, b: int = 123, b: double = 3.1416) valid for required: ( a: int, b: int = default, c: double = default)
        EXPECT_TRUE(type_signature.matches(valid_input_arguments));
        yase::MatchedArguments matched_arguments = type_signature.matchWith(valid_input_arguments);

        EXPECT_EQ(yase::castToCppInteger(matched_arguments.getInputArg("a")), 0);
        EXPECT_EQ(yase::castToCppDouble(matched_arguments.getInputArg("b")), 123);
        EXPECT_EQ(yase::castToCppDouble(matched_arguments.getInputArg("c")), 3.1416);
    }

    TEST(TypeSignatureTest, type_signature_matchWith_valid_2)
    {
        yase::TypeSignatureFactory type_signature;
        type_signature.insertInputParam("a", std::make_shared<yase::IntegerValue>(1));
        type_signature.insertInputParam("b", std::make_shared<yase::IntegerValue>(707), true);
        type_signature.insertInputParam("c", std::make_shared<yase::DoubleValue>(13.37), true);

        yase::InputArguments valid_input_arguments;
        valid_input_arguments.pushBackArg(std::make_shared<yase::IntegerValue>(0), "a");
        valid_input_arguments.pushBackArg(std::make_shared<yase::IntegerValue>(123), "b");

        // ( 0, b: int = 123) valid for required: ( a: int, b: int = default, c: double = default)
        EXPECT_TRUE(type_signature.matches(valid_input_arguments));
        yase::MatchedArguments matched_arguments = type_signature.matchWith(valid_input_arguments);

        EXPECT_EQ(yase::castToCppInteger(matched_arguments.getInputArg("a")), 0);
        EXPECT_EQ(yase::castToCppDouble(matched_arguments.getInputArg("b")), 123);
        EXPECT_EQ(yase::castToCppDouble(matched_arguments.getInputArg("c")), 13.37);
    }

    TEST(TypeSignatureTest, type_signature_matchWith_valid_3)
    {
        yase::TypeSignatureFactory type_signature;
        type_signature.insertInputParam("a", std::make_shared<yase::IntegerValue>(1));
        type_signature.insertInputParam("b", std::make_shared<yase::IntegerValue>(707), true);
        type_signature.insertInputParam("c", std::make_shared<yase::DoubleValue>(13.37), true);

        yase::InputArguments valid_input_arguments;
        valid_input_arguments.pushBackArg(std::make_shared<yase::IntegerValue>(0), "a");

        // (0) valid for required: ( a: int, b: int = default, c: double = default)
        EXPECT_TRUE(type_signature.matches(valid_input_arguments));
        yase::MatchedArguments matched_arguments = type_signature.matchWith(valid_input_arguments);

        EXPECT_EQ(yase::castToCppInteger(matched_arguments.getInputArg("a")), 0);
        EXPECT_EQ(yase::castToCppDouble(matched_arguments.getInputArg("b")), 707);
        EXPECT_EQ(yase::castToCppDouble(matched_arguments.getInputArg("c")), 13.37);
    }

    TEST(TypeSignatureTest, input_arguments_matchWith_valid_4)
    {
        yase::TypeSignatureFactory type_signature;
        type_signature.insertInputParam("a", std::make_shared<yase::IntegerValue>(1));
        type_signature.insertInputParam("b", std::make_shared<yase::IntegerValue>(707), true);
        type_signature.insertInputParam("c", std::make_shared<yase::DoubleValue>(13.37), true);

        yase::InputArguments valid_input_arguments;
        valid_input_arguments.pushBackArg(std::make_shared<yase::IntegerValue>(0));
        valid_input_arguments.pushBackArg(std::make_shared<yase::IntegerValue>(123));

        // (0, 123) valid for required: ( a: int, b: int = default, c: double = default)
        EXPECT_TRUE(type_signature.matches(valid_input_arguments));
        yase::MatchedArguments matched_arguments = type_signature.matchWith(valid_input_arguments);

        EXPECT_EQ(yase::castToCppInteger(matched_arguments.getInputArg("a")), 0);
        EXPECT_EQ(yase::castToCppDouble(matched_arguments.getInputArg("b")), 123);
        EXPECT_EQ(yase::castToCppDouble(matched_arguments.getInputArg("c")), 13.37);
    }

} // namespace yase
