/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include <agnostic_type_system/types.h>

#include <utility>

#include <gtest/gtest.h>

#include <agnostic_type_system/cast.h>

namespace yase
{
    class TypesTest : public testing::Test
    {
    protected:
        yase::BaseType::Ptr createTestVehicleColorEnum()
        {
            yase::Enum::List vehicle_colors = {"Red", "Green", "Blue"};
            return std::make_shared<yase::UnsyncedEnum>("VehicleColor", vehicle_colors);
        };

        yase::BaseType::Ptr createTestLaneStruct()
        {
            yase::TypeDictionary::Map struct_field_map;
            struct_field_map.insert(std::make_pair("id", std::make_shared<yase::IntegerValue>(0)));
            struct_field_map.insert(std::make_pair("s", std::make_shared<yase::DoubleValue>(0)));
            struct_field_map.insert(std::make_pair("t", std::make_shared<yase::DoubleValue>(0)));
            return std::make_shared<yase::Struct>("LaneAssignment", struct_field_map);
        };

        yase::BaseType::Ptr createTestVehicleStruct()
        {
            yase::TypeDictionary::Map struct_field_map;
            struct_field_map.insert(std::make_pair("velocity", std::make_shared<yase::DoubleValue>(1.337)));
            struct_field_map.insert(std::make_pair("id", std::make_shared<yase::IntegerValue>(1)));
            struct_field_map.insert(std::make_pair("color", createTestVehicleColorEnum()));
            struct_field_map.insert(std::make_pair("lane", createTestLaneStruct()));

            return std::make_shared<yase::Struct>("Vehicle", struct_field_map);
        };

        yase::BaseType::Ptr createTestInheritedVehicleStruct()
        {
            yase::TypeDictionary::Map struct_field_map;
            struct_field_map.insert(std::make_pair("siren", std::make_shared<yase::BoolValue>(false)));
            struct_field_map.insert(std::make_pair("emergency_lights", std::make_shared<yase::BoolValue>(true)));

            return std::make_shared<yase::Struct>("EmergencyVehicle", struct_field_map, castToStruct(createTestVehicleStruct()));
        };
    };

    TEST_F(TypesTest, Primitives)
    {
        yase::BaseType::Ptr test_double = std::make_shared<yase::DoubleValue>(1.337);

        // Clone provides a copy of the exiting type instance, which is not connected to original type
        yase::BaseType::Ptr test_double_clone = test_double->clone();
        yase::castToDouble(test_double)->setFunction()(7.07);
        EXPECT_EQ(yase::castToDouble(test_double)->getFunction()(), 7.07);
        EXPECT_EQ(yase::castToDouble(test_double_clone)->getFunction()(), 1.337);
    }

    TEST_F(TypesTest, struct_access)
    {
        yase::Compound::Ptr vehicle = yase::castToCompound(createTestVehicleStruct());

        // Valid fields
        EXPECT_DOUBLE_EQ(yase::castToCppDouble(vehicle->getField("velocity")), 1.337);
        EXPECT_EQ(yase::castToCppInteger(vehicle->getField("id")), 1);
        yase::NestedKeys lane_id;
        lane_id.push("lane");
        lane_id.push("id");
        EXPECT_EQ(yase::castToCppInteger(vehicle->getField(lane_id)), 0);

        // Invalid field
        yase::NestedKeys invalid_nested_field;
        invalid_nested_field.push("invalid");
        invalid_nested_field.push("nested");
        invalid_nested_field.push("field");
        EXPECT_ANY_THROW(vehicle->getField(invalid_nested_field));
    }

    TEST_F(TypesTest, struct_clone)
    {
        // Test if it works even original template is out of scope
        yase::Compound::Ptr vehicle;
        {
            yase::BaseType::Ptr template_vehicle = createTestVehicleStruct();
            vehicle = yase::castToCompound(template_vehicle->clone());
        }

        EXPECT_DOUBLE_EQ(yase::castToCppDouble(vehicle->getField("velocity")), 1.337);
        EXPECT_EQ(yase::castToCppInteger(vehicle->getField("id")), 1);
        yase::NestedKeys lane_id;
        lane_id.push("lane");
        lane_id.push("id");
        EXPECT_EQ(yase::castToCppInteger(vehicle->getField(lane_id)), 0);
    }

    TEST_F(TypesTest, inheritance_valid)
    {
        yase::Compound::Ptr emergency_vehicle = yase::castToCompound(createTestInheritedVehicleStruct());

        // Fields of base vehicle
        EXPECT_DOUBLE_EQ(yase::castToCppDouble(emergency_vehicle->getField("velocity")), 1.337);
        EXPECT_EQ(yase::castToCppInteger(emergency_vehicle->getField("id")), 1);
        yase::NestedKeys lane_id;
        lane_id.push("lane");
        lane_id.push("id");
        EXPECT_EQ(yase::castToCppInteger(emergency_vehicle->getField(lane_id)), 0);

        // Fields of derived emergency_vehicle
        EXPECT_EQ(yase::castToCppBool(emergency_vehicle->getField("siren")), false);
        EXPECT_EQ(yase::castToCppBool(emergency_vehicle->getField("emergency_lights")), true);
    }

    TEST_F(TypesTest, cloned_inherited)
    {
        yase::Compound::Ptr emergency_vehicle;
        {
            yase::BaseType::Ptr template_vehicle = createTestInheritedVehicleStruct();
            emergency_vehicle = yase::castToCompound(template_vehicle->clone());
        }

        // Fields of base vehicle
        EXPECT_DOUBLE_EQ(yase::castToCppDouble(emergency_vehicle->getField("velocity")), 1.337);
        EXPECT_EQ(yase::castToCppInteger(emergency_vehicle->getField("id")), 1);
        yase::NestedKeys lane_id;
        lane_id.push("lane");
        lane_id.push("id");
        EXPECT_EQ(yase::castToCppInteger(emergency_vehicle->getField(lane_id)), 0);

        // Fields of derived emergency_vehicle
        EXPECT_EQ(yase::castToCppBool(emergency_vehicle->getField("siren")), false);
        EXPECT_EQ(yase::castToCppBool(emergency_vehicle->getField("emergency_lights")), true);
    }

    TEST_F(TypesTest, inheritance_invalid)
    {
        yase::TypeDictionary::Map struct_field_map;

        // Invalid as a base is already a struct of type "Vehicle"
        EXPECT_ANY_THROW(std::make_shared<yase::Struct>("Vehicle", struct_field_map, castToStruct(createTestInheritedVehicleStruct())););
        // Invalid as a base is already a struct of type "EmergencyVehicle"
        EXPECT_ANY_THROW(std::make_shared<yase::Struct>("EmergencyVehicle", struct_field_map, castToStruct(createTestInheritedVehicleStruct())););
        // Invalid as field "id" already exists
        struct_field_map.insert(std::make_pair("id", std::make_shared<yase::BoolValue>(false)));
        EXPECT_ANY_THROW(std::make_shared<yase::Struct>("SomeEmergencyVehicle", struct_field_map, castToStruct(createTestInheritedVehicleStruct())););
    }

} // namespace yase
