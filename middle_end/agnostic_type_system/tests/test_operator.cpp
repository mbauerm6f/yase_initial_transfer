/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include <agnostic_type_system/operator.h>

#include <gtest/gtest.h>

#include <agnostic_type_system/types.h>
#include <agnostic_type_system/cast.h>

namespace yase
{
    TEST(TypesOperatorTest, bool_assignment_operator)
    {
        // Some start values
        yase::Bool::Ptr bool_1 = std::make_shared<yase::BoolValue>(true);
        yase::BaseType::Ptr bool_2 = std::make_shared<yase::BoolValue>(false);

        // Define relational operations
        yase::AssignFunction assign_bool_2_to_bool_1 = yase::assignmentOperation(bool_1, yase::AssignmentOperator::kDirectAssignment, bool_2);

        // Test that the assignment did not happen yet - only if the assgin functor is invoked
        EXPECT_EQ(bool_1->getFunction()(), true);
        EXPECT_EQ(yase::castToCppBool(bool_2), false);

        // Now invoke the assignment by calling the assign operator
        assign_bool_2_to_bool_1();

        // Test if assignment was correct
        EXPECT_EQ(bool_1->getFunction()(), false);
        EXPECT_EQ(yase::castToCppBool(bool_2), false);

        // Change the value of bool_2, should not affect bool_1
        yase::castToBool(bool_2)->setFunction()(true);
        EXPECT_EQ(bool_1->getFunction()(), false);
        EXPECT_EQ(yase::castToCppBool(bool_2), true);

        // Test if next assignment
        assign_bool_2_to_bool_1();

        // Test if assignment was correct
        EXPECT_EQ(bool_1->getFunction()(), true);
        EXPECT_EQ(yase::castToCppBool(bool_2), true);
    }

    TEST(TypesOperatorTest, bool_relational_operator)
    {
        // Some start values
        yase::Bool::Ptr bool_1 = std::make_shared<yase::BoolValue>(false);
        yase::BaseType::Ptr bool_2 = std::make_shared<yase::BoolValue>(false);

        // Define relational operations
        yase::RelationalFunction compare_1 = yase::relationalOperation(bool_1, yase::RelationalOperator::kEqual, bool_2);
        yase::RelationalFunction compare_2 = yase::relationalOperation(bool_1, yase::RelationalOperator::kUnEqual, bool_2);
        // Invalid operations on Bools cause an exception:

        EXPECT_ANY_THROW(yase::RelationalFunction compare_3 = yase::relationalOperation(bool_1, yase::RelationalOperator::kGreater, bool_2););

        // Change the values after the operations were defined
        bool_1->setFunction()(true);
        yase::castToBool(bool_2)->setFunction()(false);

        // Test if the operations work based on the changed values
        EXPECT_EQ(compare_1(), false);
        EXPECT_EQ(bool_1->getFunction()(), true);
        EXPECT_EQ(yase::castToCppBool(bool_2), false);

        EXPECT_EQ(compare_2(), true);
        EXPECT_EQ(bool_1->getFunction()(), true);
        EXPECT_EQ(yase::castToCppBool(bool_2), false);
    }

    TEST(TypesOperatorTest, integer_assignment_operator)
    {
        // Some start values
        yase::Integer::Ptr int_1 = std::make_shared<yase::IntegerValue>(0);
        yase::BaseType::Ptr int_2 = std::make_shared<yase::IntegerValue>(3);

        // Define relational operations
        yase::AssignFunction assign_int1 = yase::assignmentOperation(int_1, yase::AssignmentOperator::kDirectAssignment, int_2);

        // Test that the assignment did not happen yet - only if the assgin functor is invoked
        EXPECT_EQ(int_1->getFunction()(), 0);
        EXPECT_EQ(yase::castToCppInteger(int_2), 3);

        // Now invoke the assignment by calling the assign operator
        assign_int1();

        // Test if assignment was correct
        EXPECT_EQ(int_1->getFunction()(), 3);
        EXPECT_EQ(yase::castToCppInteger(int_2), 3);

        // Change the value of int_2, should not affect int_1
        yase::castToInteger(int_2)->setFunction()(7);
        EXPECT_EQ(int_1->getFunction()(), 3);
        EXPECT_EQ(yase::castToCppInteger(int_2), 7);

        // Test if next assignment
        assign_int1();

        // Test if assignment was correct
        EXPECT_EQ(int_1->getFunction()(), 7);
        EXPECT_EQ(yase::castToCppInteger(int_2), 7);
    }

    TEST(TypesOperatorTest, integer_relational_operator)
    {
        // Some start values
        yase::Integer::Ptr int_1 = std::make_shared<yase::IntegerValue>(0);
        yase::Integer::Ptr int_2 = std::make_shared<yase::IntegerValue>(1);
        yase::BaseType::Ptr double_3 = std::make_shared<yase::DoubleValue>(2.0);

        // Define relational operations
        yase::RelationalFunction compare_1 = yase::relationalOperation(int_1, yase::RelationalOperator::kEqual, int_2);
        yase::RelationalFunction compare_2 = yase::relationalOperation(int_1, yase::RelationalOperator::kUnEqual, int_2);
        yase::RelationalFunction compare_3 = yase::relationalOperation(int_1, yase::RelationalOperator::kGreater, int_2);
        yase::RelationalFunction compare_4 = yase::relationalOperation(int_1, yase::RelationalOperator::kSmaller, int_2);
        yase::RelationalFunction compare_5 = yase::relationalOperation(int_1, yase::RelationalOperator::kGreaterEqual, int_2);
        yase::RelationalFunction compare_6 = yase::relationalOperation(int_1, yase::RelationalOperator::kSmallerEqual, double_3);

        // Change the values after the operations were defined
        int_1->setFunction()(9);
        int_2->setFunction()(3);
        yase::castToDouble(double_3)->setFunction()(9.0);

        // Test if the operations work based on the changed values
        EXPECT_EQ(compare_1(), false);
        EXPECT_EQ(int_1->getFunction()(), 9);
        EXPECT_EQ(int_2->getFunction()(), 3);

        EXPECT_EQ(compare_2(), true);
        EXPECT_EQ(int_1->getFunction()(), 9);
        EXPECT_EQ(int_2->getFunction()(), 3);

        EXPECT_EQ(compare_3(), true);
        EXPECT_EQ(int_1->getFunction()(), 9);
        EXPECT_EQ(int_2->getFunction()(), 3);

        EXPECT_EQ(compare_4(), false);
        EXPECT_EQ(int_1->getFunction()(), 9);
        EXPECT_EQ(int_2->getFunction()(), 3);

        EXPECT_EQ(compare_5(), true);
        EXPECT_EQ(int_1->getFunction()(), 9);
        EXPECT_EQ(int_2->getFunction()(), 3);

        EXPECT_EQ(compare_6(), true);
        EXPECT_EQ(int_1->getFunction()(), 9);
        EXPECT_EQ(yase::castToCppDouble(double_3), 9.0);
    }

    TEST(TypesOperatorTest, integer_arithmetic_operator)
    {
        // Some start values
        yase::Integer::Ptr int_1 = std::make_shared<yase::IntegerValue>(0);
        yase::Integer::Ptr int_2 = std::make_shared<yase::IntegerValue>(1);
        yase::BaseType::Ptr int_3 = std::make_shared<yase::IntegerValue>(2);

        // Define arithmetic operations
        yase::BaseType::Ptr calc_1 = yase::arithmeticOperation(int_1, yase::ArithmeticOperator::kAddition, int_2);
        yase::BaseType::Ptr calc_2 = yase::arithmeticOperation(int_1, yase::ArithmeticOperator::kSubtraction, int_2);
        yase::BaseType::Ptr calc_3 = yase::arithmeticOperation(int_1, yase::ArithmeticOperator::kMultiplication, int_2);
        yase::BaseType::Ptr calc_4 = yase::arithmeticOperation(int_1, yase::ArithmeticOperator::kDivision, int_2);
        yase::BaseType::Ptr calc_5 = yase::arithmeticOperation(int_1, yase::ArithmeticOperator::kAddition,
                                                               yase::arithmeticOperation(int_2, yase::ArithmeticOperator::kMultiplication, int_3));

        // Change the values after the operations were defined
        int_1->setFunction()(9);
        int_2->setFunction()(3);
        yase::castToInteger(int_3)->setFunction()(4);

        // Test Addition, operands are not allowed to change
        EXPECT_EQ(yase::castToCppInteger(calc_1), 12);
        EXPECT_EQ(int_1->getFunction()(), 9);
        EXPECT_EQ(int_2->getFunction()(), 3);

        // Test Subraction
        EXPECT_EQ(yase::castToCppInteger(calc_2), 6);
        EXPECT_EQ(int_1->getFunction()(), 9);
        EXPECT_EQ(int_2->getFunction()(), 3);

        // Test Multiplication
        EXPECT_EQ(yase::castToCppInteger(calc_3), 27);
        EXPECT_EQ(int_1->getFunction()(), 9);
        EXPECT_EQ(int_2->getFunction()(), 3);

        // Test Division - The result will be always a double!
        EXPECT_EQ(yase::castToCppDouble(calc_4), 3.0);
        EXPECT_EQ(int_1->getFunction()(), 9);
        EXPECT_EQ(int_2->getFunction()(), 3);

        // Test to cascade multiple operations
        // NOTE: This does not obey the Order of operations! Just in this given example it works "by purpose"
        EXPECT_EQ(yase::castToCppInteger(int_3), 4);
        EXPECT_EQ(yase::castToCppInteger(calc_5), 9 + 3 * 4);
        EXPECT_EQ(int_1->getFunction()(), 9);
        EXPECT_EQ(int_2->getFunction()(), 3);
    }

    TEST(TypesOperatorTest, double_assignment_operator)
    {
        // Some start values
        yase::Double::Ptr double_1 = std::make_shared<yase::DoubleValue>(1.1);
        yase::BaseType::Ptr double_2 = std::make_shared<yase::DoubleValue>(2.2);
        yase::BaseType::Ptr int_3 = std::make_shared<yase::IntegerValue>(3);

        // Define relational operations
        yase::AssignFunction assign_double_2_to_double_1 = yase::assignmentOperation(double_1, yase::AssignmentOperator::kDirectAssignment, double_2);
        yase::AssignFunction assign_int_3_to_double_1 = yase::assignmentOperation(double_1, yase::AssignmentOperator::kDirectAssignment, int_3);

        // Test that the assignment did not happen yet - only if the assgin functor is invoked
        EXPECT_DOUBLE_EQ(double_1->getFunction()(), 1.1);
        EXPECT_DOUBLE_EQ(yase::castToCppDouble(double_2), 2.2);

        // Now invoke the assignment by calling the assign operator
        assign_double_2_to_double_1();

        // Test if assignment was correct
        EXPECT_DOUBLE_EQ(double_1->getFunction()(), 2.2);
        EXPECT_DOUBLE_EQ(yase::castToCppDouble(double_2), 2.2);

        // Change the value of double_2, should not affect double_1
        yase::castToDouble(double_2)->setFunction()(7.07);
        EXPECT_DOUBLE_EQ(double_1->getFunction()(), 2.2);
        EXPECT_DOUBLE_EQ(yase::castToCppDouble(double_2), 7.07);

        // Test next assignment
        assign_double_2_to_double_1();

        // Test if assignment was correct
        EXPECT_DOUBLE_EQ(double_1->getFunction()(), 7.07);
        EXPECT_DOUBLE_EQ(yase::castToCppDouble(double_2), 7.07);

        // Test an assignment with an int
        assign_int_3_to_double_1();

        // Test if assignment was correct
        EXPECT_DOUBLE_EQ(double_1->getFunction()(), 3.0);
        EXPECT_EQ(yase::castToCppInteger(int_3), 3);
    }

    TEST(TypesOperatorTest, double_relational_operator)
    {
        // Some start values
        yase::Double::Ptr double_1 = std::make_shared<yase::DoubleValue>(0.0);
        yase::Double::Ptr double_2 = std::make_shared<yase::DoubleValue>(0.0);
        yase::BaseType::Ptr int_3 = std::make_shared<yase::IntegerValue>(2);

        // Define relational operations
        yase::RelationalFunction compare_1 = yase::relationalOperation(double_1, yase::RelationalOperator::kEqual, double_2);
        yase::RelationalFunction compare_2 = yase::relationalOperation(double_1, yase::RelationalOperator::kUnEqual, double_2);
        yase::RelationalFunction compare_3 = yase::relationalOperation(double_1, yase::RelationalOperator::kGreater, double_2);
        yase::RelationalFunction compare_4 = yase::relationalOperation(double_1, yase::RelationalOperator::kSmaller, double_2);
        yase::RelationalFunction compare_5 = yase::relationalOperation(double_1, yase::RelationalOperator::kGreaterEqual, double_2);
        yase::RelationalFunction compare_6 = yase::relationalOperation(double_2, yase::RelationalOperator::kSmallerEqual, int_3);

        // Change the values after the operations were defined
        double_1->setFunction()(7.07);
        double_2->setFunction()(3.0);
        yase::castToInteger(int_3)->setFunction()(3);

        // Test if the operations work based on the changed values
        EXPECT_EQ(compare_1(), false);
        EXPECT_DOUBLE_EQ(double_1->getFunction()(), 7.07);
        EXPECT_DOUBLE_EQ(double_2->getFunction()(), 3.0);

        EXPECT_EQ(compare_2(), true);
        EXPECT_DOUBLE_EQ(double_1->getFunction()(), 7.07);
        EXPECT_DOUBLE_EQ(double_2->getFunction()(), 3.0);

        EXPECT_EQ(compare_3(), true);
        EXPECT_DOUBLE_EQ(double_1->getFunction()(), 7.07);
        EXPECT_DOUBLE_EQ(double_2->getFunction()(), 3.0);

        EXPECT_EQ(compare_4(), false);
        EXPECT_DOUBLE_EQ(double_1->getFunction()(), 7.07);
        EXPECT_DOUBLE_EQ(double_2->getFunction()(), 3.0);

        EXPECT_EQ(compare_5(), true);
        EXPECT_DOUBLE_EQ(double_1->getFunction()(), 7.07);
        EXPECT_DOUBLE_EQ(double_2->getFunction()(), 3.0);

        EXPECT_EQ(compare_6(), true);
        EXPECT_DOUBLE_EQ(double_2->getFunction()(), 3.0);
        EXPECT_EQ(yase::castToInteger(int_3)->getFunction()(), 3);
    }

    TEST(TypesOperatorTest, double_arithmetic_operator)
    {
        // Some start values
        yase::Double::Ptr double_1 = std::make_shared<yase::DoubleValue>(0.0);
        yase::Double::Ptr double_2 = std::make_shared<yase::DoubleValue>(1.0);
        yase::BaseType::Ptr int_3 = std::make_shared<yase::IntegerValue>(0);

        // Define arithmetic operations
        yase::BaseType::Ptr calc_1 = yase::arithmeticOperation(double_1, yase::ArithmeticOperator::kAddition, double_2);
        yase::BaseType::Ptr calc_2 = yase::arithmeticOperation(double_1, yase::ArithmeticOperator::kSubtraction, double_2);
        yase::BaseType::Ptr calc_3 = yase::arithmeticOperation(double_1, yase::ArithmeticOperator::kMultiplication, int_3);
        yase::BaseType::Ptr calc_4 = yase::arithmeticOperation(double_1, yase::ArithmeticOperator::kDivision, int_3);
        yase::BaseType::Ptr calc_5 = yase::arithmeticOperation(double_1, yase::ArithmeticOperator::kAddition,
                                                               yase::arithmeticOperation(double_2, yase::ArithmeticOperator::kMultiplication, int_3));

        // Change the values after the operations were defined
        double_1->setFunction()(7.07);
        double_2->setFunction()(13.37);
        yase::castToInteger(int_3)->setFunction()(2);

        // Test Addition, operands are not allowed to change
        EXPECT_DOUBLE_EQ(double_1->getFunction()(), 7.07);
        EXPECT_DOUBLE_EQ(double_2->getFunction()(), 13.37);
        EXPECT_DOUBLE_EQ(yase::castToCppDouble(calc_1), 7.07 + 13.37);

        // Test Subraction, operands are not allowed to change
        EXPECT_DOUBLE_EQ(double_1->getFunction()(), 7.07);
        EXPECT_DOUBLE_EQ(double_2->getFunction()(), 13.37);
        EXPECT_DOUBLE_EQ(yase::castToCppDouble(calc_2), 7.07 - 13.37);

        // Test Multiplication, operands are not allowed to change

        EXPECT_DOUBLE_EQ(double_1->getFunction()(), 7.07);
        EXPECT_DOUBLE_EQ(double_2->getFunction()(), 13.37);
        EXPECT_DOUBLE_EQ(yase::castToCppDouble(calc_3), 7.07 * 2);

        // Test Division, operands are not allowed to change

        EXPECT_DOUBLE_EQ(double_1->getFunction()(), 7.07);
        EXPECT_DOUBLE_EQ(double_2->getFunction()(), 13.37);
        EXPECT_DOUBLE_EQ(yase::castToCppDouble(calc_4), 7.07 / 2);

        // Test to cascade multiple operations
        // NOTE: This does not obey the Order of operations! Just in this given example it works "by purpose"
        EXPECT_DOUBLE_EQ(double_1->getFunction()(), 7.07);
        EXPECT_DOUBLE_EQ(double_2->getFunction()(), 13.37);
        EXPECT_EQ(yase::castToCppInteger(int_3), 2);
        EXPECT_DOUBLE_EQ(yase::castToCppDouble(calc_5), 7.07 + 13.37 * 2);
    }

    TEST(TypesOperatorTest, string_assignment_operator)
    {
        // Init values
        yase::String::Ptr str_1 = std::make_shared<yase::StringValue>("init_str_1");
        yase::BaseType::Ptr str_2 = std::make_shared<yase::StringValue>("init_str_2");

        // Define relational operations
        yase::AssignFunction assign_str_2_to_str_1 = yase::assignmentOperation(str_1, yase::AssignmentOperator::kDirectAssignment, str_2);

        // Test that the assignment did not happen yet - only if the assgin functor is invoked
        EXPECT_EQ(str_1->getFunction()(), "init_str_1");
        EXPECT_EQ(yase::castToCppString(str_2), "init_str_2");

        // Now invoke the assignment by calling the assign operator
        assign_str_2_to_str_1();

        // Test if assignment was correct
        EXPECT_EQ(str_1->getFunction()(), "init_str_2");
        EXPECT_EQ(yase::castToCppString(str_2), "init_str_2");

        // Change the value of str_2, should not affect str_1
        yase::castToString(str_2)->setFunction()("new_str");
        EXPECT_EQ(str_1->getFunction()(), "init_str_2");
        EXPECT_EQ(yase::castToCppString(str_2), "new_str");

        // Test if next assignment
        assign_str_2_to_str_1();

        // Test if assignment was correct
        EXPECT_EQ(str_1->getFunction()(), "new_str");
        EXPECT_EQ(yase::castToCppString(str_2), "new_str");
    }

    TEST(TypesOperatorTest, string_relational_operator)
    {
        // Init values
        yase::String::Ptr str_1 = std::make_shared<yase::StringValue>("init_str_1");
        yase::BaseType::Ptr str_2 = std::make_shared<yase::StringValue>("init_str_2");

        // Define relational operations
        yase::RelationalFunction compare_1 = yase::relationalOperation(str_1, yase::RelationalOperator::kEqual, str_2);
        yase::RelationalFunction compare_2 = yase::relationalOperation(str_1, yase::RelationalOperator::kUnEqual, str_2);
        // Invalid operations on Bools cause an exception:

        EXPECT_ANY_THROW(yase::RelationalFunction compare_3 = yase::relationalOperation(str_1, yase::RelationalOperator::kGreater, str_2););

        // Change the values after the operations were defined
        str_1->setFunction()("runtime_changed_str_1");
        yase::castToString(str_2)->setFunction()("runtime_changed_str_2");

        // Test if the operations work based on the changed values
        EXPECT_EQ(compare_1(), false);
        EXPECT_EQ(str_1->getFunction()(), "runtime_changed_str_1");
        EXPECT_EQ(yase::castToCppString(str_2), "runtime_changed_str_2");

        EXPECT_EQ(compare_2(), true);
        EXPECT_EQ(str_1->getFunction()(), "runtime_changed_str_1");
        EXPECT_EQ(yase::castToCppString(str_2), "runtime_changed_str_2");
    }

    TEST(TypesOperatorTest, enum_relational_operator)
    {
        // Define two reference enum types
        yase::UnsyncedEnum enum_default_rgb{"RgBColors", yase::Enum::List{"red", "green", "blue"}};
        yase::UnsyncedEnum enum_default_cmyk{"CmykColors", yase::Enum::List{"cyan", "magenta", "yellow", "black"}};
        // Make instances of the default types
        yase::Enum::Ptr enum_1 = castToEnum(enum_default_rgb.clone());
        yase::Enum::Ptr enum_2 = castToEnum(enum_default_rgb.clone());
        yase::Enum::Ptr enum_3 = castToEnum(enum_default_rgb.clone());
        yase::Enum::Ptr enum_4 = castToEnum(enum_default_cmyk.clone());

        // Make compare functors
        yase::RelationalFunction compare_1 = yase::relationalOperation(enum_1, yase::RelationalOperator::kEqual, enum_2);
        yase::RelationalFunction compare_2 = yase::relationalOperation(enum_1, yase::RelationalOperator::kEqual, enum_3);
        yase::RelationalFunction compare_3 = yase::relationalOperation(enum_1, yase::RelationalOperator::kEqual, enum_4);
        yase::RelationalFunction compare_4 = yase::relationalOperation(enum_1, yase::RelationalOperator::kUnEqual, enum_2);
        yase::RelationalFunction compare_5 = yase::relationalOperation(enum_1, yase::RelationalOperator::kUnEqual, enum_3);
        yase::RelationalFunction compare_6 = yase::relationalOperation(enum_1, yase::RelationalOperator::kUnEqual, enum_4);

        // Change values after the compare functors were created
        enum_1->setFunction()("red");
        enum_2->setFunction()("red");
        enum_3->setFunction()("blue");
        enum_4->setFunction()("cyan");

        // Test if the compare functors work based on the changed values
        EXPECT_TRUE(compare_1);
        EXPECT_TRUE(compare_2);
        EXPECT_TRUE(compare_3);
        EXPECT_TRUE(compare_4);
        EXPECT_TRUE(compare_5);
        EXPECT_TRUE(compare_6);
    }

} // namespace yase
