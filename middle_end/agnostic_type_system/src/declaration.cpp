/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "agnostic_type_system/declaration.h"

#include <string>
#include <queue>
#include <exception>

#include <agnostic_type_system/cast.h>
#include <agnostic_type_system/operator.h>
#include <agnostic_type_system/types.h>
#include <agnostic_type_system/type_trait.h>

namespace yase
{
  UserDefinedTypeDeclaration::UserDefinedTypeDeclaration(const UserDefined::Ptr type_template,
                                                         const TypeSignatureFactory &type_ctor_signature)
      : ctor_signature(type_ctor_signature),
        type_template_(type_template)
  {
    if (!ctor_signature.getInputParams().empty())
    {
      if (!isCompoundType(type_template_))
      {
        std::string error_msg = "Error: Declaraton of data type [";
        error_msg.append(typeCategoryToString(type_template_->type_category)).append("::").append(type_template_->user_defined_name);
        error_msg.append("] must have an empty constructor type signature, as it is a non compound data type.");
        throw std::invalid_argument(error_msg);
      }

      // Check if constructor type signature matches with underlying type
      Compound::Ptr casted_compound_type = castToCompound(type_template_);
      for (const TypeSignatureFactory::Param &arg : ctor_signature.getInputParams())
      {
        yase::BaseType::Ptr field;
        try
        {
          NestedKeys arg_identifier;
          arg_identifier.push(arg.identifer);
          field = casted_compound_type->getField(arg_identifier);
        }
        catch (...)
        {
          std::string error_msg = "Error: Constructor type signature for type [";
          error_msg.append(typeCategoryToString(type_template_->type_category)).append("::").append(type_template_->user_defined_name);
          error_msg.append("] constains field [");
          error_msg.append(arg.identifer);
          error_msg.append("] which does not exist in the type.");
          throw std::invalid_argument(error_msg);
        }
        if (!yase::isLhsSubTypeOfRhs(field, arg.type))
        {
          std::string error_msg = "Error: Argument type [";
          error_msg.append(yase::typeCategoryToString(arg.type->type_category));
          error_msg.append("] of argument [");
          error_msg.append(arg.identifer);
          error_msg.append("] in constructor type signature is not a valid subtype of field type [");
          error_msg.append(yase::typeCategoryToString(field->type_category));
          error_msg.append("].");
          throw std::invalid_argument(error_msg);
        }
      }
    }
  }

  BaseType::Ptr UserDefinedTypeDeclaration::instantiate(const InputArguments &input_args)
  {
    // If input signature is empty, instantiate type direct
    if (ctor_signature.getInputParams().empty())
    {
      return type_template_->clone();
    }

    // Match with CtorSignature with input and map all values
    MatchedArguments matched_args = ctor_signature.matchWith(input_args);
    Compound::Ptr casted_compound_type = castToCompound(type_template_->clone());
    for (const std::string &arg_identifier : matched_args.getAvailableInputArgIdentifiers())
    {
      // Get assignment operator function and execute it direct
      assignmentOperation(casted_compound_type->getField(arg_identifier),
                          AssignmentOperator::kDirectAssignment,
                          matched_args.getInputArg(arg_identifier))();
    }
    return casted_compound_type;
  }

  std::string UserDefinedTypeDeclaration::getUserDefinedName()
  {
    return type_template_->user_defined_name;
  }

  TypeCategory UserDefinedTypeDeclaration::getTypeCategory()
  {
    return type_template_->type_category;
  }

} // namespace yase
