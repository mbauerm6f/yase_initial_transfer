/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "agnostic_type_system/cast.h"

#include <agnostic_type_system/type_trait.h>

namespace yase
{
  bool castToCppBool(const BaseType::Ptr base_type)
  {
    return castToBool(base_type)->getFunction()();
  }

  int castToCppInteger(const BaseType::Ptr base_type)
  {
    return castToInteger(base_type)->getFunction()();
  }

  double castToCppDouble(const BaseType::Ptr base_type)
  {
    if ((base_type->type_category == TypeCategory::kPrimitiveDouble))
    {
      auto double_type = std::dynamic_pointer_cast<const Double>(base_type);
      return double_type->getFunction()();
    }
    else if ((base_type->type_category == TypeCategory::kPrimitiveInteger))
    {
      auto int_type = std::dynamic_pointer_cast<const Integer>(base_type);
      return int_type->getFunction()();
    }

    throw utils::InvalidTypeCategoryCast(base_type->type_category, TypeCategory::kPrimitiveDouble);
  }

  std::string castToCppString(const BaseType::Ptr base_type)
  {
    return castToString(base_type)->getFunction()();
  }

  Integer::Ptr castToInteger(const BaseType::Ptr base_type)
  {
    if (base_type == nullptr)
    {
      throw std::invalid_argument("Cannot cast type to [yase::Integer], as input type is NULL.");
    }
    if (base_type->type_category != TypeCategory::kPrimitiveInteger)
    {
      throw utils::InvalidTypeCategoryCast(base_type->type_category, TypeCategory::kPrimitiveInteger);
    }
    auto integer_type = std::dynamic_pointer_cast<Integer>(base_type);
    return integer_type;
  }

  Bool::Ptr castToBool(const BaseType::Ptr base_type)
  {
    if (base_type == nullptr)
    {
      throw std::invalid_argument("Cannot cast type to [yase::Bool], as input type is NULL.");
    }
    if (base_type->type_category != TypeCategory::kPrimitiveBool)
    {
      throw utils::InvalidTypeCategoryCast(base_type->type_category, TypeCategory::kPrimitiveBool);
    }
    auto bool_type = std::dynamic_pointer_cast<Bool>(base_type);
    return bool_type;
  }

  Double::Ptr castToDouble(const BaseType::Ptr base_type)
  {
    if (base_type == nullptr)
    {
      throw std::invalid_argument("Cannot cast type to [yase::Double], as input type is NULL.");
    }
    if (base_type->type_category != TypeCategory::kPrimitiveDouble)
    {
      throw utils::InvalidTypeCategoryCast(base_type->type_category, TypeCategory::kPrimitiveDouble);
    }
    auto double_type = std::dynamic_pointer_cast<Double>(base_type);
    return double_type;
  }

  String::Ptr castToString(const BaseType::Ptr base_type)
  {
    if (base_type == nullptr)
    {
      throw std::invalid_argument("Cannot cast type to [yase::String], as input type is NULL.");
    }
    if (base_type->type_category != TypeCategory::kPrimitiveString)
    {
      throw utils::InvalidTypeCategoryCast(base_type->type_category, TypeCategory::kPrimitiveString);
    }
    auto string_type = std::dynamic_pointer_cast<String>(base_type);
    return string_type;
  }

  Enum::Ptr castToEnum(const BaseType::Ptr base_type)
  {
    if (base_type == nullptr)
    {
      throw std::invalid_argument("Cannot cast type to [yase::Enum], as input type is NULL.");
    }
    if (base_type->type_category != TypeCategory::kUserDefinedEnum)
    {
      throw utils::InvalidTypeCategoryCast(base_type->type_category, TypeCategory::kUserDefinedEnum);
    }
    auto casted_type = std::dynamic_pointer_cast<Enum>(base_type);
    return casted_type;
  }

  Struct::Ptr castToStruct(const BaseType::Ptr base_type)
  {
    if (base_type == nullptr)
    {
      throw std::invalid_argument("Cannot cast type to [yase::Struct], as input type is NULL.");
    }
    if (base_type->type_category != TypeCategory::kUserDefinedStruct)
    {
      throw utils::InvalidTypeCategoryCast(base_type->type_category, TypeCategory::kUserDefinedStruct);
    }
    auto casted_type = std::dynamic_pointer_cast<Struct>(base_type);
    return casted_type;
  }

  Actor::Ptr castToActor(const BaseType::Ptr base_type)
  {
    if (base_type == nullptr)
    {
      throw std::invalid_argument("Cannot cast type to [yase::Actor], as input type is NULL.");
    }
    if (base_type->type_category != TypeCategory::kUserDefinedActor)
    {
      throw utils::InvalidTypeCategoryCast(base_type->type_category, TypeCategory::kUserDefinedActor);
    }
    auto casted_type = std::dynamic_pointer_cast<Actor>(base_type);
    return casted_type;
  }

  UserDefined::Ptr castToUserDefined(const BaseType::Ptr base_type)
  {
    if (!isUserDefinedType(base_type))
    {
      throw utils::InvalidTypeCategoryCast(base_type->type_category, "UserDefined");
    }
    auto casted_type = std::dynamic_pointer_cast<UserDefined>(base_type);
    return casted_type;
  }

  Compound::Ptr castToCompound(const BaseType::Ptr base_type)
  {
    if (!isUserDefinedType(base_type))
    {
      throw utils::InvalidTypeCategoryCast(base_type->type_category, "Compound");
    }
    auto casted_type = std::dynamic_pointer_cast<Compound>(base_type);
    return casted_type;
  }

} // namespace yase