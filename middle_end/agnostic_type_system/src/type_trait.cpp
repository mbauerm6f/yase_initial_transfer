/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "agnostic_type_system/type_trait.h"

#include <algorithm>
#include <string>
#include <queue>

#include <agnostic_type_system/cast.h>

namespace yase
{

  bool isLhsSubTypeOfRhs(const BaseType::Ptr lhs, const BaseType::Ptr &rhs)
  {
    if (lhs == nullptr || rhs == nullptr)
    {
      throw std::invalid_argument("Error while comparing the subtype, at least one of the arguments is NULL");
    }

    switch (lhs->type_category)
    {
    case TypeCategory::kPrimitiveBool:
    case TypeCategory::kPrimitiveInteger:
    case TypeCategory::kPrimitiveDouble:
    case TypeCategory::kPrimitiveString:
      return (lhs->type_category == rhs->type_category);
    case TypeCategory::kUserDefinedEnum:
      return isLhsSubTypeOfRhs(castToEnum(lhs), rhs);
    case TypeCategory::kUserDefinedStruct:
      return isLhsSubTypeOfRhs(castToStruct(lhs), rhs);
    case TypeCategory::kUserDefinedActor:
      return isLhsSubTypeOfRhs(castToActor(lhs), rhs);
    default:
      throw std::invalid_argument("Error while comparing the subtype, TypeCategory could not be handled.");
    }
  }

  bool isLhsSubTypeOfRhs(const Enum::Ptr lhs, const BaseType::Ptr &rhs)
  {
    if (rhs->type_category != TypeCategory::kUserDefinedEnum)
    {
      return false;
    }
    Enum::Ptr casted_rhs = castToEnum(rhs);

    Enum::List lhs_list = lhs->getEnumeratorList();
    Enum::List rhs_list = casted_rhs->getEnumeratorList();
    for (const auto &lhs_enum : lhs_list)
    {
      if (rhs_list.find(lhs_enum) == rhs_list.end())
      {
        return false;
      }
    }
    return true;
  }

  bool isLhsSubTypeOfRhs(const Struct::Ptr lhs, const BaseType::Ptr &rhs)
  {
    if (rhs->type_category != TypeCategory::kUserDefinedStruct)
    {
      return false;
    }
    Struct::Ptr casted_rhs = castToStruct(rhs);

    // Stored esplicit due to iterator invalidation
    std::deque<std::string> rhs_inheritance_order = casted_rhs->getInheritanceOrder();
    if (std::find(rhs_inheritance_order.begin(), rhs_inheritance_order.end(), lhs->user_defined_name) == rhs_inheritance_order.end())
    {
      return false;
    }

    for (const auto &lhs_field : lhs->getAccessibleFields())
    {
      const TypeDictionary::Map rhs_fields = casted_rhs->getAccessibleFields();
      const auto rhs_field = rhs_fields.find(lhs_field.first);

      if (rhs_field == rhs_fields.end())
      {
        return false;
      }

      if (!isLhsSubTypeOfRhs(lhs_field.second, rhs_field->second))
      {
        return false;
      }
    }
    return true;
  }

  bool isLhsSubTypeOfRhs(const Actor::Ptr lhs, const BaseType::Ptr &rhs)
  {
    if (rhs->type_category != TypeCategory::kUserDefinedActor)
    {
      return false;
    }
    Actor::Ptr casted_rhs = castToActor(rhs);

    if (lhs->user_defined_name != casted_rhs->user_defined_name)
    {
      return false;
    }

    std::deque<std::string> rhs_inheritance_order = casted_rhs->getInheritanceOrder();
    if (std::find(rhs_inheritance_order.begin(), rhs_inheritance_order.end(), lhs->user_defined_name) == rhs_inheritance_order.end())
    {
      return false;
    }

    for (const auto &lhs_field : lhs->getAccessibleFields())
    {
      const TypeDictionary::Map rhs_fields = casted_rhs->getAccessibleFields();
      const auto rhs_field = rhs_fields.find(lhs_field.first);
      if (rhs_field == rhs_fields.end())
      {
        return false;
      }
      if (!isLhsSubTypeOfRhs(lhs_field.second, rhs_field->second))
      {
        return false;
      }
    }
    return true;
  }

  bool isPrimitive(const BaseType::Ptr type)
  {
    if ((type->type_category == TypeCategory::kPrimitiveBool) || (type->type_category == TypeCategory::kPrimitiveDouble) || (type->type_category == TypeCategory::kPrimitiveInteger) || (type->type_category == TypeCategory::kPrimitiveString))
    {
      return true;
    }
    return false;
  }

  bool isUserDefinedType(const BaseType::Ptr type)
  {
    if (isCompoundType(type) || (type->type_category == TypeCategory::kUserDefinedEnum))
    {
      return true;
    }
    return false;
  }

  bool isCompoundType(const BaseType::Ptr type)
  {
    if ((type->type_category == TypeCategory::kUserDefinedStruct) || (type->type_category == TypeCategory::kUserDefinedActor))
    {
      return true;
    }
    return false;
  }

  bool isConvertible(const BaseType::Ptr &from, const BaseType::Ptr &to)
  {
    if (isLhsSubTypeOfRhs(to, from))
    {
      return true;
    }
    // Additional check for int -> float
    if (from->type_category == TypeCategory::kPrimitiveInteger && to->type_category == TypeCategory::kPrimitiveDouble)
    {
      return true;
    }
    return false;
  }

} // namespace yase
