/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "agnostic_type_system/operator.h"

#include <cmath>
#include <exception>
#include <functional>
#include <limits>
#include <utility>

#include <agnostic_type_system/type_trait.h>
#include <agnostic_type_system/cast.h>

namespace yase
{
  std::string assignmentOperationToString(const AssignmentOperator &category)
  {
    switch (category)
    {
    case AssignmentOperator::kDirectAssignment:
      return "=";
      break;
    default:
      throw std::invalid_argument("Unkown typeCategory.");
      break;
    }
    return "";
  }

  std::string arithmeticOperationToString(const ArithmeticOperator &op)
  {
    switch (op)
    {
    case ArithmeticOperator::kAddition:
      return "+";
      break;
    case ArithmeticOperator::kSubtraction:
      return "-";
      break;
    case ArithmeticOperator::kMultiplication:
      return "*";
      break;
    case ArithmeticOperator::kDivision:
      return "/";
      break;
    default:
      throw std::invalid_argument("Unkown typeCategory.");
      break;
    }
    return "";
  }

  std::string relationalOperationToString(const RelationalOperator &op)
  {
    switch (op)
    {
    case RelationalOperator::kEqual:
      return "==";
      break;
    case RelationalOperator::kUnEqual:
      return "!=";
      break;
    case RelationalOperator::kGreater:
      return ">";
      break;
    case RelationalOperator::kGreaterEqual:
      return ">=";
      break;
    case RelationalOperator::kSmaller:
      return "<";
      break;
    case RelationalOperator::kSmallerEqual:
      return "<=";
      break;
    default:
      throw std::invalid_argument("Unkown RelationalOperator.");
      return "";
      break;
    }
  }

  AssignFunction assignmentOperation(BaseType::Ptr lhs, const AssignmentOperator &op, const BaseType::Ptr rhs)
  {
    switch (lhs->type_category)
    {
    case TypeCategory::kPrimitiveBool:
      return utils::assignmentOperation(castToBool(lhs), op, rhs);
    case TypeCategory::kPrimitiveInteger:
      return utils::assignmentOperation(castToInteger(lhs), op, rhs);
    case TypeCategory::kPrimitiveDouble:
      return utils::assignmentOperation(castToDouble(lhs), op, rhs);
    case TypeCategory::kPrimitiveString:
      return utils::assignmentOperation(castToString(lhs), op, rhs);
    case TypeCategory::kUserDefinedEnum:
      return utils::assignmentOperation(castToEnum(lhs), op, rhs);
    case TypeCategory::kUserDefinedStruct:
      return utils::assignmentOperation(castToStruct(lhs), op, rhs);
    case TypeCategory::kUserDefinedActor:
      return utils::assignmentOperation(castToActor(lhs), op, rhs);
    default:
      throw utils::InvalidOperatorUsage(lhs->type_category, op, rhs->type_category);
    }
  }

  RelationalFunction relationalOperation(const BaseType::Ptr lhs, const RelationalOperator &op, const BaseType::Ptr rhs)
  {
    switch (lhs->type_category)
    {
    case TypeCategory::kPrimitiveBool:
      return utils::relationalOperation(castToBool(lhs), op, rhs);
    case TypeCategory::kPrimitiveInteger:
      return utils::relationalOperation(castToInteger(lhs), op, rhs);
    case TypeCategory::kPrimitiveDouble:
      return utils::relationalOperation(castToDouble(lhs), op, rhs);
    case TypeCategory::kPrimitiveString:
      return utils::relationalOperation(castToString(lhs), op, rhs);
    case TypeCategory::kUserDefinedEnum:
      return utils::relationalOperation(castToEnum(lhs), op, rhs);
    default:
      throw utils::InvalidOperatorUsage(lhs->type_category, op, rhs->type_category);
    }
  }

  BaseType::Ptr arithmeticOperation(const BaseType::Ptr lhs, const ArithmeticOperator &op, const BaseType::Ptr rhs)
  {
    switch (lhs->type_category)
    {
    case TypeCategory::kPrimitiveInteger:
      return utils::arithmeticOperation(castToInteger(lhs), op, rhs);
    case TypeCategory::kPrimitiveDouble:
      return utils::arithmeticOperation(castToDouble(lhs), op, rhs);
    default:
      throw utils::InvalidOperatorUsage(lhs->type_category, op, rhs->type_category);
    }
  }

  namespace utils
  {
    AssignFunction assignmentOperation(Bool::Ptr lhs, const AssignmentOperator & /*op*/, const BaseType::Ptr rhs)
    {
      Bool::Ptr casted_rhs = castToBool(rhs);
      AssignFunction assign_functor = [lhs, casted_rhs]() -> BaseType::Ptr
      {
        lhs->setFunction()(casted_rhs->getFunction()());
        return lhs;
      };
      return assign_functor;
    }

    AssignFunction assignmentOperation(Integer::Ptr lhs, const AssignmentOperator & /*op*/, const BaseType::Ptr rhs)
    {
      Integer::Ptr casted_rhs = castToInteger(rhs);
      AssignFunction assign_functor = [lhs, casted_rhs]() -> BaseType::Ptr
      {
        lhs->setFunction()(casted_rhs->getFunction()());
        return lhs;
      };
      return assign_functor;
    }

    AssignFunction assignmentOperation(Double::Ptr lhs, const AssignmentOperator & /*op*/, const BaseType::Ptr rhs)

    {
      if (rhs->type_category == TypeCategory::kPrimitiveInteger)
      {
        Integer::Ptr casted_rhs = castToInteger(rhs);
        AssignFunction assign_functor = [lhs, casted_rhs]() -> BaseType::Ptr
        {
          lhs->setFunction()(casted_rhs->getFunction()());
          return lhs;
        };
        return assign_functor;
      }
      else
      {
        Double::Ptr casted_rhs = castToDouble(rhs);
        AssignFunction assign_functor = [lhs, casted_rhs]() -> BaseType::Ptr
        {
          lhs->setFunction()(casted_rhs->getFunction()());
          return lhs;
        };
        return assign_functor;
      }
    }

    AssignFunction assignmentOperation(String::Ptr lhs, const AssignmentOperator & /*op*/, const BaseType::Ptr rhs)
    {
      String::Ptr casted_rhs = castToString(rhs);
      AssignFunction assign_functor = [lhs, casted_rhs]() -> BaseType::Ptr
      {
        lhs->setFunction()(casted_rhs->getFunction()());
        return lhs;
      };
      return assign_functor;
    }

    AssignFunction assignmentOperation(Enum::Ptr lhs, const AssignmentOperator &op, const BaseType::Ptr rhs)
    {
      if (!isConvertible(rhs, lhs))
      {
        throw utils::InvalidOperatorUsage(lhs->type_category, op, rhs->type_category);
      }
      Enum::Ptr casted_rhs = castToEnum(rhs);
      AssignFunction assign_functor = [lhs, casted_rhs]() -> BaseType::Ptr
      {
        lhs->setFunction()(casted_rhs->getFunction()());
        return lhs;
      };
      return assign_functor;
    }

    AssignFunction assignmentOperation(Struct::Ptr lhs, const AssignmentOperator &op, const BaseType::Ptr rhs)
    {
      if (!isConvertible(rhs, lhs))
      {
        throw utils::InvalidOperatorUsage(lhs->type_category, op, rhs->type_category);
      }
      Struct::Ptr casted_rhs = castToStruct(rhs);

      AssignFunction assign_functor = [lhs, op, casted_rhs]() -> BaseType::Ptr
      {
        for (const Field &lhs_field : lhs->getAccessibleFields())
        {
          NestedKeys lhs_key;
          lhs_key.push(lhs_field.first);
          assignmentOperation(lhs_field.second, op, casted_rhs->getField(lhs_key));
        }
        return lhs;
      };
      return assign_functor;
    }

    AssignFunction assignmentOperation(Actor::Ptr lhs, const AssignmentOperator &op, const BaseType::Ptr rhs)
    {
      if (!isConvertible(rhs, lhs))
      {
        throw utils::InvalidOperatorUsage(lhs->type_category, op, rhs->type_category);
      }
      Actor::Ptr casted_rhs = castToActor(rhs);

      AssignFunction assign_functor = [lhs, op, casted_rhs]() -> BaseType::Ptr
      {
        for (const Field &lhs_field : lhs->getAccessibleFields())
        {
          NestedKeys lhs_key;
          lhs_key.push(lhs_field.first);
          assignmentOperation(lhs_field.second, op, casted_rhs->getField(lhs_key));
        }
        return lhs;
      };
      return assign_functor;
    }

    RelationalFunction relationalOperation(const Bool::Ptr lhs, const RelationalOperator &op, const BaseType::Ptr rhs)
    {
      switch (op)
      {
      case RelationalOperator::kEqual:
      {
        Bool::Ptr casted_rhs = castToBool(rhs);
        return [lhs, casted_rhs]() -> bool
        {
          return (lhs->getFunction()() == casted_rhs->getFunction()());
        };
      }
      case RelationalOperator::kUnEqual:
      {
        Bool::Ptr casted_rhs = castToBool(rhs);
        return [lhs, casted_rhs]() -> bool
        {
          return (lhs->getFunction()() != casted_rhs->getFunction()());
        };
      }
      default:
        throw InvalidOperatorUsage(lhs->type_category, op, rhs->type_category);
      }
    }

    RelationalFunction relationalOperation(const Integer::Ptr lhs, const RelationalOperator &op, const BaseType::Ptr rhs)
    {
      switch (op)
      {
      case RelationalOperator::kEqual:
      {
        if (rhs->type_category == TypeCategory::kPrimitiveInteger)
        {
          Integer::Ptr casted_rhs = castToInteger(rhs);
          return [lhs, casted_rhs]() -> bool
          {
            return (lhs->getFunction()() == casted_rhs->getFunction()());
          };
        }
        else
        {
          Double::Ptr casted_rhs = castToDouble(rhs);
          return [lhs, casted_rhs]() -> bool
          {
            // Comparsion within double error bound
            return (std::fabs(lhs->getFunction()() - casted_rhs->getFunction()()) < std::numeric_limits<double>::epsilon());
          };
        }
      }
      case RelationalOperator::kUnEqual:
      {
        if (rhs->type_category == TypeCategory::kPrimitiveInteger)
        {
          Integer::Ptr casted_rhs = castToInteger(rhs);
          return [lhs, casted_rhs]() -> bool
          {
            return (lhs->getFunction()() != casted_rhs->getFunction()());
          };
        }
        else
        {
          Double::Ptr casted_rhs = castToDouble(rhs);
          return [lhs, casted_rhs]() -> bool
          {
            // Comparsion within double error bound
            return (std::fabs(lhs->getFunction()() - casted_rhs->getFunction()()) > std::numeric_limits<double>::epsilon());
          };
        }
      }
      case RelationalOperator::kGreater:
      {
        if (rhs->type_category == TypeCategory::kPrimitiveInteger)
        {
          Integer::Ptr casted_rhs = castToInteger(rhs);
          return [lhs, casted_rhs]() -> bool
          {
            return (lhs->getFunction()() > casted_rhs->getFunction()());
          };
        }
        else
        {
          Double::Ptr casted_rhs = castToDouble(rhs);
          return [lhs, casted_rhs]() -> bool
          {
            // TODO make double comparision?
            return (lhs->getFunction()() > casted_rhs->getFunction()());
          };
        }
      }
      case RelationalOperator::kSmaller:
      {
        if (rhs->type_category == TypeCategory::kPrimitiveInteger)
        {
          Integer::Ptr casted_rhs = castToInteger(rhs);
          return [lhs, casted_rhs]() -> bool
          {
            return (lhs->getFunction()() < casted_rhs->getFunction()());
          };
        }
        else
        {
          Double::Ptr casted_rhs = castToDouble(rhs);
          return [lhs, casted_rhs]() -> bool
          {
            // TODO make double comparision?
            return (lhs->getFunction()() < casted_rhs->getFunction()());
          };
        }
      }
      case RelationalOperator::kGreaterEqual:
      {
        if (rhs->type_category == TypeCategory::kPrimitiveInteger)
        {
          Integer::Ptr casted_rhs = castToInteger(rhs);
          return [lhs, casted_rhs]() -> bool
          {
            return (lhs->getFunction()() >= casted_rhs->getFunction()());
          };
        }
        else
        {
          Double::Ptr casted_rhs = castToDouble(rhs);
          return [lhs, casted_rhs]() -> bool
          {
            // TODO make double comparision?
            return (lhs->getFunction()() >= casted_rhs->getFunction()());
          };
        }
      }
      case RelationalOperator::kSmallerEqual:
      {
        if (rhs->type_category == TypeCategory::kPrimitiveInteger)
        {
          Integer::Ptr casted_rhs = castToInteger(rhs);
          return [lhs, casted_rhs]() -> bool
          {
            return (lhs->getFunction()() <= casted_rhs->getFunction()());
          };
        }
        else
        {
          Double::Ptr casted_rhs = castToDouble(rhs);
          return [lhs, casted_rhs]() -> bool
          {
            // TODO make double comparision?
            return (lhs->getFunction()() <= casted_rhs->getFunction()());
          };
        }
      }
      default:
        throw InvalidOperatorUsage(lhs->type_category, op, rhs->type_category);
      }
    }

    RelationalFunction relationalOperation(const Double::Ptr lhs, const RelationalOperator &op, const BaseType::Ptr rhs)
    {
      switch (op)
      {
      case RelationalOperator::kEqual:
      {
        if (rhs->type_category == TypeCategory::kPrimitiveInteger)
        {
          Integer::Ptr casted_rhs = castToInteger(rhs);
          return [lhs, casted_rhs]() -> bool
          {
            // Comparsion within double error bound
            return (std::fabs(lhs->getFunction()() - casted_rhs->getFunction()()) < std::numeric_limits<double>::epsilon());
          };
        }
        else
        {
          Double::Ptr casted_rhs = castToDouble(rhs);
          return [lhs, casted_rhs]() -> bool
          {
            // Comparsion within double error bound
            return (std::fabs(lhs->getFunction()() - casted_rhs->getFunction()()) < std::numeric_limits<double>::epsilon());
          };
        }
      }
      case RelationalOperator::kUnEqual:
      {
        if (rhs->type_category == TypeCategory::kPrimitiveInteger)
        {
          Integer::Ptr casted_rhs = castToInteger(rhs);
          return [lhs, casted_rhs]() -> bool
          {
            // Comparsion within double error bound
            return (std::fabs(lhs->getFunction()() - casted_rhs->getFunction()()) > std::numeric_limits<double>::epsilon());
          };
        }
        else
        {
          Double::Ptr casted_rhs = castToDouble(rhs);
          return [lhs, casted_rhs]() -> bool
          {
            // Comparsion within double error bound
            return (std::fabs(lhs->getFunction()() - casted_rhs->getFunction()()) > std::numeric_limits<double>::epsilon());
          };
        }
      }
      case RelationalOperator::kGreater:
      {
        if (rhs->type_category == TypeCategory::kPrimitiveInteger)
        {
          Integer::Ptr casted_rhs = castToInteger(rhs);
          return [lhs, casted_rhs]() -> bool
          {
            // TODO make double comparision?
            return (lhs->getFunction()() > casted_rhs->getFunction()());
          };
        }
        else
        {
          Double::Ptr casted_rhs = castToDouble(rhs);
          return [lhs, casted_rhs]() -> bool
          {
            // TODO make double comparision?
            return (lhs->getFunction()() > casted_rhs->getFunction()());
          };
        }
      }
      case RelationalOperator::kSmaller:
      {
        if (rhs->type_category == TypeCategory::kPrimitiveInteger)
        {
          Integer::Ptr casted_rhs = castToInteger(rhs);
          return [lhs, casted_rhs]() -> bool
          {
            // TODO make double comparision?
            return (lhs->getFunction()() < casted_rhs->getFunction()());
          };
        }
        else
        {
          Double::Ptr casted_rhs = castToDouble(rhs);
          return [lhs, casted_rhs]() -> bool
          {
            // TODO make double comparision?
            return (lhs->getFunction()() < casted_rhs->getFunction()());
          };
        }
      }
      case RelationalOperator::kGreaterEqual:
      {
        if (rhs->type_category == TypeCategory::kPrimitiveInteger)
        {
          Integer::Ptr casted_rhs = castToInteger(rhs);
          return [lhs, casted_rhs]() -> bool
          {
            // TODO make double comparision?
            return (lhs->getFunction()() >= casted_rhs->getFunction()());
          };
        }
        else
        {
          Double::Ptr casted_rhs = castToDouble(rhs);
          return [lhs, casted_rhs]() -> bool
          {
            // TODO make double comparision?
            return (lhs->getFunction()() >= casted_rhs->getFunction()());
          };
        }
      }
      case RelationalOperator::kSmallerEqual:
      {
        if (rhs->type_category == TypeCategory::kPrimitiveInteger)
        {
          Integer::Ptr casted_rhs = castToInteger(rhs);
          return [lhs, casted_rhs]() -> bool
          {
            // TODO make double comparision?
            return (lhs->getFunction()() <= casted_rhs->getFunction()());
          };
        }
        else
        {
          Double::Ptr casted_rhs = castToDouble(rhs);
          return [lhs, casted_rhs]() -> bool
          {
            // TODO make double comparision?
            return (lhs->getFunction()() <= casted_rhs->getFunction()());
          };
        }
      }
      default:
        throw InvalidOperatorUsage(lhs->type_category, op, rhs->type_category);
      }
    }

    RelationalFunction relationalOperation(const String::Ptr lhs, const RelationalOperator &op, const BaseType::Ptr rhs)
    {
      switch (op)
      {
      case RelationalOperator::kEqual:
      {
        String::Ptr casted_rhs = castToString(rhs);
        return [lhs, casted_rhs]() -> bool
        {
          return (lhs->getFunction()() == casted_rhs->getFunction()());
        };
      }
      case RelationalOperator::kUnEqual:
      {
        String::Ptr casted_rhs = castToString(rhs);
        return [lhs, casted_rhs]() -> bool
        {
          return (lhs->getFunction()() != casted_rhs->getFunction()());
        };
      }
      default:
        throw InvalidOperatorUsage(lhs->type_category, op, rhs->type_category);
      }
    }

    RelationalFunction relationalOperation(const Enum::Ptr lhs, const RelationalOperator &op, const BaseType::Ptr rhs)
    {
      switch (op)
      {
      case RelationalOperator::kEqual:
      {
        Enum::Ptr casted_rhs = castToEnum(rhs);
        return [lhs, casted_rhs]() -> bool
        {
          return (lhs->getFunction()() == casted_rhs->getFunction()());
        };
      }
      case RelationalOperator::kUnEqual:
      {
        Enum::Ptr casted_rhs = castToEnum(rhs);
        return [lhs, casted_rhs]() -> bool
        {
          return (lhs->getFunction()() != casted_rhs->getFunction()());
        };
      }
      default:
        throw InvalidOperatorUsage(lhs->type_category, op, rhs->type_category);
      }
    }

    BaseType::Ptr arithmeticOperation(const Integer::Ptr lhs, const ArithmeticOperator &op, const BaseType::Ptr rhs)
    {
      switch (op)
      {
      case ArithmeticOperator::kAddition:
      {
        if (rhs->type_category == TypeCategory::kPrimitiveInteger)
        {
          Integer::Ptr casted_rhs = castToInteger(rhs);
          return std::make_shared<IntegerRValue>([lhs, casted_rhs]() -> double
                                                 { return (lhs->getFunction()() + casted_rhs->getFunction()()); });
        }
        else
        {
          Double::Ptr casted_rhs = castToDouble(rhs);
          return std::make_shared<DoubleRValue>([lhs, casted_rhs]() -> double
                                                { return (lhs->getFunction()() + casted_rhs->getFunction()()); });
        }
      }
      case ArithmeticOperator::kSubtraction:
      {
        if (rhs->type_category == TypeCategory::kPrimitiveInteger)
        {
          Integer::Ptr casted_rhs = castToInteger(rhs);
          return std::make_shared<IntegerRValue>([lhs, casted_rhs]() -> double
                                                 { return (lhs->getFunction()() - casted_rhs->getFunction()()); });
        }
        else
        {
          Double::Ptr casted_rhs = castToDouble(rhs);
          return std::make_shared<DoubleRValue>([lhs, casted_rhs]() -> double
                                                { return (lhs->getFunction()() - casted_rhs->getFunction()()); });
        }
      }
      case ArithmeticOperator::kMultiplication:
      {
        if (rhs->type_category == TypeCategory::kPrimitiveInteger)
        {
          Integer::Ptr casted_rhs = castToInteger(rhs);
          return std::make_shared<IntegerRValue>([lhs, casted_rhs]() -> double
                                                 { return (lhs->getFunction()() * casted_rhs->getFunction()()); });
        }
        else
        {
          Double::Ptr casted_rhs = castToDouble(rhs);
          return std::make_shared<DoubleRValue>([lhs, casted_rhs]() -> double
                                                { return (lhs->getFunction()() * casted_rhs->getFunction()()); });
        }
      }
      case ArithmeticOperator::kDivision:
      {
        // NOTE: Even rhs is an Integer, the result will be a double
        if (rhs->type_category == TypeCategory::kPrimitiveInteger)
        {
          Integer::Ptr casted_rhs = castToInteger(rhs);
          return std::make_shared<DoubleRValue>([lhs, casted_rhs]() -> double
                                                { return (lhs->getFunction()() / casted_rhs->getFunction()()); });
        }
        else
        {
          Double::Ptr casted_rhs = castToDouble(rhs);
          return std::make_shared<DoubleRValue>([lhs, casted_rhs]() -> double
                                                { return (lhs->getFunction()() / casted_rhs->getFunction()()); });
        }
      }
      default:
        throw InvalidOperatorUsage(lhs->type_category, op, rhs->type_category);
      }
    }

    BaseType::Ptr arithmeticOperation(const Double::Ptr lhs, const ArithmeticOperator &op, const BaseType::Ptr rhs)
    {
      switch (op)
      {
      case ArithmeticOperator::kAddition:
      {
        if (rhs->type_category == TypeCategory::kPrimitiveInteger)
        {
          Integer::Ptr casted_rhs = castToInteger(rhs);
          return std::make_shared<DoubleRValue>([lhs, casted_rhs]() -> double
                                                { return (lhs->getFunction()() + casted_rhs->getFunction()()); });
        }
        else
        {
          Double::Ptr casted_rhs = castToDouble(rhs);
          return std::make_shared<DoubleRValue>([lhs, casted_rhs]() -> double
                                                { return (lhs->getFunction()() + casted_rhs->getFunction()()); });
        }
      }
      case ArithmeticOperator::kSubtraction:
      {
        if (rhs->type_category == TypeCategory::kPrimitiveInteger)
        {
          Integer::Ptr casted_rhs = castToInteger(rhs);
          return std::make_shared<DoubleRValue>([lhs, casted_rhs]() -> double
                                                { return (lhs->getFunction()() - casted_rhs->getFunction()()); });
        }
        else
        {
          Double::Ptr casted_rhs = castToDouble(rhs);
          return std::make_shared<DoubleRValue>([lhs, casted_rhs]() -> double
                                                { return (lhs->getFunction()() - casted_rhs->getFunction()()); });
        }
      }
      case ArithmeticOperator::kMultiplication:
      {
        if (rhs->type_category == TypeCategory::kPrimitiveInteger)
        {
          Integer::Ptr casted_rhs = castToInteger(rhs);
          return std::make_shared<DoubleRValue>([lhs, casted_rhs]() -> double
                                                { return (lhs->getFunction()() * casted_rhs->getFunction()()); });
        }
        else
        {
          Double::Ptr casted_rhs = castToDouble(rhs);
          return std::make_shared<DoubleRValue>([lhs, casted_rhs]() -> double
                                                { return (lhs->getFunction()() * casted_rhs->getFunction()()); });
        }
      }
      case ArithmeticOperator::kDivision:
      {
        if (rhs->type_category == TypeCategory::kPrimitiveInteger)
        {
          Integer::Ptr casted_rhs = castToInteger(rhs);
          return std::make_shared<DoubleRValue>([lhs, casted_rhs]() -> double
                                                { return (lhs->getFunction()() / casted_rhs->getFunction()()); });
        }
        else
        {
          Double::Ptr casted_rhs = castToDouble(rhs);
          return std::make_shared<DoubleRValue>([lhs, casted_rhs]() -> double
                                                { return (lhs->getFunction()() / casted_rhs->getFunction()()); });
        }
      }
      default:
        throw InvalidOperatorUsage(lhs->type_category, op, rhs->type_category);
      }
    }

    IntegerRValue::IntegerRValue(const Integer::GetFunction functor)
        : Integer(
              std::move(functor), [](int) -> void
              { throw std::runtime_error("Invalid invocation of method [getFunction] of IntegerRValue."); },
              []() -> BaseType::Ptr
              { throw std::runtime_error("Invalid invocation of method [clone] of IntegerRValue."); })
    {
    }

    DoubleRValue::DoubleRValue(const Double::GetFunction functor)
        : Double(
              std::move(functor), [](int) -> void
              { throw std::runtime_error("Invalid invocation of method [getFunction] of DoubleRValue."); },
              []() -> BaseType::Ptr
              { throw std::runtime_error("Invalid invocation of method [clone] of DoubleRValue."); })
    {
    }

  } // namespace utils
} // namespace yase
