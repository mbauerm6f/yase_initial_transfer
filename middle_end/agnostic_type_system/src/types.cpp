/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "agnostic_type_system/types.h"

// tODO check all headers
#include <algorithm>
#include <iostream>
#include <string>
#include <memory>
#include <map>
#include <exception>
#include <memory>
#include <functional>

#include <agnostic_type_system/type_trait.h>
#include <agnostic_type_system/cast.h>

namespace yase
{
  std::string typeCategoryToString(const TypeCategory &category)
  {
    switch (category)
    {
    case TypeCategory::kPrimitiveBool:
      return "Bool";
      break;
    case TypeCategory::kPrimitiveInteger:
      return "Integer";
      break;
    case TypeCategory::kPrimitiveDouble:
      return "Double";
      break;
    case TypeCategory::kPrimitiveString:
      return "String";
      break;
    case TypeCategory::kUserDefinedEnum:
      return "Enum";
      break;
    case TypeCategory::kUserDefinedStruct:
      return "Struct";
      break;
    case TypeCategory::kUserDefinedActor:
      return "Actor";
      break;
    default:
      throw std::invalid_argument("Unkown typeCategory.");
      break;
    }
    return "";
  }

  BaseType::BaseType(const TypeCategory &category) : type_category(category) {}

  std::ostream &operator<<(std::ostream &os, const BaseType::Ptr &type)
  {
    if (type == nullptr)
    {
      return os << "[yase::NULL]";
    }

    switch (type->type_category)
    {
    case TypeCategory::kPrimitiveBool:
      return os << *castToBool(type);
    case TypeCategory::kPrimitiveInteger:
      return os << *castToInteger(type);
    case TypeCategory::kPrimitiveDouble:
      return os << *castToDouble(type);
    case TypeCategory::kPrimitiveString:
      return os << *castToString(type);
    case TypeCategory::kUserDefinedEnum:
      return os << *castToEnum(type);
    case TypeCategory::kUserDefinedStruct:
      return os << *castToStruct(type);
    case TypeCategory::kUserDefinedActor:
      return os << *castToActor(type);
    default:
      std::string error_msg = "Error while printing Type: Unknown type category: ";
      error_msg.append(std::to_string(static_cast<int>(type->type_category)));
      throw std::runtime_error(error_msg);
    }
  }

  std::ostream &operator<<(std::ostream &os, const Bool &type)
  {
    return os << "[yase::Bool] = ";
    try
    {
      if (type.getFunction()() == true)
      {
        return os << "true";
      }
      return os << "false";
    }
    catch (...)
    {
      return os << "NULL";
    }
  }

  std::ostream &operator<<(std::ostream &os, const Integer &type)
  {
    os << "[yase::Integer] = ";
    try
    {
      return os << type.getFunction()();
    }
    catch (...)
    {
      return os << "NULL";
    }
  }

  std::ostream &operator<<(std::ostream &os, const Double &type)
  {
    os << "[yase::Double] = ";
    try
    {
      return os << type.getFunction()();
    }
    catch (...)
    {
      return os << "NULL";
    }
  }

  std::ostream &operator<<(std::ostream &os, const String &type)
  {
    return os << "[yase::String] = ";
    try
    {
      return os << "\"" << type.getFunction()() << "\"";
    }
    catch (...)
    {
      return os << " = NULL";
    }
  }

  TypeDictionary::TypeDictionary(const TypeDictionary::Map &init_map)
  {
    for (const auto &field : init_map)
    {
      add(field.first, field.second);
    }
  }

  bool TypeDictionary::contains(const std::string &key) const
  {
    return (map_.find(key) != map_.end());
  }

  void TypeDictionary::add(const std::string &key, BaseType::Ptr type)
  {
    add(std::make_pair(key, type));
  }

  void TypeDictionary::add(const Field &field)
  {
    if (contains(field.first))
    {
      std::string error_msg = "Duplicate insertion of field identifier [";
      error_msg.append(field.first);
      error_msg.append("] in TypeDictionary.");
      throw std::runtime_error(error_msg);
    }
    map_.insert(std::make_pair(field.first, field.second));
  }

  BaseType::Ptr TypeDictionary::get(const std::string &key) const
  {
    if (!contains(key))
    {
      std::string error_msg = "Requested field identifier [";
      error_msg.append(key);
      error_msg.append("] does not exist in TypeDictionary.");
      throw std::runtime_error(error_msg);
    }
    return map_.find(key)->second;
  }

  const TypeDictionary::Map &TypeDictionary::getMap() const
  {
    return map_;
  }

  TypeDictionary TypeDictionary::clone() const
  {
    TypeDictionary cloned_dict;
    for (const auto &field : map_)
    {
      cloned_dict.add(std::pair<std::string, BaseType::Ptr>(field.first, field.second->clone()));
    }
    return cloned_dict;
  }

  UserDefined::UserDefined(const TypeCategory &type_category, const std::string &user_defined_type_name) : BaseType(type_category), user_defined_name(user_defined_type_name) {}

  Enum::Enum(const std::string &type_name, const GetFunction &get_function, const SetFunction &set_function)
      : UserDefined(TypeCategory::kUserDefinedEnum, type_name), get_function_(get_function), set_function_(set_function) {}

  Enum::GetFunction Enum::getFunction() const
  {
    return get_function_;
  }

  Enum::SetFunction Enum::setFunction()
  {
    return set_function_;
  }

  std::ostream &operator<<(std::ostream &os, const Enum &type)
  {
    return os << "[yase::Enum::" << type.user_defined_name << "] = ";
    try
    {
      return os << type.user_defined_name << "::" << type.getFunction()();
    }
    catch (...)
    {
      return os << "NULL";
    }
  }

  UnsyncedEnum::UnsyncedEnum(const std::string &user_defined_name, const Enum::List enumerator_list)
      : Enum(
            user_defined_name, [this]() -> std::string
            { return this->value_; },
            [this](const std::string &new_value) -> void
            {
              if (enum_list.find(new_value) == enum_list.end())
              {
                std::string error_msg = "Enumerator category [";
                error_msg.append(new_value);
                error_msg.append("] is not available in Enum [");
                error_msg.append(this->user_defined_name);
                error_msg.append("].");
                throw std::runtime_error(error_msg);
              }
              this->value_ = new_value;
            }),
        enum_list(std::move(enumerator_list))
  {
    // Set first value as default
    setFunction()(*(enum_list.begin()));
  }

  BaseType::Ptr UnsyncedEnum::clone() const
  {
    return std::make_shared<UnsyncedEnum>(user_defined_name, enum_list);
  }

  Enum::List UnsyncedEnum::getEnumeratorList() const
  {
    return enum_list;
  }

  Compound::Compound(const TypeCategory &type_category,
                     const std::string &user_defined_type_name,
                     const TypeDictionary &init_field_map)
      : UserDefined(type_category, user_defined_type_name),
        local_fields_(init_field_map) {}

  Compound::Compound(const TypeCategory &type_category,
                     const std::string &user_defined_type_name,
                     const TypeDictionary &init_field_map,
                     const Compound::Ptr inherited_base)
      : UserDefined(type_category, user_defined_type_name),
        local_fields_(init_field_map),
        base_(std::dynamic_pointer_cast<Compound>(inherited_base->clone())) {}

  BaseType::Ptr Compound::getField(const std::string &key) const
  {
    const auto accessible_fields = getAccessibleFields();
    auto field_it = accessible_fields.find(key);
    if (field_it != accessible_fields.end())
    {
      return field_it->second;
    }

    std::string err_msg = "ERROR: Compound data type [";
    err_msg.append(typeCategoryToString(type_category));
    err_msg.append("] has no field [");
    err_msg.append(key);
    err_msg.append("].");
    throw std::invalid_argument(err_msg);
  }

  BaseType::Ptr Compound::getField(const NestedKeys &full_key) const
  {
    // Get front key and remaining key
    const std::string front_key = full_key.front();
    NestedKeys remaining_key = full_key;
    remaining_key.pop();

    const auto accessible_fields = getAccessibleFields();
    auto field_it = accessible_fields.find(front_key);
    if (field_it != accessible_fields.end())
    {
      // If no deeper search is needed, return direct, otherwise dig deeper
      if (remaining_key.empty())
      {
        return field_it->second;
      }
      return castToCompound(field_it->second)->getField(remaining_key);
    }

    std::string err_msg = "ERROR: Compound data type [";
    err_msg.append(typeCategoryToString(type_category));
    err_msg.append("] has no field [");
    err_msg.append(front_key);
    err_msg.append("].");
    throw std::invalid_argument(err_msg);
  }

  TypeDictionary::Map Compound::getAccessibleFields() const
  {
    TypeDictionary::Map fields = local_fields_.getMap();
    if (base_)
    {
      TypeDictionary::Map base_fields = base_->getAccessibleFields();
      fields.insert(base_fields.begin(), base_fields.end());
    }
    return fields;
  }

  TypeDictionary::Map Compound::getInheritanceLocalFields() const
  {
    return local_fields_.getMap();
  }

  std::deque<std::string> Compound::getInheritanceOrder() const
  {
    std::deque<std::string> inheritance_order;
    if (base_)
    {
      inheritance_order = base_->getInheritanceOrder();
    }
    inheritance_order.push_front(user_defined_name);
    return inheritance_order;
  }

  Struct::Struct(const std::string &type_name, const TypeDictionary &init_field_map)
      : Compound(TypeCategory::kUserDefinedStruct, type_name, std::move(init_field_map))
  {
  }

  Struct::Struct(const std::string &type_name, const TypeDictionary &init_field_map, const Struct::Ptr inherited_base)
      : Compound(TypeCategory::kUserDefinedStruct, type_name, std::move(init_field_map), std::move(inherited_base))
  {
    // Check recursively if derived from own base
    std::deque<std::string> inheritance_order = base_->getInheritanceOrder();
    for (const auto &next_base : inheritance_order)
    {
      if (next_base == type_name)
      {
        throw utils::InvalidInheritanceFromOwnBase(TypeCategory::kUserDefinedStruct, type_name);
      }
    }

    // Check for duplicate fields
    for (const auto &field : local_fields_.getMap())
    {
      NestedKeys field_key;
      field_key.push(field.first);
      if (utils::fieldExistsInTypeDictionary(base_->getAccessibleFields(), field_key))
      {
        throw utils::InvalidInheritanceDuplicateField(TypeCategory::kUserDefinedStruct, type_name, field.first);
      }
    }
  }

  BaseType::Ptr Struct::clone() const
  {
    if (base_)
    {
      return std::make_shared<Struct>(user_defined_name, local_fields_.clone(), std::dynamic_pointer_cast<Struct>(base_->clone()));
    }
    return std::make_shared<Struct>(user_defined_name, local_fields_.clone());
  }

  std::ostream &operator<<(std::ostream &os, const Struct &type)
  {
    os << "[yase::Struct::" << type.user_defined_name << "] = {";
    for (const auto &iter : type.getAccessibleFields())
    {
      os << " " << iter.first << ": " << iter.second << ";";
    }
    return os << " }";
  }

  Actor::Actor(const std::string &type_name, const TypeDictionary &init_field_map)
      : Compound(TypeCategory::kUserDefinedActor, type_name, std::move(init_field_map)) {}

  Actor::Actor(const std::string &type_name, const TypeDictionary &init_field_map, const Actor::Ptr inherited_base)
      : Compound(TypeCategory::kUserDefinedActor, type_name, std::move(init_field_map), std::dynamic_pointer_cast<Actor>(inherited_base->clone()))
  {
    // Check recursively if derived from own base
    std::deque<std::string> inheritance_order = base_->getInheritanceOrder();
    for (const auto &next_base : inheritance_order)
    {
      if (next_base == type_name)
      {
        throw utils::InvalidInheritanceFromOwnBase(TypeCategory::kUserDefinedActor, type_name);
      }
    }

    // Check for duplicate fields
    for (const auto &field : local_fields_.getMap())
    {
      NestedKeys field_key;
      field_key.push(field.first);
      if (utils::fieldExistsInTypeDictionary(base_->getAccessibleFields(), field_key))
      {
        throw utils::InvalidInheritanceDuplicateField(TypeCategory::kUserDefinedActor, type_name, field.first);
      }
    }
  }

  BaseType::Ptr Actor::clone() const
  {
    if (base_)
    {
      return std::make_shared<Actor>(user_defined_name, local_fields_.clone(), std::dynamic_pointer_cast<Actor>(base_->clone()));
    }
    return std::make_shared<Actor>(user_defined_name, local_fields_.clone());
  }

  void Actor::initInSimulator()
  {
    std::string error_msg;
    error_msg.append("Method initInSimulator() is not defined for [Actor::");
    error_msg.append(user_defined_name);
    error_msg.append("].");
    throw std::runtime_error(error_msg);
  }

  void Actor::updateInSimulator()
  {
    std::string error_msg;
    error_msg.append("Method updateInSimulator() is not defined for [Actor::");
    error_msg.append(user_defined_name);
    error_msg.append("].");
    throw std::runtime_error(error_msg);
  }

  std::ostream &operator<<(std::ostream &os, const Actor &type)
  {
    os << "[yase::Actor::" << type.user_defined_name << "] = {";
    for (const auto &iter : type.getAccessibleFields())
    {
      os << " " << iter.first << ": " << iter.second << ";";
    }
    return os << " }";
  }

  namespace utils
  {
    bool fieldExistsInTypeDictionary(const TypeDictionary::Map &field_map, const NestedKeys &full_key)
    {
      // Empty keys are not allowed
      if (full_key.empty())
      {
        return false;
      }

      // Get front key and remaining key
      std::string front_key = full_key.front();
      NestedKeys remaining_key = full_key;
      remaining_key.pop();

      auto field_it = field_map.find(front_key);

      // Not found
      if (field_it == field_map.end())
      {
        return false;
      }

      // Found --> Check if nested nested field exists
      try
      {
        if (!remaining_key.empty())
        {
          castToCompound(field_it->second)->getField(remaining_key);
        }
      }
      catch (...)
      {
        return false;
      }
      return true;
    }

  } // namespace utils
} // namespace yase
