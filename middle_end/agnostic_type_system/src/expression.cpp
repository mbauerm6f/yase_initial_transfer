/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "agnostic_type_system/expression.h"

#include <iterator>

namespace yase
{

  ArithmeticExpression::ArithmeticExpression(const BaseType::Ptr init_start_operand) : start_operand(init_start_operand) {}

  ArithmeticExpression::ArithmeticExpression(const BaseType::Ptr init_start_operand, const OperatorSequence &trailing_operator_sequence) : start_operand(init_start_operand), operation_sequence(trailing_operator_sequence) {}

  ArithmeticExpression &ArithmeticExpression::append(const ArithmeticOperator &next_op, const BaseType::Ptr next_operand)
  {
    operation_sequence.push_back(std::make_pair(next_op, next_operand));
    return *this;
  }

  ArithmeticExpression &ArithmeticExpression::append(const TrailingOperation &trailing_operation)
  {
    operation_sequence.push_back(trailing_operation);
    return *this;
  }

  BaseType::Ptr ArithmeticExpression::prepareArithmeticFunction() const
  {
    // If only one operation, solve it directly
    if (operation_sequence.size() == 1)
    {
      return arithmeticOperation(start_operand, operation_sequence.front().first, operation_sequence.front().second);
    }

    // If Multiplication/Division exists, solve first found operation instance and solve rest in next expression recursively
    // first operand must handled explicitly
    if ((operation_sequence.front().first == ArithmeticOperator::kMultiplication) || (operation_sequence.front().first == ArithmeticOperator::kMultiplication))
    {
      BaseType::Ptr next_start_operand = arithmeticOperation(start_operand, operation_sequence.front().first, operation_sequence.front().second);
      ArithmeticExpression next_expression{next_start_operand};
      for (size_t it = 1; it < operation_sequence.size(); it++)
      {
        next_expression.append(operation_sequence[it].first, operation_sequence[it].second);
      }
      return next_expression.prepareArithmeticFunction();
    }

    // Iterate over the rest
    for (size_t it = 1; it < operation_sequence.size(); it++)
    {

      if ((operation_sequence[it].first == ArithmeticOperator::kMultiplication) || (operation_sequence[it].first == ArithmeticOperator::kMultiplication))
      {
        BaseType::Ptr combined_operand = arithmeticOperation(operation_sequence[it - 1].second, operation_sequence[it].first, operation_sequence[it].second);
        ArithmeticExpression next_expression{start_operand};
        for (size_t it_2 = 0; it_2 < operation_sequence.size(); it_2++)
        {
          if (it_2 == it - 1)
          {
            next_expression.append(operation_sequence[it_2].first, combined_operand);
          }
          else if (it_2 == it)
          {
            // ignore that iterator
          }
          else
          {
            next_expression.append(operation_sequence[it_2].first, operation_sequence[it_2].second);
          }
        }
        return next_expression.prepareArithmeticFunction();
      }
    }
    // Only Addition/Subtractions -> solve it iteratively
    BaseType::Ptr r_value_operand = operation_sequence.back().second;
    for (int it = operation_sequence.size() - 1; it > 0; it--)
    {
      r_value_operand = arithmeticOperation(operation_sequence[it - 1].second, operation_sequence[it].first, r_value_operand);
    }
    return arithmeticOperation(start_operand, operation_sequence.begin()->first, r_value_operand);
  }

  std::ostream &operator<<(std::ostream &os, const ArithmeticExpression &expression)
  {
    os << expression.start_operand;
    for (const auto &iter : expression.operation_sequence)
    {
      os << " " << arithmeticOperationToString(iter.first) << " " << iter.second;
    }
    return os;
  }

} // namespace yase
