/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "agnostic_type_system/type_signature.h"

#include <exception>
#include <iostream>

#include <agnostic_type_system/operator.h>
#include <agnostic_type_system/type_trait.h>

namespace yase
{

  InputArguments::Arg::Arg(const std::string &arg_identifer, const BaseType::Ptr arg_type)
      : identifer(arg_identifer), type(arg_type) {}

  void InputArguments::pushBackArg(BaseType::Ptr input_arg, const std::string &arg_identifier)
  {
    if (!arg_identifier.empty())
    {
      // Check if argument identifer was used before
      for (const Arg &existing_arg : args_)
      {
        if (existing_arg.identifer == arg_identifier)
        {
          std::string error_msg = "SyntaxError: Duplicate argument [";
          error_msg.append(arg_identifier);
          error_msg.append("] in InputArguments.");
          throw std::invalid_argument(error_msg);
        }
      }
    }
    else
    {
      // Check if there exist nominal arguments before (positional not allowed after nominal!)
      for (const Arg &previous_arg : args_)
      {
        if (!previous_arg.identifer.empty())
        {
          std::string error_msg = "SyntaxError: Positional argument follows keyword argument in InputArguments.";
          throw std::invalid_argument(error_msg);
        }
      }
    }

    Arg new_arg(arg_identifier, input_arg);
    args_.push_back(new_arg);
  }

  std::vector<InputArguments::Arg> InputArguments::getArgumentList() const
  {
    return args_;
  }

  void MatchedArguments::pushBackInputArg(BaseType::Ptr input_arg, const std::string &arg_identifier)
  {
    if (arg_identifier.empty())
    {
      throw std::invalid_argument("Error while adding matched input argument - Empty argument identifer not allowed.");
    }
    else if (input_args_.find(arg_identifier) != input_args_.end())
    {
      std::string error_msg = "Error while adding matched input argument - Argument identifier [";
      error_msg.append(arg_identifier);
      error_msg.append("] already exits.");
      throw std::invalid_argument(error_msg);
    }
    input_args_.insert(std::make_pair(arg_identifier, input_arg));
  }

  void MatchedArguments::pushBackReturnArg(BaseType::Ptr return_arg)
  {
    return_args_.push_back(return_arg);
  }

  std::set<std::string> MatchedArguments::getAvailableInputArgIdentifiers() const
  {
    std::set<std::string> input_arg_identifiers;
    for (const auto &input_arg : input_args_)
    {
      input_arg_identifiers.insert(input_arg.first);
    }
    return input_arg_identifiers;
  }

  BaseType::Ptr MatchedArguments::getInputArg(const std::string &arg_identifier) const
  {
    if (arg_identifier.empty())
    {
      throw std::invalid_argument("Error while getting matched input argument - Empty argument identifer not allowed.");
    }
    auto input_arg_it = input_args_.find(arg_identifier);
    if (input_arg_it == input_args_.end())
    {
      std::string error_msg = "Error while getting matched input argument - Argument [";
      error_msg.append(arg_identifier);
      error_msg.append("] does not exist.");
      throw std::invalid_argument(error_msg);
    }
    return input_arg_it->second;
  }

  std::vector<BaseType::Ptr> MatchedArguments::getReturnArgList() const
  {
    return return_args_;
  }

  TypeSignatureFactory::Param::Param(const std::string &param_identifer, const BaseType::Ptr param_type, const bool is_default_value)
      : identifer(param_identifer), type(param_type), is_default_value(is_default_value) {}

  void TypeSignatureFactory::insertInputParam(const std::string &param_identifier, BaseType::Ptr param_type, const bool is_default)
  {
    // Check if param identifer was used before
    for (const Param &existing_param : input_params_)
    {
      if (existing_param.identifer == param_identifier)
      {
        std::string error_msg = "SyntaxError: Duplicate argument [";
        error_msg.append(param_identifier);
        error_msg.append("] in TypeSignatureFactory.");
        throw std::invalid_argument(error_msg);
      }
    }

    // Check if non-default argument follows default argument
    if (!is_default)
    {
      for (const Param &existing_param : input_params_)
      {
        if (existing_param.is_default_value)
        {
          throw std::invalid_argument("SyntaxError: Non-default argument follows default argument.");
        }
      }
    }

    Param new_param(param_identifier, param_type, is_default);
    input_params_.push_back(new_param);
  }

  void TypeSignatureFactory::pushBackReturnParam(BaseType::Ptr return_param)
  {
    return_params_.push_back(return_param);
  }

  MatchedArguments TypeSignatureFactory::matchWith(const InputArguments &input_args) const
  {
    MatchedArguments matched_args;
    // Check if input arguments match
    if (input_args.getArgumentList().size() > input_params_.size())
    {
      std::string error_msg = "TypeSignatureFactory takes ";
      error_msg.append(std::to_string(input_params_.size()));
      error_msg.append(" arguments but ");
      error_msg.append(std::to_string(input_args.getArgumentList().size()));
      error_msg.append(" were given.");
      throw std::invalid_argument(error_msg);
    }

    // Assign all arguments
    std::set<std::string> arguments_given_by_input;
    for (size_t input_pos = 0; input_pos < input_args.getArgumentList().size(); input_pos++)
    {
      std::string current_identifer;
      size_t current_signature_pos;
      // Positional argument
      if (input_args.getArgumentList()[input_pos].identifer.empty())
      {
        // Positional argument -> Identifier and position are the same for input and type signature
        current_signature_pos = input_pos;
        current_identifer = input_params_[input_pos].identifer;
      }
      else
      {
        // Nominal argument -> Position of input and type signature differs
        current_identifer = input_args.getArgumentList()[input_pos].identifer;
        bool identifier_exists = false;
        for (size_t signature_pos = 0; signature_pos < input_params_.size(); signature_pos++)
        {
          if (input_params_[signature_pos].identifer == current_identifer)
          {
            identifier_exists = true;
            current_signature_pos = signature_pos;
          }
        }
        if (!identifier_exists)
        {
          std::string error_msg = "Error: Got unexpected keyword argument [";
          error_msg.append(current_identifer);
          error_msg.append("] in type signature.");
          throw std::invalid_argument(error_msg);
        }
      }

      // Check if argument was already assigned
      for (const std::string &existing_identifier : arguments_given_by_input)
      {
        if (existing_identifier == current_identifer)
        {
          std::string error_msg = "Got multiple values for argument [";
          error_msg.append(current_identifer);
          error_msg.append("].");
          throw std::invalid_argument(error_msg);
        }
      }

      // Assign by reference if suitable
      if (!isLhsSubTypeOfRhs(input_args.getArgumentList()[input_pos].type, input_params_[current_signature_pos].type))
      {
        std::string error_msg = "Input argument [";
        error_msg.append(current_identifer);
        error_msg.append("] of type [");
        error_msg.append(typeCategoryToString(input_args.getArgumentList()[input_pos].type->type_category));
        error_msg.append("] is not an adequate (sub) type of type [");
        error_msg.append(typeCategoryToString(input_params_[current_signature_pos].type->type_category));
        error_msg.append("] defined in the TypeSignatureFactory.");
        throw std::invalid_argument(error_msg);
      }
      matched_args.pushBackInputArg(input_args.getArgumentList()[input_pos].type, current_identifer);
      arguments_given_by_input.insert(current_identifer);
    }

    // Check if missing arguments can be filled with default values
    for (const auto &arg : input_params_)
    {
      if (arguments_given_by_input.find(arg.identifer) == arguments_given_by_input.end())
      {
        if (!arg.is_default_value)
        {
          std::string error_msg = "Missing required argument [";
          error_msg.append(arg.identifer);
          error_msg.append("] which is neither given by input, nor set with default value.");
          throw std::invalid_argument(error_msg);
        }
        matched_args.pushBackInputArg(arg.type->clone(), arg.identifer);
      }
    }

    // Insert return params
    for (const BaseType::Ptr &return_param : return_params_)
    {
      matched_args.pushBackReturnArg(return_param->clone());
    }

    return matched_args;
  }

  bool TypeSignatureFactory::matches(const InputArguments &input_list) const
  {
    try
    {
      MatchedArguments matched_args = matchWith(input_list);
    }
    catch (...)
    {
      return false;
    }
    return true;
  }

  std::vector<TypeSignatureFactory::Param> TypeSignatureFactory::getInputParams() const
  {
    return input_params_;
  }

  std::vector<BaseType::Ptr> TypeSignatureFactory::getReturnParams() const
  {
    return return_params_;
  }

  std::ostream &operator<<(std::ostream &os, const TypeSignatureFactory &type_signature)
  {
    os << "(";

    // Print input params
    bool first = true;
    for (const auto &input_param : type_signature.getInputParams())
    {
      if (!first)
      {
        os << ", ";
        first = false;
      }
      os << input_param.identifer << ": ";
      os << input_param.type;
      if (input_param.is_default_value == true)
      {
        os << " = [default defined]";
      }
    }
    os << ") -> ";
    // Print return params
    if (type_signature.getReturnParams().empty())
    {
      os << "None";
    }
    else
    {
      for (const auto &return_param : type_signature.getReturnParams())
      {
        if (!first)
        {
          os << ", ";
          first = false;
        }
        os << return_param;
      }
    }

    return os;
  }

} // namespace yase
