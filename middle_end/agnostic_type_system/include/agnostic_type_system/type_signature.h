/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef AGNOSTIC_TYPE_SYSTEM_TYPE_SIGNATURE_H
#define AGNOSTIC_TYPE_SYSTEM_TYPE_SIGNATURE_H

#include <functional>
#include <string>
#include <unordered_map>
#include <vector>

#include <agnostic_type_system/types.h>

namespace yase
{

  // Given arguments to instantiate a function/ method /subroutine
  class InputArguments
  {
  public:
    struct Arg
    {
      Arg(const std::string &arg_identifer, const BaseType::Ptr arg_type);

      const std::string identifer;
      const BaseType::Ptr type;
    };

    // Can throw if argument identifier was used before
    void pushBackArg(BaseType::Ptr input_arg, const std::string &arg_identifier = "");

    std::vector<Arg> getArgumentList() const;

  private:
    std::vector<Arg> args_;
  };

  // Forward declaration needed in MatchedArguments
  class TypeSignatureFactory;

  // Arguments, which are checked against a type signature
  class MatchedArguments
  {
  public:
    // Returns set of all available input argument identifiers
    std::set<std::string> getAvailableInputArgIdentifiers() const;

    // Access matched input argument with its identifier
    BaseType::Ptr getInputArg(const std::string &arg_identifier) const;

    // Returns ordered return arguments
    std::vector<BaseType::Ptr> getReturnArgList() const;

  private:
    // Only TypeSignatureFactory is allowed to fill arguments by matching InputArguments against a type signature
    friend TypeSignatureFactory;
    void pushBackInputArg(BaseType::Ptr input_arg, const std::string &arg_identifier);
    void pushBackReturnArg(BaseType::Ptr return_arg);

    std::unordered_map<std::string, BaseType::Ptr> input_args_;
    std::vector<BaseType::Ptr> return_args_;
  };

  // TypeSignature of a function/ method /subroutine.
  // This factory can generate MatchedArguments with InputArguments.
  // Hereby the InputArguments are checked against the TypeSignature and filled with default values if needed.
  class TypeSignatureFactory
  {
  public:
    struct Param
    {
      Param(const std::string &param_identifer, const BaseType::Ptr param_type, const bool is_default_value = false);

      const std::string identifer;
      BaseType::Ptr type;
      bool is_default_value{false};
    };

    // Can throw if argument identifier was used before
    void insertInputParam(const std::string &param_identifier, BaseType::Ptr param_type, const bool is_default = false);

    // Pushes additional return param at the end
    void pushBackReturnParam(BaseType::Ptr return_param);

    // Checks InputArguments against Typesignature and returns final MatchedArguments
    MatchedArguments matchWith(const InputArguments &input_args) const;

    // Check if all non default values are given and types matches
    bool matches(const InputArguments &input_args) const;

    std::vector<Param> getInputParams() const;
    std::vector<BaseType::Ptr> getReturnParams() const;

  private:
    std::vector<Param> input_params_;

    std::vector<BaseType::Ptr> return_params_;
  };

  std::ostream &operator<<(std::ostream &os, const TypeSignatureFactory &type_signature);

} // namespace yase

#endif // AGNOSTIC_TYPE_SYSTEM_TYPE_SIGNATURE_H