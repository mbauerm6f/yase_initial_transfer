/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef AGNOSTIC_TYPE_SYSTEM_CAST_H
#define AGNOSTIC_TYPE_SYSTEM_CAST_H

#include <agnostic_type_system/types.h>

namespace yase
{
  // Direct cast into C++ native types
  bool castToCppBool(const BaseType::Ptr type);
  int castToCppInteger(const BaseType::Ptr type);
  double castToCppDouble(const BaseType::Ptr type);
  std::string castToCppString(const BaseType::Ptr type);

  // Cast to specific type
  Bool::Ptr castToBool(const BaseType::Ptr type);
  Integer::Ptr castToInteger(const BaseType::Ptr type);
  Double::Ptr castToDouble(const BaseType::Ptr type);
  String::Ptr castToString(const BaseType::Ptr type);
  UserDefined::Ptr castToUserDefined(const BaseType::Ptr type);
  Compound::Ptr castToCompound(const BaseType::Ptr type);
  Enum::Ptr castToEnum(const BaseType::Ptr type);
  Struct::Ptr castToStruct(const BaseType::Ptr type);
  Actor::Ptr castToActor(const BaseType::Ptr type);

  namespace utils
  {
    class InvalidTypeCategoryCast : public std::runtime_error
    {
    public:
      InvalidTypeCategoryCast(const TypeCategory &from,
                              const TypeCategory &to) : std::runtime_error(std::string("Invalid cast from type category [yase::")
                                                                               .append(typeCategoryToString(from)
                                                                                           .append("] to type category [yase::")
                                                                                           .append(typeCategoryToString(to).append("].")))) {}

      InvalidTypeCategoryCast(const TypeCategory &from,
                              const std::string &to) : std::runtime_error(std::string("Invalid cast from type category [yase::")
                                                                              .append(typeCategoryToString(from)
                                                                                          .append("] to type category [yase::")
                                                                                          .append(to)
                                                                                          .append("]."))) {}
    };
  }

} // namespace yase

#endif // AGNOSTIC_TYPE_SYSTEM_CAST_H