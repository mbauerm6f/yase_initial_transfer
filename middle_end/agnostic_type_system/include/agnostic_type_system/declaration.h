/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef AGNOSTIC_TYPE_SYSTEM_DECLARATION_H
#define AGNOSTIC_TYPE_SYSTEM_DECLARATION_H

#include <agnostic_type_system/types.h>
#include <agnostic_type_system/type_signature.h>

namespace yase
{

  // This declaration serves is the template from which type instances can be instantiated
  class UserDefinedTypeDeclaration
  {
  public:
    UserDefinedTypeDeclaration(const UserDefined::Ptr type_template,
                               const TypeSignatureFactory &type_ctor_signature = TypeSignatureFactory());

    // Instantiate a type instance from type template with argument list
    BaseType::Ptr instantiate(const InputArguments &input_args);

    std::string getUserDefinedName();
    TypeCategory getTypeCategory();

    const TypeSignatureFactory ctor_signature;

  private:
    // Type template is private to prevent direct clones
    const UserDefined::Ptr type_template_;
  };

} // namespace yase

#endif // AGNOSTIC_TYPE_SYSTEM_DECLARATION_H