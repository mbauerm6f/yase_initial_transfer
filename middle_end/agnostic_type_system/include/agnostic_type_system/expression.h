/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef AGNOSTIC_TYPE_SYSTEM_EXPRESSION_H
#define AGNOSTIC_TYPE_SYSTEM_EXPRESSION_H

#include <vector>

#include <agnostic_type_system/types.h>
#include <agnostic_type_system/operator.h>

namespace yase
{
  // An ArithmeticExpression is a sequence of operations which respects the order of operations rules
  class ArithmeticExpression
  {
  public:
    using TrailingOperation = std::pair<const ArithmeticOperator, const BaseType::Ptr>;
    using OperatorSequence = std::vector<TrailingOperation>;

    ArithmeticExpression(const BaseType::Ptr init_start_operand);
    ArithmeticExpression(const BaseType::Ptr init_start_operand, const OperatorSequence &trailing_operator_sequence);

    ArithmeticExpression &append(const ArithmeticOperator &next_op, const BaseType::Ptr next_operand);

    ArithmeticExpression &append(const TrailingOperation &trailing_operation);

    // Solves the expression (while ensuring order of operations) to a callablearithmetic funcion
    // Returns rvalue, from which the getfunction() can be called to solve expression.
    BaseType::Ptr prepareArithmeticFunction() const;

    friend std::ostream &operator<<(std::ostream &os, const ArithmeticExpression &expression);

  private:
    const BaseType::Ptr start_operand;
    OperatorSequence operation_sequence;
  };

} // namespace yase

#endif // AGNOSTIC_TYPE_SYSTEM_EXPRESSION_H