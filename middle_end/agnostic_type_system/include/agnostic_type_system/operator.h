/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef AGNOSTIC_TYPE_SYSTEM_OPERATOR_H
#define AGNOSTIC_TYPE_SYSTEM_OPERATOR_H

#include <exception>
#include <string>
#include <memory>

#include <agnostic_type_system/types.h>

namespace yase
{
  // Currently only the direct assignment is supported
  enum class AssignmentOperator
  {
    kDirectAssignment
  };
  std::string assignmentOperationToString(const AssignmentOperator &op);

  enum class RelationalOperator
  {
    kEqual,
    kUnEqual,
    kSmaller,
    kGreater,
    kSmallerEqual,
    kGreaterEqual
  };
  std::string relationalOperationToString(const RelationalOperator &op);

  enum class ArithmeticOperator
  {
    kAddition,
    kSubtraction,
    kMultiplication,
    kDivision
  };
  std::string arithmeticOperationToString(const ArithmeticOperator &op);

  // Assignment functions follow: T& T::operator =(const T2& b);
  using AssignFunction = std::function<BaseType::Ptr()>;

  using RelationalFunction = std::function<bool()>;

  // Returns a Assign function, calling the function assigns the values
  AssignFunction assignmentOperation(BaseType::Ptr lhs, const AssignmentOperator &op, const BaseType::Ptr rhs);

  // Returns a relational function, calling the function evaluates the relational operation
  RelationalFunction relationalOperation(const BaseType::Ptr lhs, const RelationalOperator &op, const BaseType::Ptr rhs);

  // Returns a RValue, executing the getFunction triggers the calculation
  BaseType::Ptr arithmeticOperation(const BaseType::Ptr lhs, const ArithmeticOperator &op, const BaseType::Ptr rhs);

  namespace utils
  {
    AssignFunction assignmentOperation(Bool::Ptr lhs, const AssignmentOperator &op, const BaseType::Ptr rhs);
    AssignFunction assignmentOperation(Integer::Ptr lhs, const AssignmentOperator &op, const BaseType::Ptr rhs);
    AssignFunction assignmentOperation(Double::Ptr lhs, const AssignmentOperator &op, const BaseType::Ptr rhs);
    AssignFunction assignmentOperation(String::Ptr lhs, const AssignmentOperator &op, const BaseType::Ptr rhs);
    AssignFunction assignmentOperation(Enum::Ptr lhs, const AssignmentOperator &op, const BaseType::Ptr rhs);
    AssignFunction assignmentOperation(Struct::Ptr lhs, const AssignmentOperator &op, const BaseType::Ptr rhs);
    AssignFunction assignmentOperation(Actor::Ptr lhs, const AssignmentOperator &op, const BaseType::Ptr rhs);

    RelationalFunction relationalOperation(const Bool::Ptr lhs, const RelationalOperator &op, const BaseType::Ptr rhs);
    RelationalFunction relationalOperation(const Integer::Ptr lhs, const RelationalOperator &op, const BaseType::Ptr rhs);
    RelationalFunction relationalOperation(const Double::Ptr lhs, const RelationalOperator &op, const BaseType::Ptr rhs);
    RelationalFunction relationalOperation(const String::Ptr lhs, const RelationalOperator &op, const BaseType::Ptr rhs);
    RelationalFunction relationalOperation(const Enum::Ptr lhs, const RelationalOperator &op, const BaseType::Ptr rhs);

    BaseType::Ptr arithmeticOperation(const Integer::Ptr lhs, const ArithmeticOperator &op, const BaseType::Ptr rhs);
    BaseType::Ptr arithmeticOperation(const Double::Ptr lhs, const ArithmeticOperator &op, const BaseType::Ptr rhs);

    // IntegerRValue - Do not use it other then in arithmetics
    class IntegerRValue : public Integer
    {
    public:
      IntegerRValue(const Integer::GetFunction functor);
    };

    // DoubleRValue - Do not use it other then in arithmetics
    class DoubleRValue : public Double
    {
    public:
      DoubleRValue(const Double::GetFunction functor);
    };

    class InvalidOperatorUsage : public std::runtime_error
    {
    public:
      InvalidOperatorUsage(const TypeCategory &lhs,
                           const AssignmentOperator &op,
                           const TypeCategory &rhs) : std::runtime_error(std::string("Invalid usage of assignment operator [")
                                                                             .append(assignmentOperationToString(op))
                                                                             .append("] for left hand side [yase::")
                                                                             .append(typeCategoryToString(lhs).append("] and right hand side [yase::").append(typeCategoryToString(rhs).append("].")))) {}

      InvalidOperatorUsage(const TypeCategory &lhs,
                           const RelationalOperator &op,
                           const TypeCategory &rhs) : std::runtime_error(std::string("Invalid usage of relational operator [")
                                                                             .append(relationalOperationToString(op))
                                                                             .append("] for left hand side [yase::")
                                                                             .append(typeCategoryToString(lhs).append("] and right hand side [yase::").append(typeCategoryToString(rhs).append("].")))) {}

      InvalidOperatorUsage(const TypeCategory &lhs,
                           const ArithmeticOperator &op,
                           const TypeCategory &rhs) : std::runtime_error(std::string("Invalid usage of arithmetic operator [")
                                                                             .append(arithmeticOperationToString(op))
                                                                             .append("] for left hand side [yase::")
                                                                             .append(typeCategoryToString(lhs).append("] and right hand side [yase::").append(typeCategoryToString(rhs).append("].")))) {}
    };
  }

} // namespace yase

#endif // AGNOSTIC_TYPE_SYSTEM_OPERATOR_H