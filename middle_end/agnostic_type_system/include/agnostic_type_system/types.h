/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef AGNOSTIC_TYPE_SYSTEM_TYPES_H
#define AGNOSTIC_TYPE_SYSTEM_TYPES_H

#include <exception>
#include <functional>
#include <set>
#include <string>
#include <unordered_map>
#include <queue>
#include <memory>
#include <vector>

// ############## Type Categories ##############

namespace yase
{
  enum class TypeCategory
  {
    kPrimitiveBool,
    kPrimitiveInteger,
    kPrimitiveDouble,
    kPrimitiveString,
    kUserDefinedEnum,
    kUserDefinedStruct,
    kUserDefinedActor
  };

  std::string typeCategoryToString(const TypeCategory &category);

  // ############## Forward declarations ##############

  template <class T, TypeCategory category>
  class Primitive;

  using Bool = Primitive<bool, TypeCategory::kPrimitiveBool>;
  using Integer = Primitive<int, TypeCategory::kPrimitiveInteger>;
  using Double = Primitive<double, TypeCategory::kPrimitiveDouble>;
  using String = Primitive<std::string, TypeCategory::kPrimitiveString>;

  class UserDefined;
  class Compound;

  class Enum;
  class Struct;
  class Actor;

  // ############## BaseType ##############

  // A BaseType::Ptr acts like a void pointer, but has the safety of a shared_ptr and and type info
  class BaseType
  {
  public:
    using Ptr = std::shared_ptr<BaseType>;

    BaseType() = default;
    virtual ~BaseType() = default;
    BaseType(const BaseType &copy_from) = delete;
    BaseType &operator=(const BaseType &copy_from) = delete;
    BaseType(BaseType &&) = delete;
    BaseType &operator=(BaseType &&) = delete;

    virtual BaseType::Ptr clone() const = 0;

    const TypeCategory type_category;

  private:
    // Hidden Ctor, can only be accessed by friend classes
    BaseType(const TypeCategory &category);

    friend Bool;
    friend Integer;
    friend Double;
    friend String;
    friend UserDefined;
  };

  std::ostream &operator<<(std::ostream &os, const BaseType::Ptr &type);

  // ############## Primitives ##############

  // Primitive template, from which Bool, Integer, Double and String are generated from
  template <class T, TypeCategory category>
  class Primitive : public BaseType
  {
  public:
    using Ptr = std::shared_ptr<Primitive>;

    using GetFunction = std::function<T()>;
    using SetFunction = std::function<void(T)>;
    using CloneFunction = std::function<BaseType::Ptr()>;

    Primitive(const GetFunction &get_function, const SetFunction &set_function, const CloneFunction &clone_function)
        : BaseType(category), get_function_(get_function), set_function_(set_function), clone_function_(clone_function) {}

    GetFunction getFunction() const
    {
      return this->get_function_;
    }
    SetFunction setFunction()
    {
      return this->set_function_;
    }
    BaseType::Ptr clone() const final
    {
      return this->clone_function_();
    }

  private:
    const GetFunction get_function_;
    const SetFunction set_function_;
    const CloneFunction clone_function_;
  };

  std::ostream &operator<<(std::ostream &os, const Bool &type);
  std::ostream &operator<<(std::ostream &os, const Integer &type);
  std::ostream &operator<<(std::ostream &os, const Double &type);
  std::ostream &operator<<(std::ostream &os, const String &type);

  // PrimitiveValue - Holds a concrete primitive value which exists nowhere else
  template <class T, TypeCategory category>
  class Value : public Primitive<T, category>
  {
  public:
    Value(T value) : Primitive<T, category>(std::bind(&Value::get, this),
                                            std::bind(&Value::set, this, std::placeholders::_1),
                                            std::bind(&Value::clone, this)),
                     value_(value) {}

  private:
    T &get() { return value_; }
    T &set(const T new_value) { value_ = new_value; }
    BaseType::Ptr clone() { return std::make_shared<Value<T, category>>(get()); }

    T value_;
  };

  using BoolValue = Value<bool, TypeCategory::kPrimitiveBool>;
  using IntegerValue = Value<int, TypeCategory::kPrimitiveInteger>;
  using DoubleValue = Value<double, TypeCategory::kPrimitiveDouble>;
  using StringValue = Value<std::string, TypeCategory::kPrimitiveString>;

  // ############## Lists and Dictionaries ##############
  // Currently they are are not BaseTypes, but this might change

  using TypeList = std::vector<BaseType::Ptr>;

  // A deque of keys
  // Example: my_var.nested_field_a --> ["my_var" , "nested_field", "a"]
  using NestedKeys = std::queue<std::string>;

  // Field: Identifier key with type instance
  using Field = std::pair<std::string, BaseType::Ptr>;

  // The TypeDictionary is a wrapper around a type map with convenience methods
  class TypeDictionary
  {
  public:
    using Map = std::unordered_map<std::string, BaseType::Ptr>;

    TypeDictionary() = default;
    ~TypeDictionary() = default;

    TypeDictionary(const Map &map);

    TypeDictionary clone() const;

    bool contains(const std::string &key) const;

    void add(const std::string &key, BaseType::Ptr type);
    void add(const Field &field);

    BaseType::Ptr get(const std::string &key) const;

    const Map &getMap() const;

  private:
    Map map_;
  };

  // ############## UserDefined types ##############

  class UserDefined : public BaseType
  {
  public:
    using Ptr = std::shared_ptr<UserDefined>;

    // Specific name of user defined type
    std::string user_defined_name;

  private:
    // Ctor is only visible for friends
    UserDefined(const TypeCategory &type_category, const std::string &user_defined_type_name);
    friend Enum;
    friend Compound;
  };

  class Enum : public UserDefined
  {
  public:
    using Ptr = std::shared_ptr<Enum>;
    using List = std::set<std::string>;

    using GetFunction = std::function<std::string()> const;
    using SetFunction = std::function<void(std::string)>;

    Enum(const std::string &type_name, const GetFunction &get_function, const SetFunction &set_function);

    GetFunction getFunction() const;
    SetFunction setFunction();

    virtual List getEnumeratorList() const = 0;

  private:
    const GetFunction get_function_;
    const SetFunction set_function_;
  };

  std::ostream &operator<<(std::ostream &os, const Enum &type);

  // UnsyncedEnum - Enum value exists only in scenario and is not synchronized with underlying simulator
  class UnsyncedEnum : public Enum
  {
  public:
    UnsyncedEnum(const std::string &type_name, const List enumerator_list);

    BaseType::Ptr clone() const final;

    List getEnumeratorList() const final;

    const List enum_list;

  private:
    std::string value_;
  };

  // A compound data type can have nested fields within
  class Compound : public UserDefined
  {
  public:
    using Ptr = std::shared_ptr<Compound>;

    // Get field of compound type
    BaseType::Ptr getField(const std::string &key) const;

    // Get nested field of compound type
    BaseType::Ptr getField(const NestedKeys &key) const;

    // Order from this instance to base
    std::deque<std::string> getInheritanceOrder() const;

    // Returns map with all accessible fields (with inherited base fields)
    TypeDictionary::Map getAccessibleFields() const;

    // Gets the fields of this inheritance level (without inherited base fields)
    TypeDictionary::Map getInheritanceLocalFields() const;

  private:
    // Ctor is only visible for friends
    Compound(const TypeCategory &type_category,
             const std::string &user_defined_type_name,
             const TypeDictionary &init_field_map);

    Compound(const TypeCategory &type_category,
             const std::string &user_defined_type_name,
             const TypeDictionary &init_field_map,
             const Compound::Ptr inherited_base);
    friend Struct;
    friend Actor;

    // Local fields of compound type
    const TypeDictionary local_fields_;

    // Inherited base
    const Compound::Ptr base_{nullptr};
  };

  class Struct : public Compound
  {
  public:
    using Ptr = std::shared_ptr<Struct>;

    Struct(const std::string &type_name, const TypeDictionary &field_map);
    Struct(const std::string &type_name, const TypeDictionary &field_map, const Struct::Ptr inherited_base);

    BaseType::Ptr clone() const;
  };

  std::ostream &operator<<(std::ostream &os, const Struct &type);

  class Actor : public Compound
  {
  public:
    using Ptr = std::shared_ptr<Actor>;

    Actor(const std::string &type_name, const TypeDictionary &field_map);
    Actor(const std::string &type_name, const TypeDictionary &field_map, const Actor::Ptr inherited_base);

    BaseType::Ptr clone() const;

    // Inits an actor in the underlying simulator
    virtual void initInSimulator();

    // Synchronizes the actor with the underlying simulator
    virtual void updateInSimulator();
  };

  std::ostream &operator<<(std::ostream &os, const Actor &type);

  namespace utils
  {
    // Checks if (nested) field exists in field map
    bool fieldExistsInTypeDictionary(const TypeDictionary::Map &field_map, const NestedKeys &full_key);

    class InvalidInheritanceFromOwnBase : public std::runtime_error
    {
    public:
      InvalidInheritanceFromOwnBase(const TypeCategory &user_defined_type, const std::string &user_defined_type_name) : std::runtime_error(std::string("Invalid declaration of [yase::")
                                                                                                                                               .append(typeCategoryToString(user_defined_type).append("::").append(user_defined_type_name).append("]: Cannot inherit from own type."))) {}
    };

    class InvalidInheritanceDuplicateField : public std::runtime_error
    {
    public:
      InvalidInheritanceDuplicateField(const TypeCategory &user_defined_type,
                                       const std::string &user_defined_type_name,
                                       const std::string &duplicate_field)
          : std::runtime_error(std::string("Invalid declaration of [yase::")
                                   .append(typeCategoryToString(user_defined_type).append("::").append(user_defined_type_name).append("]: The field ").append(duplicate_field).append(" is already derived and therefore duplicate."))) {}
    };

  }

} // namespace yase

#endif // AGNOSTIC_TYPE_SYSTEM_TYPES_H