/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef AGNOSTIC_TYPE_SYSTEM_TYPE_TRAIT_H
#define AGNOSTIC_TYPE_SYSTEM_TYPE_TRAIT_H

#include <agnostic_type_system/types.h>

namespace yase
{
  // Performs nominal subtype comparision, the actual values are not compared
  bool isLhsSubTypeOfRhs(const BaseType::Ptr lhs, const BaseType::Ptr &rhs);
  bool isLhsSubTypeOfRhs(const Enum::Ptr lhs, const BaseType::Ptr &rhs);
  bool isLhsSubTypeOfRhs(const Struct::Ptr lhs, const BaseType::Ptr &rhs);
  bool isLhsSubTypeOfRhs(const Actor::Ptr lhs, const BaseType::Ptr &rhs);

  bool isPrimitive(const BaseType::Ptr type);
  bool isCompoundType(const BaseType::Ptr type);
  bool isUserDefinedType(const BaseType::Ptr type);

  bool isConvertible(const BaseType::Ptr &from, const BaseType::Ptr &to);

} // namespace yase

#endif // AGNOSTIC_TYPE_SYSTEM_TYPE_TRAIT_H