/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "gtest/gtest.h"

#include <agnostic_behavior_tree/actions/analyse_nodes.h>
#include <agnostic_behavior_tree/actions/functor_action_node.h>
#include <agnostic_behavior_tree/composite/parallel_node.h>
#include <agnostic_behavior_tree/decorator/constraint_node.h>
#include <agnostic_behavior_tree/decorator/symbol_declaration_node.h>
#include <agnostic_behavior_tree/utils/visitors.h>

namespace yase {

// Defines multiple test instances to test the functionality
struct ConstraintNodeFixture : testing::Test {
  ConstraintNodeFixture(){};
  virtual ~ConstraintNodeFixture(){};

  // ------------- TEST INSTANCES -----------------

  // Exemplary declarer to provide ego params
  class EgoParamInserter : public SymbolDeclarer {
   public:
    // get shared symbol
    void declareSharedSymbols(BlackboardWriter& blackboard) override final {
      blackboard.set<unsigned int>("ego_id", 1);
      blackboard.set<std::shared_ptr<double>>("ego_target_speed", std::make_shared<double>(30.0));
      blackboard.set<std::shared_ptr<const double>>("ego_max_deceleration", std::make_shared<const double>(5.0));
      blackboard.set<std::shared_ptr<bool>>("vehicle_collisions", std::make_shared<bool>(false));
    };
  };

  // Node which returns kSuccess after three ticks
  // Furthermore it alters the collision symbol
  class TestNode : public ActionNode {
   public:
    TestNode() : ActionNode("TestNode"){};

    // This is public for test purposed, normally make it private!
    std::shared_ptr<double> ego_target_speed;

    void onInit() override { m_tick_count = 0; };

   private:
    NodeStatus tick() final {
      m_tick_count++;
      if (m_tick_count > 3) {
        executionInfo("Successful after three ticks.");
        *m_collisions = true;
        return NodeStatus::kSuccess;
      }
      executionInfo("Still Running");
      return NodeStatus::kRunning;
    };

    /// look up required symbol "vehicle_collisions"
    virtual void lookUpSymbolsInBlackBoard(const BlackboardReader& blackboard) override {
      m_collisions = blackboard.get<std::shared_ptr<bool>>("vehicle_collisions");
      ego_target_speed = blackboard.get<std::shared_ptr<double>>("ego_target_speed");
    };

    size_t m_tick_count{0};
    std::shared_ptr<bool> m_collisions;
  };
};

/// -------- TEST CONDITION --------------
// Condition is violated if ego target speed is less then 20 m/s
class ConstraintEgoMinSpeed : public Condition {
 public:
  ConstraintEgoMinSpeed() : Condition("ConstraintEgoMinSpeed"){};

  // Check ego speed
  bool evaluate() final { return (*m_ego_target_speed > 20.0); };

  // Nothing to reset
  void onInit() final{};

  // Get Ego speed from blackboard
  virtual void lookUpSharedSymbols(const BlackboardReader& blackboard) override final {
    m_ego_target_speed = blackboard.get<std::shared_ptr<double>>("ego_target_speed");
  };

 private:
  std::shared_ptr<const double> m_ego_target_speed;
};

class ConstraintEgoMaxSpeed : public Condition {
 public:
  ConstraintEgoMaxSpeed() : Condition("ConstraintEgoMaxSpeed"){};

  // Check ego speed
  bool evaluate() final { return (*m_ego_target_speed < 20.0); };

  // Nothing to reset
  void onInit() final{};

  // Get Ego speed from blackboard
  virtual void lookUpSharedSymbols(const BlackboardReader& blackboard) override final {
    m_ego_target_speed = blackboard.get<std::shared_ptr<double>>("ego_target_speed");
  };

 private:
  std::shared_ptr<const double> m_ego_target_speed;
};

// Test if child is not initialised when the pre constraint is false (Avoid computation!)
TEST_F(ConstraintNodeFixture, test_onInit_with_negative_pre_constraint) {
  /// -------- TEST SETUP --------------
  // Create the declarer
  SymbolDeclarer::Ptr ego_param_declarer(new EgoParamInserter());
  DecoratorNode::Ptr ego_param_declarer_decorator =
      std::make_shared<SymbolDeclarationNode>("EgoParamDeclarer", std::move(ego_param_declarer));

  // Create the constraint
  TimedConstraint pre_constraint_maximal_ego_speed =
      std::make_pair(EvaluationPhase::kPre, std::unique_ptr<Condition>(new ConstraintEgoMaxSpeed()));
  DecoratorNode::Ptr constraint_decorator =
      std::make_shared<ConstraintNode>(std::move(pre_constraint_maximal_ego_speed));

  // Build up the test tree
  std::shared_ptr<AnalyseNode> dummy_node = std::make_shared<AnalyseNode>(1);
  constraint_decorator->setChild(dummy_node);
  ego_param_declarer_decorator->setChild(constraint_decorator);
  ego_param_declarer_decorator->initTree();

  // Init and tick
  EXPECT_EQ(dummy_node->isInitialised(), false);
  // onInit does not yet have an effect on the child, only if pre constraint is evaluated to true
  constraint_decorator->onInit();
  EXPECT_EQ(dummy_node->isInitialised(), false);
  ego_param_declarer_decorator->executeTick();
  EXPECT_EQ(dummy_node->isInitialised(), false);
  EXPECT_EQ(dummy_node->onInitCalls(), 0);
}

// Test if child is initialised AFTER the pre constraint is true
TEST_F(ConstraintNodeFixture, test_onInit_with_positive_pre_constraint) {
  /// -------- TEST SETUP --------------
  // Create the declarer
  SymbolDeclarer::Ptr ego_param_declarer(new EgoParamInserter());
  DecoratorNode::Ptr ego_param_declarer_decorator =
      std::make_shared<SymbolDeclarationNode>("EgoParamDeclarer", std::move(ego_param_declarer));

  // Create the constraint
  TimedConstraint pre_constraint_minimal_ego_speed =
      std::make_pair(EvaluationPhase::kPre, std::unique_ptr<Condition>(new ConstraintEgoMinSpeed()));
  DecoratorNode::Ptr constraint_decorator =
      std::make_shared<ConstraintNode>(std::move(pre_constraint_minimal_ego_speed));

  // Build up the test tree
  std::shared_ptr<AnalyseNode> dummy_node = std::make_shared<AnalyseNode>(1);
  constraint_decorator->setChild(dummy_node);
  ego_param_declarer_decorator->setChild(constraint_decorator);
  ego_param_declarer_decorator->initTree();

  // Init and tick
  EXPECT_EQ(dummy_node->isInitialised(), false);
  // onInit does not yet have an effect on the child, only if pre constraint is evaluated to true
  constraint_decorator->onInit();
  EXPECT_EQ(dummy_node->isInitialised(), false);
  ego_param_declarer_decorator->executeTick();
  EXPECT_EQ(dummy_node->isInitialised(), true);

  // Terminate tree
  ego_param_declarer_decorator->onTerminate();
  EXPECT_EQ(dummy_node->isInitialised(), false);
}

/// Test a init condition --> should be legal
TEST_F(ConstraintNodeFixture, constraint_node_compile_constraint_fulfilled) {
  /// -------- TEST SETUP --------------
  // Create the declarer
  SymbolDeclarer::Ptr ego_param_declarer(new EgoParamInserter());
  DecoratorNode::Ptr ego_param_declarer_decorator =
      std::make_shared<SymbolDeclarationNode>("EgoParamDeclarer", std::move(ego_param_declarer));

  // Create the constraint
  TimedConstraint init_constraint_minimal_ego_speed =
      std::make_pair(EvaluationPhase::kInit, std::unique_ptr<Condition>(new ConstraintEgoMinSpeed()));
  DecoratorNode::Ptr constraint_decorator =
      std::make_shared<ConstraintNode>(std::move(init_constraint_minimal_ego_speed));

  // Build up the test tree
  std::shared_ptr<TestNode> test_node = std::make_shared<TestNode>();
  constraint_decorator->setChild(test_node);
  ego_param_declarer_decorator->setChild(constraint_decorator);

  // Resolve the symbols --> Here the compile conditions are checked!
  EXPECT_NO_THROW(ego_param_declarer_decorator->initTree(););

  // Further check: we violate the condition while runtime --> legal, as condition only exists while compile time!
  EXPECT_EQ(ego_param_declarer_decorator->executeTick(), NodeStatus::kRunning);
  *(test_node->ego_target_speed) = 10.0;
  EXPECT_EQ(ego_param_declarer_decorator->executeTick(), NodeStatus::kRunning);
}

/// Test a init condition --> is violated!
TEST_F(ConstraintNodeFixture, constraint_node_compile_constraint_violated) {
  /// -------- TEST SETUP --------------
  // Create the declarer
  SymbolDeclarer::Ptr ego_param_declarer(new EgoParamInserter());
  DecoratorNode::Ptr ego_param_declarer_decorator =
      std::make_shared<SymbolDeclarationNode>("EgoParamDeclarer", std::move(ego_param_declarer));

  // Create the constraint
  TimedConstraint init_constraint_maximal_ego_speed =
      std::make_pair(EvaluationPhase::kInit, std::unique_ptr<Condition>(new ConstraintEgoMaxSpeed()));
  DecoratorNode::Ptr constraint_decorator =
      std::make_shared<ConstraintNode>(std::move(init_constraint_maximal_ego_speed));

  // Build up the test tree
  std::shared_ptr<TestNode> test_node = std::make_shared<TestNode>();
  constraint_decorator->setChild(test_node);
  ego_param_declarer_decorator->setChild(constraint_decorator);

  // Resolve the symbols --> Here the compile condition is checked and one condition is violated!
  EXPECT_ANY_THROW(ego_param_declarer_decorator->initTree(););
}

/// Test a pre constraint --> should be legal
TEST_F(ConstraintNodeFixture, constraint_node_pre_constraint_fulfilled) {
  /// -------- TEST SETUP --------------
  // Create the declarer
  SymbolDeclarer::Ptr ego_param_declarer(new EgoParamInserter());
  DecoratorNode::Ptr ego_param_declarer_decorator =
      std::make_shared<SymbolDeclarationNode>("EgoParamDeclarer", std::move(ego_param_declarer));

  // Create the constraint
  TimedConstraint pre_constraint_minimal_ego_speed =
      std::make_pair(EvaluationPhase::kPre, std::unique_ptr<Condition>(new ConstraintEgoMinSpeed()));
  DecoratorNode::Ptr constraint_decorator =
      std::make_shared<ConstraintNode>(std::move(pre_constraint_minimal_ego_speed));

  // Build up the test tree
  std::shared_ptr<TestNode> test_node = std::make_shared<TestNode>();
  constraint_decorator->setChild(test_node);
  ego_param_declarer_decorator->setChild(constraint_decorator);

  // Resolve the symbols
  ego_param_declarer_decorator->initTree();

  // TEST:
  EXPECT_EQ(ego_param_declarer_decorator->executeTick(), NodeStatus::kRunning);
  // TEST: Violation of the condition while runtime --> legal, as condition only pre checks the condition!
  *(test_node->ego_target_speed) = 10.0;
  EXPECT_EQ(ego_param_declarer_decorator->executeTick(), NodeStatus::kRunning);
}

/// Tests pre constraint --> is violated!
TEST_F(ConstraintNodeFixture, constraint_node_pre_constraint_violated) {
  /// -------- TEST SETUP --------------
  // Create the declarer
  SymbolDeclarer::Ptr ego_param_declarer(new EgoParamInserter());
  DecoratorNode::Ptr ego_param_declarer_decorator =
      std::make_shared<SymbolDeclarationNode>("EgoParamDeclarer", std::move(ego_param_declarer));

  // Create the constraints
  TimedConstraint pre_constraint_minimal_ego_speed =
      std::make_pair(EvaluationPhase::kPre, std::unique_ptr<Condition>(new ConstraintEgoMinSpeed()));
  DecoratorNode::Ptr constraint_decorator =
      std::make_shared<ConstraintNode>(std::move(pre_constraint_minimal_ego_speed));

  // Build up the test tree
  std::shared_ptr<TestNode> test_node = std::make_shared<TestNode>();
  constraint_decorator->setChild(test_node);
  ego_param_declarer_decorator->setChild(constraint_decorator);

  // Resolve the symbols
  ego_param_declarer_decorator->initTree();

  // TEST: Violation of the condition at first execution --> kFailure
  *(test_node->ego_target_speed) = 10.0;
  EXPECT_EQ(ego_param_declarer_decorator->executeTick(), NodeStatus::kFailure);
}

/// Test a pre constraint --> should be legal
TEST_F(ConstraintNodeFixture, constraint_node_runtime_constraint_fulfilled) {
  /// -------- TEST SETUP --------------
  // Create the declarer
  SymbolDeclarer::Ptr ego_param_declarer(new EgoParamInserter());
  DecoratorNode::Ptr ego_param_declarer_decorator =
      std::make_shared<SymbolDeclarationNode>("EgoParamDeclarer", std::move(ego_param_declarer));

  // Create the constraint
  TimedConstraint runtime_constraint_minimal_ego_speed =
      std::make_pair(EvaluationPhase::kRuntime, std::unique_ptr<Condition>(new ConstraintEgoMinSpeed()));
  DecoratorNode::Ptr constraint_decorator =
      std::make_shared<ConstraintNode>(std::move(runtime_constraint_minimal_ego_speed));

  // Build up the test tree
  std::shared_ptr<TestNode> test_node = std::make_shared<TestNode>();
  constraint_decorator->setChild(test_node);
  ego_param_declarer_decorator->setChild(constraint_decorator);

  // Resolve the symbols
  ego_param_declarer_decorator->initTree();

  // TEST:
  EXPECT_EQ(ego_param_declarer_decorator->executeTick(), NodeStatus::kRunning);
  EXPECT_EQ(ego_param_declarer_decorator->executeTick(), NodeStatus::kRunning);
  EXPECT_EQ(ego_param_declarer_decorator->executeTick(), NodeStatus::kRunning);
  EXPECT_EQ(ego_param_declarer_decorator->executeTick(), NodeStatus::kSuccess);
}

/// Tests runtime condition --> is violated!
TEST_F(ConstraintNodeFixture, constraint_node_runtime_constraint_violated) {
  /// -------- TEST SETUP --------------
  // Create the declarer
  SymbolDeclarer::Ptr ego_param_declarer(new EgoParamInserter());
  DecoratorNode::Ptr ego_param_declarer_decorator =
      std::make_shared<SymbolDeclarationNode>("EgoParamDeclarer", std::move(ego_param_declarer));

  // Create the constraint
  TimedConstraint runtime_constraint_minimal_ego_speed =
      std::make_pair(EvaluationPhase::kRuntime, std::unique_ptr<Condition>(new ConstraintEgoMinSpeed()));
  DecoratorNode::Ptr constraint_decorator =
      std::make_shared<ConstraintNode>(std::move(runtime_constraint_minimal_ego_speed));

  // Build up the test tree
  std::shared_ptr<TestNode> test_node = std::make_shared<TestNode>();
  constraint_decorator->setChild(test_node);
  ego_param_declarer_decorator->setChild(constraint_decorator);

  // Resolve the symbols
  ego_param_declarer_decorator->initTree();

  // TEST: condition fulfilled with first execution
  EXPECT_EQ(ego_param_declarer_decorator->executeTick(), NodeStatus::kRunning);
  // TEST: Violation of a condition while runtime --> kFailure
  *(test_node->ego_target_speed) = 10.0;
  EXPECT_EQ(ego_param_declarer_decorator->executeTick(), NodeStatus::kFailure);
}

/// Test a pre constraint --> should be legal
TEST_F(ConstraintNodeFixture, constraint_node_post_constraint_fulfilled) {
  /// -------- TEST SETUP --------------
  // Create the declarer
  SymbolDeclarer::Ptr ego_param_declarer(new EgoParamInserter());
  DecoratorNode::Ptr ego_param_declarer_decorator =
      std::make_shared<SymbolDeclarationNode>("EgoParamDeclarer", std::move(ego_param_declarer));

  // Create the constraint
  TimedConstraint post_constraint_minimal_ego_speed =
      std::make_pair(EvaluationPhase::kPost, std::unique_ptr<Condition>(new ConstraintEgoMinSpeed()));
  DecoratorNode::Ptr constraint_decorator =
      std::make_shared<ConstraintNode>(std::move(post_constraint_minimal_ego_speed));

  // Build up the test tree
  std::shared_ptr<TestNode> test_node = std::make_shared<TestNode>();
  constraint_decorator->setChild(test_node);
  ego_param_declarer_decorator->setChild(constraint_decorator);

  // Resolve the symbols
  ego_param_declarer_decorator->initTree();

  // TEST: The fourth execution is successfull and the post constraint is also fulfilled --> kSuccess
  EXPECT_EQ(ego_param_declarer_decorator->executeTick(), NodeStatus::kRunning);
  EXPECT_EQ(ego_param_declarer_decorator->executeTick(), NodeStatus::kRunning);
  EXPECT_EQ(ego_param_declarer_decorator->executeTick(), NodeStatus::kRunning);
  EXPECT_EQ(ego_param_declarer_decorator->executeTick(), NodeStatus::kSuccess);
}

/// Tests pre constraints --> one of them is violated!
TEST_F(ConstraintNodeFixture, constraint_node_post_constraint_violated) {
  /// -------- TEST SETUP --------------
  // Create the declarer
  SymbolDeclarer::Ptr ego_param_declarer(new EgoParamInserter());
  DecoratorNode::Ptr ego_param_declarer_decorator =
      std::make_shared<SymbolDeclarationNode>("EgoParamDeclarer", std::move(ego_param_declarer));

  // Create the constraints
  TimedConstraint post_constraint_minimal_ego_speed =
      std::make_pair(EvaluationPhase::kPost, std::unique_ptr<Condition>(new ConstraintEgoMinSpeed()));
  DecoratorNode::Ptr constraint_decorator =
      std::make_shared<ConstraintNode>(std::move(post_constraint_minimal_ego_speed));

  // Build up the test tree
  std::shared_ptr<TestNode> test_node = std::make_shared<TestNode>();
  constraint_decorator->setChild(test_node);
  ego_param_declarer_decorator->setChild(constraint_decorator);

  // Resolve the symbols
  ego_param_declarer_decorator->initTree();

  // TEST: All executions are successful, as constraint is checked only at the end
  EXPECT_EQ(ego_param_declarer_decorator->executeTick(), NodeStatus::kRunning);
  *(test_node->ego_target_speed) = 10.0;
  EXPECT_EQ(ego_param_declarer_decorator->executeTick(), NodeStatus::kRunning);
  EXPECT_EQ(ego_param_declarer_decorator->executeTick(), NodeStatus::kRunning);
  // TEST: The fourth execution is successfull but the post constraint is also violated --> kFailure!
  EXPECT_EQ(ego_param_declarer_decorator->executeTick(), NodeStatus::kFailure);
}

}  // namespace yase
