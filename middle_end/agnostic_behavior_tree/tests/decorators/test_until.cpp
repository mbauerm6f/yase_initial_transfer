/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "gtest/gtest.h"

#include <agnostic_behavior_tree/actions/analyse_nodes.h>
#include <agnostic_behavior_tree/decorator/until_node.h>
#include <agnostic_behavior_tree/utils/condition.h>

namespace yase {

// Evaluates to true after three evaluation calls
class TrueAfterThreeEvaluationCalls : public Condition {
 public:
  explicit TrueAfterThreeEvaluationCalls() : Condition("TrueAfterThreeEvaluationCalls"){};

  void onInit() final { m_evaluation_calls = 0; }

  bool evaluate() final {
    if (m_evaluation_calls >= 3) {
      return true;
    }
    m_evaluation_calls++;
    return false;
  };

 private:
  unsigned int m_evaluation_calls{0};
};

// Test if execution stops after until condition is met
TEST(UntilNodeFixture, until_condition_met) {
  /// -------- TEST SETUP --------------
  // └─ [Decorator::Until::TrueAfterThreeEvaluationCalls]
  //     └─ [Action::AnalyseNodeAlwaysRunning]
  auto end_condition = std::unique_ptr<Condition>(new TrueAfterThreeEvaluationCalls());
  DecoratorNode::Ptr until_decorator = std::make_shared<Until>(std::move(end_condition));
  std::shared_ptr<AnalyseNode> dummy_node = std::make_shared<AlwaysRunning>();
  until_decorator->setChild(dummy_node);

  // Initialise the whole tree and call onInit of root node
  until_decorator->initTree();
  until_decorator->onInit();

  // Tick tree three times - after that the Until decorator should finish the child behavior.
  EXPECT_EQ(dummy_node->isInitialised(), true);
  EXPECT_EQ(until_decorator->executeTick(), NodeStatus::kRunning);
  EXPECT_EQ(until_decorator->executeTick(), NodeStatus::kRunning);
  EXPECT_EQ(until_decorator->executeTick(), NodeStatus::kRunning);
  // Finish
  EXPECT_EQ(until_decorator->executeTick(), NodeStatus::kSuccess);

  // Just double check if child behavior was terminated correctly
  until_decorator->onTerminate();
  EXPECT_EQ(dummy_node->isInitialised(), false);
}

// Test if finishes before until condition is met
TEST(UntilNodeFixture, finished_before_until_condition_met) {
  /// -------- TEST SETUP --------------
  // └─ [Decorator::Until::TrueAfterThreeEvaluationCalls]
  //     └─ [Action::AnalyseNode]
  auto end_condition = std::unique_ptr<Condition>(new TrueAfterThreeEvaluationCalls());
  DecoratorNode::Ptr until_decorator = std::make_shared<Until>(std::move(end_condition));
  std::shared_ptr<AnalyseNode> dummy_node = std::make_shared<AnalyseNode>(1);
  until_decorator->setChild(dummy_node);

  // Initialise the whole tree and call onInit of root node
  until_decorator->initTree();
  until_decorator->onInit();

  // Tick tree three times - after that the Until decorator should finish the child behavior.
  EXPECT_EQ(until_decorator->executeTick(), NodeStatus::kRunning);
  // Finish
  EXPECT_EQ(until_decorator->executeTick(), NodeStatus::kSuccess);

  // Just double check if child behavior was terminated correctly
  until_decorator->onTerminate();
  EXPECT_EQ(dummy_node->isInitialised(), false);
}

}  // namespace yase
