/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "gtest/gtest.h"

#include <agnostic_behavior_tree/actions/analyse_nodes.h>
#include <agnostic_behavior_tree/actions/functor_action_node.h>
#include <agnostic_behavior_tree/composite/parallel_node.h>
#include <agnostic_behavior_tree/decorator/symbol_declaration_node.h>

#include <vector>

namespace yase {

// Defines multiple test instances to test the functionality
struct SymbolDeclarationNodeFixture : testing::Test {
  SymbolDeclarationNodeFixture(){};
  virtual ~SymbolDeclarationNodeFixture(){};

  // ------------- TEST INSTANCES -----------------
  struct RoadDummy {
    unsigned int laneId{0};
  };

  // Exemplary declarer
  class RoadInserter : public SymbolDeclarer {
   public:
    // get shared symbol
    virtual void declareSharedSymbols(BlackboardWriter& blackboard) override final {
      RoadDummy road;
      road.laneId = 707;
      blackboard.set<std::shared_ptr<RoadDummy>>("road", std::make_shared<RoadDummy>(road));
    };
  };

  // Exemplary declarer
  class EgoParamInserter : public SymbolDeclarer {
   public:
    // get shared symbol
    virtual void declareSharedSymbols(BlackboardWriter& blackboard) override final {
      blackboard.set<unsigned int>("ego_id", 1);
      blackboard.set<std::shared_ptr<double>>("ego_target_speed", std::make_shared<double>(30.0));
    };
  };

  // Node which requires access to road
  class TestNodeAccessRoad : public ActionNode {
   public:
    TestNodeAccessRoad() : ActionNode("TestNodeAccessRoad"){};

    void onInit() override{};

   private:
    NodeStatus tick() final {
      if (!m_road) {
        std::cerr << "no road " << std::endl;
      }
      if (m_road->laneId != 707) {
        executionInfo("The road seems not be accessible as expected!");
        return NodeStatus::kFailure;
      }
      executionInfo("All shared symbols can be accesssed.");
      return NodeStatus::kSuccess;
    };

    /// look up required symbol "vehicle_collisions"
    virtual void lookUpSymbolsInBlackBoard(const BlackboardReader& blackboard) override final {
      m_road = blackboard.get<std::shared_ptr<RoadDummy>>("road");
    };

    std::shared_ptr<RoadDummy> m_road;
  };

  // Node which requires access to road and ego params
  class TestNodeAccessAllParams : public ActionNode {
   public:
    TestNodeAccessAllParams() : ActionNode("TestNodeAccessAllParams"){};

    void onInit() override{};

   private:
    virtual NodeStatus tick() final {
      if (m_road->laneId != 707) {
        executionInfo("The road seems not be accessible as expected!");
        return NodeStatus::kFailure;
      }
      if (m_ego_id != 1) {
        executionInfo("The ego_id is not accessible as expected!");
        return NodeStatus::kFailure;
      }
      if (*(m_ego_target_speed) != 30.0) {
        executionInfo("The ego_target_speed is not accessible as expected!");
        return NodeStatus::kFailure;
      }
      executionInfo("All shared symbols can be accesssed.");
      return NodeStatus::kSuccess;
    };

    /// look up required symbol "vehicle_collisions"
    virtual void lookUpSymbolsInBlackBoard(const BlackboardReader& blackboard) override final {
      m_road = blackboard.get<std::shared_ptr<RoadDummy>>("road");
      m_ego_id = blackboard.get<unsigned int>("ego_id");
      m_ego_target_speed = blackboard.get<std::shared_ptr<double>>("ego_target_speed");
    };

    std::shared_ptr<RoadDummy> m_road;
    unsigned int m_ego_id;
    std::shared_ptr<double> m_ego_target_speed;
  };
};

// Legal tree: all data can be accessed
//
//└── [Decorator::SymbolDeclarationNode::RoadDeclarer]
//    Symbol table scope name: Decorator::SymbolDeclarationNode::RoadDeclarer
//    - Key: [road], type: [St10shared_ptrIN3abt28SymbolDeclarationNodeFixture9RoadDummyEE]
//  └── [Composite::Parallel::Unnamed]
//        Symbol table scope name: Composite::Parallel::Unnamed
//      ├── [Action::TestNodeAccessRoad]
//      │     Symbol table scope name: Action::TestNodeAccessRoad
//      └── [Decorator::SymbolDeclarationNode::EgoParamDeclarer]
//            Symbol table scope name: Decorator::SymbolDeclarationNode::EgoParamDeclarer
//            - Key: [ego_target_speed], type: [St10shared_ptrIdE]
//            - Key: [ego_id], type: [St10shared_ptrIjE]
//          └── [Action::TestNodeAccessAllParams]
//                Symbol table scope name: Action::TestNodeAccessAllParams
TEST_F(SymbolDeclarationNodeFixture, symbol_declaration_node_legal_setup) {
  // Top node: makes road everywhere downstream available
  SymbolDeclarer::Ptr road_declarer(new RoadInserter());
  DecoratorNode::Ptr road_declarer_decorator =
      std::make_shared<SymbolDeclarationNode>("RoadDeclarer", std::move(road_declarer));

  // a parallel node to fork the tree into two subtrees (is the child of road_declarer_decorator)
  CompositeNode::Ptr parallel = std::make_shared<ParallelNode>();
  road_declarer_decorator->setChild(parallel);

  // Child 1: needs only access to road
  parallel->addChild(std::make_shared<TestNodeAccessRoad>());

  // Child 2: Declares further variables for subtree
  SymbolDeclarer::Ptr ego_param(new EgoParamInserter);
  DecoratorNode::Ptr ego_param_decorator =
      std::make_shared<SymbolDeclarationNode>("EgoParamDeclarer", std::move(ego_param));
  ego_param_decorator->setChild(std::make_shared<TestNodeAccessAllParams>());
  parallel->addChild(std::move(ego_param_decorator));

  // The the collision will be observed in three ticks, which causes a failure downstream:
  road_declarer_decorator->initTree();
  // init tree multiple times to check if the result is still consistent.
  road_declarer_decorator->initTree();
  road_declarer_decorator->initTree();
  EXPECT_EQ(road_declarer_decorator->executeTick(), NodeStatus::kSuccess);
}

// Illegal tree: not accessible data requested
//
//└── [Decorator::SymbolDeclarationNode::RoadDeclarer]
//    Symbol table scope name: Decorator::SymbolDeclarationNode::RoadDeclarer
//    - Key: [road], type: [St10shared_ptrIN3abt28SymbolDeclarationNodeFixture9RoadDummyEE]
//  └── [Composite::Parallel::Unnamed]
//        Symbol table scope name: Composite::Parallel::Unnamed
//      ├── [Action::TestNodeAccessAllParams]
//      │     Symbol table scope name: Action::TestNodeAccessAllParams
//      └── [Decorator::SymbolDeclarationNode::EgoParamDeclarer]
//            Symbol table scope name: Decorator::SymbolDeclarationNode::EgoParamDeclarer
//          └── [Action::TestNodeAccessAllParams]
//                Symbol table scope name: Action::TestNodeAccessAllParams
TEST_F(SymbolDeclarationNodeFixture, symbol_declaration_node_illegal_setup) {
  // Top node: makes road everywhere downstream available
  SymbolDeclarer::Ptr road_declarer(new RoadInserter());
  DecoratorNode::Ptr road_declarer_decorator =
      std::make_shared<SymbolDeclarationNode>("RoadDeclarer", std::move(road_declarer));

  // a parallel node to fork the tree into two subtrees (is the child of road_declarer_decorator)
  CompositeNode::Ptr parallel = std::make_shared<ParallelNode>();
  road_declarer_decorator->setChild(parallel);

  // Child 1: needs only access to road
  parallel->addChild(std::make_shared<TestNodeAccessAllParams>());

  // Child 2: Declares further variables for subtree
  SymbolDeclarer::Ptr ego_param(new EgoParamInserter);
  DecoratorNode::Ptr ego_param_decorator =
      std::make_shared<SymbolDeclarationNode>("EgoParamDeclarer", std::move(ego_param));
  ego_param_decorator->setChild(std::make_shared<TestNodeAccessAllParams>());
  parallel->addChild(std::move(ego_param_decorator));

  // The the collision will be observed in three ticks, which causes a failure downstream:
  EXPECT_ANY_THROW(road_declarer_decorator->initTree(););
}

}  // namespace yase
