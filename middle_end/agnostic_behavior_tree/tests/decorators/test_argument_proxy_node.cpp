/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "gtest/gtest.h"

#include <agnostic_behavior_tree/actions/analyse_nodes.h>
#include <agnostic_behavior_tree/actions/functor_action_node.h>
#include <agnostic_behavior_tree/composite/parallel_node.h>
#include <agnostic_behavior_tree/decorator/symbol_declaration_node.h>
#include <agnostic_behavior_tree/decorator/symbol_proxy_node.h>

#include <vector>

namespace yase {

// Defines multiple test instances to test the functionality
struct SymbolProxyNodeFixture : testing::Test {
  SymbolProxyNodeFixture(){};
  virtual ~SymbolProxyNodeFixture(){};

  // ------------- TEST INSTANCES -----------------

  // Exemplary declarer to provide ego params
  class EgoParamInserter : public SymbolDeclarer {
   public:
    // get shared symbol
    void declareSharedSymbols(BlackboardWriter& blackboard) override final {
      blackboard.set<unsigned int>("ego_id", 1);
      blackboard.set<std::shared_ptr<double>>("ego_target_speed", std::make_shared<double>(30.0));
    };
  };

  class ArgProxy : public ProxyArguments {
   public:
    // get shared symbol
    Blackboard::ProxyTable createProxyTableAndDeclareSharedSymbols(BlackboardWriter& blackboard) override final {
      Blackboard::ProxyTable proxy;
      // remapp key to other value
      proxy.insert({"vehicle_id", "ego_id"});
      // if no remapping should be done, set some default instead:
      blackboard.set<std::shared_ptr<size_t>>("target_lane", std::make_shared<size_t>(707));
      return proxy;
    };
  };

  // Node which requires access to the ego params
  class TestNodeAccessEgoParams : public ActionNode {
   public:
    TestNodeAccessEgoParams() : ActionNode("TestNodeAccessEgoParams"){};

    void onInit() override{};

   private:
    NodeStatus tick() final {
      if (m_ego_id != 1 && *m_ego_target_speed != 30.0) {
        executionInfo("The ego params are not accessible as expected!");
        return NodeStatus::kFailure;
      }
      executionInfo("All shared symbols can be accesssed.");
      return NodeStatus::kSuccess;
    };

    /// look up required symbol "vehicle_collisions"
    virtual void lookUpSymbolsInBlackBoard(const BlackboardReader& blackboard) override final {
      m_ego_id = blackboard.get<unsigned int>("ego_id");
      m_ego_target_speed = blackboard.get<std::shared_ptr<double>>("ego_target_speed");
    };

    unsigned int m_ego_id;
    std::shared_ptr<double> m_ego_target_speed;
  };

  // Node which requires access to all proxy arguments
  class TestNodeAccessProxyArgs : public ActionNode {
   public:
    TestNodeAccessProxyArgs() : ActionNode("TestNodeAccessProxyArgs"){};

    void onInit() override{};

   private:
    NodeStatus tick() final {
      if (m_vehicle_id != 1 && *m_target_lane != 707) {
        executionInfo("The proxy args are not accessible as expected!");
        return NodeStatus::kFailure;
      }
      executionInfo("All shared symbols can be accesssed.");
      return NodeStatus::kSuccess;
    };

    /// look up required symbol "vehicle_collisions"
    virtual void lookUpSymbolsInBlackBoard(const BlackboardReader& blackboard) override final {
      m_vehicle_id = blackboard.get<unsigned int>("vehicle_id");
      m_target_lane = blackboard.get<std::shared_ptr<size_t>>("target_lane");
    };

    unsigned int m_vehicle_id;
    std::shared_ptr<size_t> m_target_lane;
  };
};

// Test if child is initialised and terminated
TEST_F(SymbolProxyNodeFixture, test_onInit_and_onTerminate) {
  // Setup
  std::shared_ptr<AnalyseNode> dummy_node = std::make_shared<AnalyseNode>(1);

  SymbolDeclarer::Ptr ego_param_declarer(new EgoParamInserter());
  DecoratorNode::Ptr test_decorator =
      std::make_shared<SymbolDeclarationNode>("RoadDeclarer", std::move(ego_param_declarer));
  test_decorator->setChild(dummy_node);

  // Init and tick of parallel (should init children as well)
  test_decorator->onInit();
  EXPECT_EQ(dummy_node->isInitialised(), true);
  test_decorator->onTerminate();
  EXPECT_EQ(dummy_node->isInitialised(), false);
}

// Legal tree: all data is accessible
//
//└── [Decorator::SymbolDeclarationNode::RoadDeclarer]
//      Symbol table scope name: Decorator::SymbolDeclarationNode::RoadDeclarer
//      - Key: [ego_target_speed], type: [St10shared_ptrIdE]
//      - Key: [ego_id], type: [St10shared_ptrIjE]
//    └── [Composite::Parallel::Unnamed]
//          Symbol table scope name: Composite::Parallel::Unnamed
//        ├── [Action::TestNodeAccessEgoParams]
//        │     Symbol table scope name: Action::TestNodeAccessEgoParams
//        └── [Decorator::SymbolProxyNode::EgoParamDeclarer]
//              Symbol table scope name: Decorator::SymbolProxyNode::EgoParamDeclarer
//              - Key: [target_lane], type: [St10shared_ptrImE]
//              Argument proxy - remapped variables: 1
//              - Key: [vehicle_id] remapped to key: [ego_id]
//              REMARK: Beyond this proxy only the remapped keys are accessible!
//            └── [Action::TestNodeAccessProxyArgs]
//                  Symbol table scope name: Action::TestNodeAccessProxyArgs
TEST_F(SymbolProxyNodeFixture, symbol_declaration_node_legal_setup) {
  // Top node: makes road everywhere downstream available
  SymbolDeclarer::Ptr ego_param_declarer(new EgoParamInserter());
  DecoratorNode::Ptr ego_param_declarer_decorator =
      std::make_shared<SymbolDeclarationNode>("RoadDeclarer", std::move(ego_param_declarer));

  // a parallel node to fork the tree into two subtrees (is the child of ego_param_declarer_decorator)
  CompositeNode::Ptr parallel = std::make_shared<ParallelNode>();
  ego_param_declarer_decorator->setChild(parallel);

  // Child 1: wants to access the ego params --> legal, as declared upstream
  parallel->addChild(std::make_shared<TestNodeAccessEgoParams>());

  // Child 2: Adds argument proxy decorator between parallel and leaf child
  ProxyArguments::Ptr arg_proxy(new ArgProxy);
  DecoratorNode::Ptr arg_proxy_decorator = std::make_shared<SymbolProxyNode>("EgoParamDeclarer", std::move(arg_proxy));
  arg_proxy_decorator->setChild(std::make_shared<TestNodeAccessProxyArgs>());
  parallel->addChild(std::move(arg_proxy_decorator));

  // The the collision will be observed in three ticks, which causes a failure downstream:
  ego_param_declarer_decorator->initTree();
  // init tree multiple times to check if the result is still consistent.
  ego_param_declarer_decorator->initTree();
  ego_param_declarer_decorator->initTree();
  EXPECT_EQ(ego_param_declarer_decorator->executeTick(), NodeStatus::kSuccess);
}

// Illegal tree: node wants to access data which is hidden behind argument proxy
//
//└── [Decorator::SymbolDeclarationNode::RoadDeclarer]
//    - Key: [ego_target_speed], type: [St10shared_ptrIdE]
//    - Key: [ego_id], type: [St10shared_ptrIjE]
//  └── [Composite::Parallel::Unnamed]
//      ├── [Action::TestNodeAccessEgoParams]
//      └── [Decorator::SymbolProxyNode::EgoParamDeclarer]
//            Symbol table scope name: Decorator::SymbolProxyNode::EgoParamDeclarer
//            - Key: [target_lane], type: [St10shared_ptrImE]
//            Argument proxy - remapped variables: 1
//            - Key: [vehicle_id] remapped to key: [ego_id]
//            REMARK: Beyond this proxy only the remapped keys are accessible!
//          └── [Action::TestNodeAccessEgoParams]
//                Symbol table scope name: Action::TestNodeAccessEgoParams
TEST_F(SymbolProxyNodeFixture, symbol_declaration_node_illegal_setup) {
  // Top node: makes road everywhere downstream available
  SymbolDeclarer::Ptr ego_param_declarer(new EgoParamInserter());
  DecoratorNode::Ptr ego_param_declarer_decorator =
      std::make_shared<SymbolDeclarationNode>("RoadDeclarer", std::move(ego_param_declarer));

  // a parallel node to fork the tree into two subtrees (is the child of ego_param_declarer_decorator)
  CompositeNode::Ptr parallel = std::make_shared<ParallelNode>();
  ego_param_declarer_decorator->setChild(parallel);

  // Child 1: wants to access the ego params --> legal, as declared upstream
  parallel->addChild(std::make_shared<TestNodeAccessEgoParams>());

  // Child 2: Adds argument proxy decorator between parallel and leaf child
  ProxyArguments::Ptr arg_proxy(new ArgProxy);
  DecoratorNode::Ptr arg_proxy_decorator = std::make_shared<SymbolProxyNode>("EgoParamDeclarer", std::move(arg_proxy));
  // ILLEGAL: This node wants to access the ego params, while they are hidden behind the argument proxy node!
  arg_proxy_decorator->setChild(std::make_shared<TestNodeAccessEgoParams>());
  parallel->addChild(std::move(arg_proxy_decorator));

  // Illegal setup should throw (Check it multiple time to check if result is consistent)
  EXPECT_ANY_THROW(ego_param_declarer_decorator->initTree(););
  EXPECT_ANY_THROW(ego_param_declarer_decorator->initTree(););
  EXPECT_ANY_THROW(ego_param_declarer_decorator->initTree(););
}

}  // namespace yase
