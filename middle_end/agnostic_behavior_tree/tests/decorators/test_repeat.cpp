/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "gtest/gtest.h"

#include <agnostic_behavior_tree/actions/analyse_nodes.h>
#include <agnostic_behavior_tree/decorator/repeat_node.h>

namespace yase {
TEST(RepeatTest, repeat_1) {
  // Running --> Running
  Repeat repeat_test_node{0};
  std::shared_ptr<AnalyseNode> analyse_node = std::make_shared<AlwaysRunning>();

  repeat_test_node.setChild(analyse_node);
  repeat_test_node.onInit();

  EXPECT_EQ(repeat_test_node.executeTick(), NodeStatus::kRunning);
  EXPECT_EQ(repeat_test_node.executeTick(), NodeStatus::kRunning);
  EXPECT_EQ(repeat_test_node.executeTick(), NodeStatus::kRunning);
  EXPECT_EQ(repeat_test_node.executeTick(), NodeStatus::kRunning);
}

// Repeat has to repeat the analyse node three times
TEST(RepeatTest, repeat_2) {
  // Building blocks
  Repeat repeat_test_node{3};
  std::shared_ptr<AnalyseNode> analyse_node = std::make_shared<AnalyseNode>(1);

  // Connect to tree
  repeat_test_node.setChild(analyse_node);
  repeat_test_node.onInit();

  // Run test
  EXPECT_EQ(repeat_test_node.executeTick(), NodeStatus::kRunning);
  EXPECT_EQ(repeat_test_node.executeTick(), NodeStatus::kRunning);
  // Analyse node is the first time successful
  EXPECT_EQ(analyse_node->onTerminateCalls(), 1);
  EXPECT_EQ(analyse_node->onInitCalls(), 1);

  EXPECT_EQ(repeat_test_node.executeTick(), NodeStatus::kRunning);
  EXPECT_EQ(repeat_test_node.executeTick(), NodeStatus::kRunning);
  // Analyse node is second time successful
  EXPECT_EQ(analyse_node->onTerminateCalls(), 2);
  EXPECT_EQ(analyse_node->onInitCalls(), 2);

  EXPECT_EQ(repeat_test_node.executeTick(), NodeStatus::kRunning);
  EXPECT_EQ(repeat_test_node.executeTick(), NodeStatus::kSuccess);
  // Analyse node is the last time successful
  EXPECT_EQ(analyse_node->onTerminateCalls(), 3);
  EXPECT_EQ(analyse_node->onInitCalls(), 3);

  // After this the analyse node is not ticked anymore
  EXPECT_EQ(repeat_test_node.executeTick(), NodeStatus::kSuccess);

  EXPECT_EQ(analyse_node->onTerminateCalls(), 3);
  EXPECT_EQ(analyse_node->onInitCalls(), 3);
}

TEST(RepeatTest, repeat_3) {
  // Failure --> Directly repeats failure
  Repeat repeat_test_node{0};
  std::shared_ptr<AnalyseNode> analyse_node = std::make_shared<AlwaysFailure>();

  repeat_test_node.setChild(analyse_node);
  repeat_test_node.onInit();

  EXPECT_EQ(repeat_test_node.executeTick(), NodeStatus::kFailure);
}

}  // namespace yase
