/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "gtest/gtest.h"

#include <agnostic_behavior_tree/utils/condition.h>

namespace yase {

// Evaluates to true after three evaluation calls
class TrueAfterThreeEvaluationCalls : public Condition {
 public:
  explicit TrueAfterThreeEvaluationCalls() : Condition("TrueAfterThreeEvaluationCalls"){};

  void onInit() final { m_evaluation_calls = 0; }

  bool evaluate() final {
    if (m_evaluation_calls >= 3) {
      return true;
    }
    m_evaluation_calls++;
    return false;
  };

 private:
  unsigned int m_evaluation_calls{0};
};

// Evaluates to true after three evaluation calls
class AlwaysTrue : public Condition {
 public:
  explicit AlwaysTrue() : Condition("AlwaysTrue"){};

  void onInit() final {}

  bool evaluate() final { return true; };
};

// Test if execution stops after until condition is met
TEST(TestCondition, all_of_conditions) {
  std::vector<Condition::UPtr> conditions;
  conditions.push_back(std::unique_ptr<Condition>(new TrueAfterThreeEvaluationCalls()));
  conditions.push_back(std::unique_ptr<Condition>(new AlwaysTrue()));

  AllOfConditions all_of_conditions(std::move(conditions));

  // Tick evaluate three times - after that the all_of condition returns true
  EXPECT_EQ(all_of_conditions.evaluate(), false);
  EXPECT_EQ(all_of_conditions.evaluate(), false);
  EXPECT_EQ(all_of_conditions.evaluate(), false);
  EXPECT_EQ(all_of_conditions.evaluate(), true);
  EXPECT_EQ(all_of_conditions.evaluate(), true);

  // Call onInit() to reset the condition -> must be the same as before
  all_of_conditions.onInit();
  EXPECT_EQ(all_of_conditions.evaluate(), false);
  EXPECT_EQ(all_of_conditions.evaluate(), false);
  EXPECT_EQ(all_of_conditions.evaluate(), false);
  EXPECT_EQ(all_of_conditions.evaluate(), true);
  EXPECT_EQ(all_of_conditions.evaluate(), true);
}

// Test if execution stops after until condition is met
TEST(TestCondition, any_of_conditions) {
  std::vector<Condition::UPtr> conditions;
  conditions.push_back(std::unique_ptr<Condition>(new TrueAfterThreeEvaluationCalls()));
  conditions.push_back(std::unique_ptr<Condition>(new AlwaysTrue()));

  AnyOfConditions any_of_conditions(std::move(conditions));

  // One condition is directly true
  EXPECT_EQ(any_of_conditions.evaluate(), true);

  // Call onInit() to reset the condition -> must be the same as before
  any_of_conditions.onInit();
  EXPECT_EQ(any_of_conditions.evaluate(), true);
}

}  // namespace yase
