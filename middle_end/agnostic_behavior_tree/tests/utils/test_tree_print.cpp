/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "gtest/gtest.h"

#include <agnostic_behavior_tree/actions/analyse_nodes.h>
#include <agnostic_behavior_tree/actions/functor_action_node.h>
#include <agnostic_behavior_tree/composite/parallel_node.h>
#include <agnostic_behavior_tree/decorator/symbol_declaration_node.h>
#include <agnostic_behavior_tree/decorator/symbol_proxy_node.h>
#include <agnostic_behavior_tree/utils/tree_print.h>

#include <memory>

namespace yase {
// Defines multiple test instances to test the functionality
struct TreePrintFixture : testing::Test {
  TreePrintFixture() {
    // Build up a complex tree to print

    // A parallel node to fork the tree into two subtrees (is the child of ego_param_declarer_decorator)
    CompositeNode::Ptr root = std::make_shared<ParallelNode>();
    root->addChild(std::make_shared<AlwaysSuccess>());

    // Top node: makes road everywhere downstream available
    SymbolDeclarer::Ptr ego_param_declarer(new EgoParamInserter());
    DecoratorNode::Ptr ego_param_declarer_decorator =
        std::make_shared<SymbolDeclarationNode>("RoadDeclarer", std::move(ego_param_declarer));
    root->addChild(ego_param_declarer_decorator);
    root->addChild(std::make_shared<AlwaysRunning>());

    // A parallel node to fork the tree into two subtrees (is the child of ego_param_declarer_decorator)
    CompositeNode::Ptr parallel = std::make_shared<ParallelNode>();
    ego_param_declarer_decorator->setChild(parallel);

    // Child 1: wants to access the ego params --> legal, as declared upstream
    parallel->addChild(std::make_shared<TestNodeAccessEgoParams>());

    // Child 2: Adds argument proxy decorator between parallel and leaf child
    ProxyArguments::Ptr arg_proxy(new ArgProxy);
    DecoratorNode::Ptr arg_proxy_decorator =
        std::make_shared<SymbolProxyNode>("EgoParamDeclarer", std::move(arg_proxy));
    arg_proxy_decorator->setChild(std::make_shared<TestNodeAccessProxyArgs>());
    parallel->addChild(std::move(arg_proxy_decorator));
    parallel->addChild(std::make_shared<AlwaysSuccess>());
    parallel->addChild(std::make_shared<AlwaysRunning>());

    // The the collision will be observed in three ticks, which causes a failure downstream:
    ego_param_declarer_decorator->initTree();

    test_tree = root;
  };

  virtual ~TreePrintFixture(){};

  // test tree
  BehaviorNode::Ptr test_tree;

  // ------------- TEST INSTANCES -----------------

  // Exemplary declarer to provide ego params
  class EgoParamInserter : public SymbolDeclarer {
   public:
    // get shared symbol
    void declareSharedSymbols(BlackboardWriter& blackboard) final {
      blackboard.set<std::shared_ptr<unsigned int>>("ego_id", std::make_shared<unsigned int>(1));
      blackboard.set<std::shared_ptr<double>>("ego_target_speed", std::make_shared<double>(30.0));
    };
  };

  class ArgProxy : public ProxyArguments {
   public:
    // get shared symbol
    Blackboard::ProxyTable createProxyTableAndDeclareSharedSymbols(BlackboardWriter& blackboard) final {
      Blackboard::ProxyTable proxy;
      // remapp key to other value
      proxy.insert({"vehicle_id", "ego_id"});
      // if no remapping should be done, set some default instead:
      blackboard.set<std::shared_ptr<size_t>>("target_lane", std::make_shared<size_t>(707));
      return proxy;
    };
  };

  // Node which requires access to the ego params
  class TestNodeAccessEgoParams : public ActionNode {
   public:
    TestNodeAccessEgoParams() : ActionNode("TestNodeAccessEgoParams"){};

    void onInit() override{};

   private:
    virtual NodeStatus tick() final {
      if (*m_ego_id != 1 && *m_ego_target_speed != 30.0) {
        executionInfo("The ego params are not accessible as expected!");
        return NodeStatus::kFailure;
      }
      executionInfo("All shared symbols can be accesssed.");
      return NodeStatus::kSuccess;
    };

    /// look up required symbols
    virtual void lookUpSymbolsInBlackBoard(const BlackboardReader& blackboard) final {
      m_ego_id = blackboard.get<std::shared_ptr<unsigned int>>("ego_id");
      m_ego_target_speed = blackboard.get<std::shared_ptr<double>>("ego_target_speed");
    };

    std::shared_ptr<unsigned int> m_ego_id;
    std::shared_ptr<double> m_ego_target_speed;
  };

  // Node which requires access to all proxy arguments
  class TestNodeAccessProxyArgs : public ActionNode {
   public:
    TestNodeAccessProxyArgs() : ActionNode("TestNodeAccessProxyArgs"){};

    void onInit() override{};

   private:
    virtual NodeStatus tick() final {
      if (*m_vehicle_id != 1 && *m_target_lane != 707) {
        executionInfo("The proxy args are not accessible as expected!");
        return NodeStatus::kFailure;
      }
      executionInfo("All shared symbols can be accesssed.");
      return NodeStatus::kSuccess;
    };

    /// look up required symbols
    virtual void lookUpSymbolsInBlackBoard(const BlackboardReader& blackboard) final {
      m_vehicle_id = blackboard.get<std::shared_ptr<unsigned int>>("vehicle_id");
      m_target_lane = blackboard.get<std::shared_ptr<size_t>>("target_lane");
    };

    std::shared_ptr<unsigned int> m_vehicle_id;
    std::shared_ptr<size_t> m_target_lane;
  };
};

TEST_F(TreePrintFixture, print_tree) { EXPECT_NO_THROW(printTree(test_tree);); }

TEST_F(TreePrintFixture, print_tree_with_states) {
  EXPECT_NO_THROW(printTreeWithStates(test_tree););
  test_tree->executeTick();
  EXPECT_NO_THROW(printTreeWithStates(test_tree););
}

TEST_F(TreePrintFixture, print_tree_with_symbols) { EXPECT_NO_THROW(printTreeWithSymbols(test_tree);); }

}  // namespace yase
