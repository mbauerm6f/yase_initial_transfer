/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "gtest/gtest.h"

#include <agnostic_behavior_tree/actions/analyse_nodes.h>
#include <agnostic_behavior_tree/composite/sequence_node.h>

#include <memory>

namespace yase {
TEST(SequenceTest, successful_sequence) {
  // Sequence with three childrem. Together they need 6 ticks
  SequenceNode test_sequence;
  test_sequence.addChild(std::make_shared<AnalyseNode>(1));
  test_sequence.addChild(std::make_shared<AnalyseNode>(2));
  test_sequence.addChild(std::make_shared<AnalyseNode>(3));

  // Helper variables
  NodeStatus status = NodeStatus::kRunning;
  size_t counter = 0;

  // Execute until finished
  while (status == NodeStatus::kRunning) {
    status = test_sequence.executeTick();
    counter++;
  }

  // Should finish successful with 6 ticks
  EXPECT_EQ(status, NodeStatus::kSuccess);
  EXPECT_EQ(counter, 7);
}

TEST(SequenceTest, failing_sequence) {
  // sequence with three childrem
  SequenceNode test_sequence;
  test_sequence.addChild(std::make_shared<AnalyseNode>(1));
  test_sequence.addChild(std::make_shared<AlwaysFailure>());
  test_sequence.addChild(std::make_shared<AnalyseNode>(1));

  // Helper variables
  NodeStatus status = NodeStatus::kRunning;

  // Execute until finished
  while (status == NodeStatus::kRunning) {
    status = test_sequence.executeTick();
  }

  // Should fail
  EXPECT_EQ(status, NodeStatus::kFailure);
}

// Test if sequence inits the children at start and terminates them as they finish
TEST(SequenceTest, test_onInit_and_onTerminate) {
  // Setup
  std::shared_ptr<AnalyseNode> dummy_node_1 = std::make_shared<AnalyseNode>(1);
  std::shared_ptr<AnalyseNode> dummy_node_2 = std::make_shared<AnalyseNode>(1);

  CompositeNode::Ptr sequence = std::make_shared<SequenceNode>();
  sequence->addChild(dummy_node_1);
  sequence->addChild(dummy_node_2);

  // Before the dummy_nodes should be uninitialized
  EXPECT_EQ(dummy_node_1->isInitialised(), false);
  EXPECT_EQ(dummy_node_2->isInitialised(), false);

  // Init and tick of parallel (first running child should be initialised)
  sequence->onInit();
  sequence->executeTick();
  EXPECT_EQ(dummy_node_1->isInitialised(), true);
  EXPECT_EQ(dummy_node_2->isInitialised(), false);

  // Second tick - First child finished and should be terminated
  sequence->executeTick();
  EXPECT_EQ(dummy_node_1->isInitialised(), false);
  EXPECT_EQ(dummy_node_2->isInitialised(), true);

  // Third tick - All children finished and should be terminated
  EXPECT_EQ(sequence->executeTick(), NodeStatus::kSuccess);
  EXPECT_EQ(dummy_node_1->isInitialised(), false);
  EXPECT_EQ(dummy_node_2->isInitialised(), false);
}

// Test if sequence node terminates all children on sudden terminate
TEST(SequenceTest, test_sudden_onTerminate) {
  // Setup
  std::shared_ptr<AnalyseNode> dummy_node_1 = std::make_shared<AnalyseNode>(1);
  std::shared_ptr<AnalyseNode> dummy_node_2 = std::make_shared<AnalyseNode>(2);

  CompositeNode::Ptr sequence = std::make_shared<SequenceNode>();
  sequence->addChild(dummy_node_1);
  sequence->addChild(dummy_node_2);

  // Init and tick of parallel (should init children as well)
  sequence->onInit();
  sequence->executeTick();
  EXPECT_EQ(dummy_node_1->isInitialised(), true);
  EXPECT_EQ(dummy_node_2->isInitialised(), false);

  // Sudden terminate
  sequence->onTerminate();
  EXPECT_EQ(dummy_node_1->isInitialised(), false);
  EXPECT_EQ(dummy_node_2->isInitialised(), false);
}

}  // namespace yase
