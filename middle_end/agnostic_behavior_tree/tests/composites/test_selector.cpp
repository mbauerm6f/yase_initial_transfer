/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "gtest/gtest.h"

#include <agnostic_behavior_tree/actions/analyse_nodes.h>
#include <agnostic_behavior_tree/composite/selector_node.h>

#include <memory>

namespace yase {
TEST(SelectorTest, direct_successful_selector) {
  // Second children is directly success
  CompositeNode::Ptr test_selector = std::make_shared<SelectorNode>();
  test_selector->addChild(std::make_shared<AlwaysFailure>());
  test_selector->addChild(std::make_shared<AlwaysSuccess>());

  // Should directly finish with success
  EXPECT_EQ(test_selector->executeTick(), NodeStatus::kSuccess);
}

TEST(SelectorTest, successful_selector) {
  // Second children is directly success
  CompositeNode::Ptr test_selector = std::make_shared<SelectorNode>();
  test_selector->addChild(std::make_shared<AlwaysFailure>());
  test_selector->addChild(std::make_shared<AnalyseNode>(2));

  // Helper variables
  NodeStatus status = NodeStatus::kRunning;

  // Execute until finished
  while (status == NodeStatus::kRunning) {
    status = test_selector->executeTick();
  }

  // Should finish successfully
  EXPECT_EQ(status, NodeStatus::kSuccess);
}

TEST(SelectorTest, failing_selector) {
  // None of the children succeeds/ can run
  CompositeNode::Ptr test_selector = std::make_shared<SelectorNode>();
  test_selector->addChild(std::make_shared<AlwaysFailure>());
  test_selector->addChild(std::make_shared<AlwaysFailure>());

  // Helper variables
  NodeStatus status = NodeStatus::kRunning;

  // Execute until finished
  while (status == NodeStatus::kRunning) {
    status = test_selector->executeTick();
  }

  // Should fail
  EXPECT_EQ(status, NodeStatus::kFailure);
}

// Test if sequence inits the children at start and terminates them as they finish
TEST(SelectorTest, test_onInit_and_onTerminate) {
  // Setup
  std::shared_ptr<AnalyseNode> dummy_node_1 = std::make_shared<AlwaysFailure>();
  std::shared_ptr<AnalyseNode> dummy_node_2 = std::make_shared<AnalyseNode>(1, NodeStatus::kFailure);
  std::shared_ptr<AnalyseNode> dummy_node_3 = std::make_shared<AlwaysRunning>();

  CompositeNode::Ptr selector = std::make_shared<SelectorNode>();
  selector->addChild(dummy_node_1);
  selector->addChild(dummy_node_2);
  selector->addChild(dummy_node_3);

  // Before the dummy_nodes should be uninitialized
  EXPECT_EQ(dummy_node_1->isInitialised(), false);
  EXPECT_EQ(dummy_node_2->isInitialised(), false);
  EXPECT_EQ(dummy_node_3->isInitialised(), false);

  // Init and tick of parallel - Second child runs
  selector->onInit();
  selector->executeTick();
  EXPECT_EQ(dummy_node_1->isInitialised(), false);
  EXPECT_EQ(dummy_node_1->onInitCalls(), 1);
  EXPECT_EQ(dummy_node_1->onTerminateCalls(), 1);

  EXPECT_EQ(dummy_node_2->isInitialised(), true);
  EXPECT_EQ(dummy_node_2->onInitCalls(), 1);
  EXPECT_EQ(dummy_node_2->onTerminateCalls(), 0);

  EXPECT_EQ(dummy_node_3->isInitialised(), false);
  EXPECT_EQ(dummy_node_3->onInitCalls(), 0);
  EXPECT_EQ(dummy_node_3->onTerminateCalls(), 0);

  // Second tick - Second child finishes
  selector->executeTick();
  EXPECT_EQ(dummy_node_1->isInitialised(), false);
  EXPECT_EQ(dummy_node_1->onInitCalls(), 2);
  EXPECT_EQ(dummy_node_1->onTerminateCalls(), 2);

  EXPECT_EQ(dummy_node_2->isInitialised(), false);
  EXPECT_EQ(dummy_node_2->onInitCalls(), 1);
  EXPECT_EQ(dummy_node_2->onTerminateCalls(), 1);

  EXPECT_EQ(dummy_node_3->isInitialised(), true);
  EXPECT_EQ(dummy_node_3->onInitCalls(), 1);
  EXPECT_EQ(dummy_node_3->onTerminateCalls(), 0);

  // Third tick - Second child finishes
  selector->executeTick();
  EXPECT_EQ(dummy_node_1->isInitialised(), false);
  EXPECT_EQ(dummy_node_1->onInitCalls(), 3);
  EXPECT_EQ(dummy_node_1->onTerminateCalls(), 3);

  EXPECT_EQ(dummy_node_2->isInitialised(), true);
  EXPECT_EQ(dummy_node_2->onInitCalls(), 2);
  EXPECT_EQ(dummy_node_2->onTerminateCalls(), 1);

  EXPECT_EQ(dummy_node_3->isInitialised(), false);
  EXPECT_EQ(dummy_node_3->onInitCalls(), 1);
  EXPECT_EQ(dummy_node_3->onTerminateCalls(), 1);
}

// Test if sequence node terminates all children on sudden terminate
TEST(SelectorTest, test_sudden_onTerminate) {
  // Setup
  std::shared_ptr<AnalyseNode> dummy_node_1 = std::make_shared<AnalyseNode>(1);
  std::shared_ptr<AnalyseNode> dummy_node_2 = std::make_shared<AnalyseNode>(2);

  CompositeNode::Ptr selector = std::make_shared<SelectorNode>();
  selector->addChild(dummy_node_1);
  selector->addChild(dummy_node_2);

  // Init and tick of parallel (should init children as well)
  selector->onInit();
  selector->executeTick();
  EXPECT_EQ(dummy_node_1->isInitialised(), true);
  EXPECT_EQ(dummy_node_2->isInitialised(), false);

  // Sudden terminate - Children onTerminate should be called
  selector->onTerminate();
  EXPECT_EQ(dummy_node_1->onTerminateCalls(), 1);
  EXPECT_EQ(dummy_node_2->onTerminateCalls(), 1);
}

}  // namespace yase
