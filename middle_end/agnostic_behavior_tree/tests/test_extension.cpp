/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "gtest/gtest.h"

#include <agnostic_behavior_tree/action_node.h>
#include <agnostic_behavior_tree/actions/analyse_nodes.h>
#include <agnostic_behavior_tree/behavior_node.h>
#include <agnostic_behavior_tree/composite/parallel_node.h>
#include <agnostic_behavior_tree/composite/selector_node.h>
#include <agnostic_behavior_tree/composite/sequence_node.h>
#include <agnostic_behavior_tree/composite_node.h>
#include <agnostic_behavior_tree/decorator_node.h>
#include <agnostic_behavior_tree/utils/tree_analyzer.h>

#include <functional>
#include <memory>
#include <string>

// Test description:
//
// To extend all nodes with costum methods for a specific usecase/ simulator,
// the functionality can be added via composition.
// In the given test the nodes are extended with the costum method serializeToFormatXyz().
//
// As this method is node specific, it must be defined for all nodes it should be used.
// The given example does it only for a limited set.
namespace simulator_xyz {
// Simulator specific extension
// From this base class the specialized subclasses can be inherited.
class SimulatorExtension : public yase::Extension {
 public:
  virtual bool serializeToFormatXyz() = 0;
};

// ### General nodes without extension (extension variable is nullptr) ###
using BehaviorNode = yase::BehaviorNode;
using CompositeNode = yase::CompositeNode;

// ### Extension for some standard nodes ###
class SequenceNode : public yase::SequenceNode {
 public:
  // Extension definition for a sequence
  class SimulatorExtensionSequence : public SimulatorExtension {
   public:
    SimulatorExtensionSequence(SequenceNode* node) : m_sequence(node){};

    virtual bool serializeToFormatXyz() override {
      bool test = false;
      for (auto child : m_sequence->children()) {
        test = std::dynamic_pointer_cast<SimulatorExtension>(child->extension())->serializeToFormatXyz();
      }
      return test;
    }

   private:
    SequenceNode* m_sequence;
  };

  SequenceNode(std::string name)
      : yase::SequenceNode(name, std::shared_ptr<yase::Extension>(new SimulatorExtensionSequence(this))){};
  virtual ~SequenceNode() override = default;

 private:
  friend class SimulatorExtensionSequence;
};

class ParallelNode : public yase::ParallelNode {
 public:
  // Extension definition for a parallel
  class SimulatorExtensionParallel : public SimulatorExtension {
   public:
    SimulatorExtensionParallel(ParallelNode* node) : m_parallel(node){};

    virtual bool serializeToFormatXyz() override {
      bool test = false;
      for (auto child : m_parallel->children()) {
        test = std::dynamic_pointer_cast<SimulatorExtension>(child->extension())->serializeToFormatXyz();
      }
      return test;
    }

   private:
    ParallelNode* m_parallel;
  };

  ParallelNode(std::string name)
      : yase::ParallelNode(name, std::shared_ptr<yase::Extension>(new SimulatorExtensionParallel(this))){};
  virtual ~ParallelNode() override = default;
};

// No specific extension defined - will fail if called
class SelectorNode : public yase::SelectorNode {
 public:
  SelectorNode(std::string name) : yase::SelectorNode(name){};
  virtual ~SelectorNode() override = default;
};

class AlwaysSuccess : public yase::AlwaysSuccess {
 public:
  // Extension definition for a parallel
  class SimulatorExtensionAlwaysSuccess : public SimulatorExtension {
   public:
    SimulatorExtensionAlwaysSuccess(AlwaysSuccess* node) : m_always_success(node){};

    virtual bool serializeToFormatXyz() override { return true; }

   private:
    AlwaysSuccess* m_always_success;
  };

  AlwaysSuccess() : yase::AlwaysSuccess(std::shared_ptr<yase::Extension>(new SimulatorExtensionAlwaysSuccess(this))){};
  virtual ~AlwaysSuccess() override = default;
};

}  // namespace simulator_xyz

namespace test {

// Extension method access
//
// All used nodes have the extension method defined -> Should be accessible
TEST(ExtensionTest, all_nodes_with_extension) {
  simulator_xyz::CompositeNode::Ptr parallel = std::make_shared<simulator_xyz::ParallelNode>("parallel");
  simulator_xyz::CompositeNode::Ptr sequence = std::make_shared<simulator_xyz::SequenceNode>("sequence");
  simulator_xyz::BehaviorNode::Ptr always_success_1 = std::make_shared<simulator_xyz::AlwaysSuccess>();
  simulator_xyz::BehaviorNode::Ptr always_success_2 = std::make_shared<simulator_xyz::AlwaysSuccess>();
  simulator_xyz::BehaviorNode::Ptr always_success_3 = std::make_shared<simulator_xyz::AlwaysSuccess>();

  parallel->addChild(always_success_1);
  sequence->addChild(always_success_2);
  sequence->addChild(always_success_3);
  parallel->addChild(sequence);

  // └─ [Composite::Parallel::parallel]     - has node specific extension
  //    ├─ [Action::AlwaysSuccess]          - has node specific extension
  //    └─ [Composite::Sequence::sequence]  - has node specific extension
  //       ├─ [Action::AlwaysSuccess]       - has node specific extension
  //       └─ [Action::AlwaysSuccess]       - has node specific extension

  // all used nodes have the extension method defined
  EXPECT_TRUE(yase::extensionsOfTreeDefined<simulator_xyz::SimulatorExtension>(parallel));
  // should be legal to call it for the whole tree
  EXPECT_TRUE(
      std::dynamic_pointer_cast<simulator_xyz::SimulatorExtension>(parallel->extension())->serializeToFormatXyz());
}

// Undefined extension access
//
// Trying to call the function on all nodes, but selector is not defined for this extension:
TEST(ExtensionTest, undefined_extension_access) {
  simulator_xyz::CompositeNode::Ptr parallel = std::make_shared<simulator_xyz::ParallelNode>("parallel");
  simulator_xyz::CompositeNode::Ptr selector = std::make_shared<simulator_xyz::SelectorNode>("selector");
  simulator_xyz::BehaviorNode::Ptr always_success_1 = std::make_shared<simulator_xyz::AlwaysSuccess>();
  simulator_xyz::BehaviorNode::Ptr always_success_2 = std::make_shared<simulator_xyz::AlwaysSuccess>();
  simulator_xyz::BehaviorNode::Ptr always_success_3 = std::make_shared<simulator_xyz::AlwaysSuccess>();

  parallel->addChild(always_success_1);
  selector->addChild(always_success_2);
  selector->addChild(always_success_3);
  parallel->addChild(selector);

  // └─ [Composite::Parallel::parallel]     - has node specific extension
  //    ├─ [Action::AlwaysSuccess]          - has node specific extension
  //    └─ [Composite::Selector::selector]  - NO specific extension !!!
  //       ├─ [Action::AlwaysSuccess]       - has node specific extension
  //       └─ [Action::AlwaysSuccess]       - has node specific extension

  // The selector has no defined extension
  EXPECT_FALSE(yase::extensionsOfTreeDefined<simulator_xyz::SimulatorExtension>(parallel));
  // invalid and therefore should throw
  EXPECT_ANY_THROW(
      std::dynamic_pointer_cast<simulator_xyz::SimulatorExtension>(parallel->extension())->serializeToFormatXyz());
}

}  // namespace test
