/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "agnostic_behavior_tree/scoped_blackboard.h"

#include <string>

namespace yase {

BlackboardBase::BlackboardBase(const std::string& name) : scope_name(name) {}

BlackboardBase::BlackboardBase(const std::string& name, BlackboardBase::Ptr parent)
    : scope_name(name), m_parent_blackboard(parent) {}

bool BlackboardBase::exists(const std::string& key) const noexcept {
  const linb::any* symbol_any = get(key);
  return (symbol_any != nullptr);
}

void BlackboardBase::printLocalSymbols(std::string prefix) const noexcept {
  // Print local variables
  for (auto local_symbol_it : m_local_symbols) {
    std::string variable_info = "- Key: [";
    variable_info.append(local_symbol_it.first);
    variable_info.append("], type: [");
    variable_info.append(local_symbol_it.second.type().name());
    variable_info.append("]");
    std::cout << prefix << variable_info << std::endl;
  }
  // Print remmaped keys
  if (m_proxy_table) {
    std::cout << prefix << "Argument proxy - remapped variables: " << m_proxy_table->size() << std::endl;
    for (const auto& local_remapped_symbol : *m_proxy_table) {
      std::string variable_info = "- Key: [";
      variable_info.append(local_remapped_symbol.first);
      variable_info.append("] remapped to key: [");
      variable_info.append(local_remapped_symbol.second);
      variable_info.append("]");
      std::cout << prefix << variable_info << std::endl;
    }
    std::cout << prefix << "REMARK: Beyond this proxy only the remapped keys are accessible!" << std::endl;
  }
}

void BlackboardBase::printAllAccessibleSymbols(std::string prefix) const noexcept {
  printLocalSymbols(prefix);

  // Print all parent variables
  if (m_parent_blackboard) {
    std::cout << prefix << "└──##############  Parent blackboard:  ##############" << std::endl;
    std::cout << prefix << "   Blackboard name: " << scope_name << std::endl;
    m_parent_blackboard->printAllAccessibleSymbols(prefix.append("   "));
  }
}

const linb::any* BlackboardBase::get(const std::string& key) const noexcept {
  // Search local
  auto local_symbol = m_local_symbols.find(key);
  if (local_symbol != m_local_symbols.end()) {
    return &(local_symbol->second);
  }
  // Check if remapping necessary
  if (m_proxy_table) {
    // Search with remapped key if necessary
    auto remapped_local_symbol = m_proxy_table->find(key);
    if (remapped_local_symbol != m_proxy_table->end()) {
      return m_parent_blackboard->get(remapped_local_symbol->second);
    }
  } else if (m_parent_blackboard) {
    // Search at parent direct
    return m_parent_blackboard->get(key);
  }
  // If not found, return nullptr
  return nullptr;
}

bool BlackboardBase::existsParentBlackboard() const noexcept { return (m_parent_blackboard != nullptr); }

BlackboardWriter::BlackboardWriter() : BlackboardBase("") {}

BlackboardReader::BlackboardReader() : BlackboardBase("") {}

Blackboard::Blackboard(const std::string& name) : BlackboardBase(name) {}

Blackboard::Blackboard(const std::string& name, BlackboardBase::Ptr parent) : BlackboardBase(name, parent) {}

void Blackboard::clearLocal() {
  m_local_symbols.clear();
  m_proxy_table = nullptr;
}

void Blackboard::setParentBlackboard(Blackboard::Ptr new_parent_blackboard) {
  if (!new_parent_blackboard) {
    std::string exception_msg = "For blackboard [";
    exception_msg.append(scope_name);
    exception_msg.append("] the new parent blackboard cannot be set as it is a nullptr!");
    throw std::invalid_argument(exception_msg);
  }
  if (existsParentBlackboard()) {
    std::string exception_msg = "For blackboard [";
    exception_msg.append(scope_name);
    exception_msg.append("] the new parent blackboard [");
    exception_msg.append(new_parent_blackboard->scope_name);
    exception_msg.append("] can not be set as the parent blackboard [");
    exception_msg.append(m_parent_blackboard->scope_name);
    exception_msg.append("] already exists.");
    throw std::invalid_argument(exception_msg);
  }
  m_parent_blackboard = new_parent_blackboard;
}

void Blackboard::setProxyTable(BlackboardBase::ProxyTable new_proxy_table) {
  if (m_proxy_table) {
    std::string exception_msg = "For blackboard [";
    exception_msg.append(scope_name);
    exception_msg.append("] a ProxyTable already exists.");
    throw std::invalid_argument(exception_msg);
  }
  m_proxy_table = std::unique_ptr<ProxyTable>(new ProxyTable{new_proxy_table});
}

}  // namespace yase
