/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "agnostic_behavior_tree/utils/tree_print.h"

#include <agnostic_behavior_tree/behavior_node.h>
#include <agnostic_behavior_tree/utils/visitors.h>

namespace yase {
// Converts the node into an output string
std::string nodeToStr(const BehaviorNode::Ptr node, const bool ansi_escape_code, const bool print_ticks) {
  std::string ret;
  if (node->tickedInCurrentCycle()) {
    ret = toStr(node->status(), ansi_escape_code);
  } else {
    ret = toStr(node->status(), false);
  }
  if (print_ticks) {
    ret.append(" - ").append(std::to_string(node->tickCycle()));
  }
  ret.append(std::string(" - [").append(node->name()).append("]"));
  if (!node->executionInfo().empty()) {
    ret.append(" - ").append(node->executionInfo());
  }
  return ret;
}

// Prints a tree with status from the given tree node on
void printTree(const BehaviorNode::Ptr root_node) {
  std::function<void(const BehaviorNode::Ptr, const std::string& start_prefix, const std::string& follow_prefix)>
      print_name_function;
  print_name_function = [](const BehaviorNode::Ptr node, const std::string& start_prefix,
                           const std::string& /*follow_prefix*/) {
    std ::cout << start_prefix << "[" << node->name() << "]" << std::endl;
  };
  // Print the tree starting from root_node
  std::cout << "--------Behavior Tree --------" << std::endl;
  applyPrintVisitor(root_node, print_name_function);
}

// Prints a tree with status from the given tree nodeon
void printTreeWithSymbols(const BehaviorNode::Ptr root_node) {
  std::function<void(const BehaviorNode::Ptr, const std::string& start_prefix, const std::string& follow_prefix)>
      print_node_with_symbol_function;

  // Prints the node with symbols and remapping
  print_node_with_symbol_function = [](const BehaviorNode::Ptr node, const std::string& start_prefix,
                                       const std::string& follow_prefix) {
    std::cout << start_prefix << "[" << node->name() << "]" << std::endl;
    // print local symbols
    node->blackboardBase()->printLocalSymbols(follow_prefix + std::string("  "));
  };
  // Print the tree starting from root_node
  std::cout << "--------Behavior Tree With Symbols--------" << std::endl;
  applyPrintVisitor(root_node, print_node_with_symbol_function);
}

// Prints tree with states and execution info
void printTreeWithStates(const BehaviorNode::Ptr root_node) {
  std::function<void(const BehaviorNode::Ptr, const std::string& start_prefix, const std::string& follow_prefix)>
      print_node_with_state_function;

  // Prints the node with state and execution info
  print_node_with_state_function = [](const BehaviorNode::Ptr node, const std::string& start_prefix,
                                      const std::string& /*follow_prefix*/) {
    std::cout << start_prefix << nodeToStr(node, true, true) << std::endl;
  };
  // Print the tree starting from root_node
  std::cout << "--------Behavior Tree With States--------" << std::endl;
  applyPrintVisitor(root_node, print_node_with_state_function);
}

// Prints tree with failure traceback (also with captured failures, which did not cause root failure!)
void printTreeWithFailureTraces(const BehaviorNode::Ptr root_node) {
  std::function<void(const BehaviorNode::Ptr, const std::string& start_prefix, const std::string& follow_prefix)>
      print_node_and_trace_failure;

  // Prints the node with Failure higlighting
  print_node_and_trace_failure = [](const BehaviorNode::Ptr node, const std::string& start_prefix,
                                    const std::string& /*follow_prefix*/) {
    if (node->status() == NodeStatus::kFailure) {
      std::cout << start_prefix << nodeToStr(node, true) << std::endl;
    } else {
      std::cout << start_prefix << nodeToStr(node, false) << std::endl;
    }
  };
  // Print the tree starting from root_node
  std::cout << "--------Behavior Tree With Failure Trace--------" << std::endl;
  applyPrintVisitor(root_node, print_node_and_trace_failure);
}

}  // namespace yase
