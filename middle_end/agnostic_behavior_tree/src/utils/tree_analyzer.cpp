/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "agnostic_behavior_tree/utils/tree_analyzer.h"

#include <agnostic_behavior_tree/behavior_node.h>
#include <agnostic_behavior_tree/utils/visitors.h>

#include <vector>

namespace yase {
// Counts all nodes including the root
size_t countNodes(const BehaviorNode::Ptr root_node) {
  size_t count{0};
  auto countFunctor = [&count](const BehaviorNode::Ptr /*node*/) mutable { ++count; };
  applyVisitor(root_node, countFunctor);
  return count;
}

// Get all failing nodes of the tree
std::vector<BehaviorNode::Ptr> getAllFailingNodes(const BehaviorNode::Ptr& root_node) {
  std::vector<BehaviorNode::Ptr> failing_nodes;
  auto getFailingFunctor = [&failing_nodes](const BehaviorNode::Ptr node) mutable {
    if (node->status() == NodeStatus::kFailure) {
      failing_nodes.push_back(node);
    }
  };
  applyVisitor(root_node, getFailingFunctor);
  return failing_nodes;
}

}  // namespace yase
