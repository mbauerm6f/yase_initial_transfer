/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "agnostic_behavior_tree/utils/visitors.h"

#include <agnostic_behavior_tree/behavior_node.h>
#include <agnostic_behavior_tree/composite_node.h>
#include <agnostic_behavior_tree/decorator_node.h>

#include <functional>
#include <memory>

namespace yase {

// Allows to execute a generic Functor on every node
void applyVisitor(const BehaviorNode::Ptr root_node, const std::function<void(const BehaviorNode::Ptr)>& visitor) {
  if (!root_node) {
    throw std::invalid_argument("The given node is a nullptr - can not apply visitor");
  }
  // Apply visitor on this node
  visitor(root_node);
  // Apply on children if any
  if (auto composite = std::dynamic_pointer_cast<const CompositeNode>(root_node)) {
    for (const auto& child : composite->children()) {
      applyVisitor(child, visitor);
    }
  } else if (auto decorator = std::dynamic_pointer_cast<const DecoratorNode>(root_node)) {
    if (decorator->child() != nullptr) {
      applyVisitor(decorator->child(), visitor);
    }
  }
}

// Prints a tree with status from the given tree nodeon
void applyPrintVisitor(
    const BehaviorNode::Ptr root_node,
    const std::function<void(
        const BehaviorNode::Ptr, const std::string& start_prefix, const std::string& follow_prefix)>& print_visitor) {
  std::function<void(const std::string&, const BehaviorNode::Ptr, const bool)> printFunctor;
  printFunctor = [&printFunctor, &print_visitor](const std::string& prefix, const BehaviorNode::Ptr node,
                                                 const bool is_this_tail) {
    // local prefix for this node
    std::string local_prefix;
    // prefix for deeper nodes
    std::string next_prefix;
    if (is_this_tail) {
      local_prefix = prefix + "└─ ";
      next_prefix = prefix + "   ";
    } else {
      local_prefix = prefix + "├─ ";
      next_prefix = prefix + "│  ";
    }

    // print local node
    if (!node) {
      std::cout << local_prefix << "!nullptr!" << std::endl;
      return;
    }
    print_visitor(node, local_prefix, next_prefix);

    // print the children of a composite / decorator
    if (auto composite = std::dynamic_pointer_cast<const CompositeNode>(node)) {
      for (size_t i = 0; i < composite->childrenCount(); i++) {
        bool is_next_tail = false;
        if (i == composite->childrenCount() - 1) {
          is_next_tail = true;
        }
        printFunctor(next_prefix, composite->child(i), is_next_tail);
      }
    } else if (auto decorator = std::dynamic_pointer_cast<const DecoratorNode>(node)) {
      printFunctor(next_prefix, decorator->child(), true);
    }
  };

  // Print the tree starting from root_node
  printFunctor("", root_node, true);
}

}  // namespace yase
