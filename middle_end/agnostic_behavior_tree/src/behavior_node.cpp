/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "agnostic_behavior_tree/behavior_node.h"

#include <string>

namespace yase {

std::string toStr(NodeStatus status, bool use_ansi_escape_code) {
  if (!use_ansi_escape_code) {
    switch (status) {
      case NodeStatus::kSuccess:
        return "SUCCESS";
      case NodeStatus::kFailure:
        return "FAILURE";
      case NodeStatus::kRunning:
        return "RUNNING";
      case NodeStatus::kIdle:
        return "IDLE";
    }
  } else {
    switch (status) {
      case NodeStatus::kSuccess:
        return "\x1b[32m"
               "SUCCESS"
               "\x1b[0m";
      case NodeStatus::kFailure:
        return "\x1b[31m"
               "FAILURE"
               "\x1b[0m";
      case NodeStatus::kRunning:
        return "\x1b[36m"
               "RUNNING"
               "\x1b[0m";
      case NodeStatus::kIdle:
        return "IDLE";
    }
  }
  return "Undefined status!";
}

}  // namespace yase
