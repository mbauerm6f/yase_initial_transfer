# The Agnostic Behavior Tree package of the YASE framework


This package contains the agnostic behavior tree implementation for scenario execution.
The main design goal is a modular, simulator agnostic and deterministic execution.

!["Example behavior tree with execution tree traversal"](doc/figures/bt_example.png?raw=true )



## Architecture:

### Behavior Nodes:

The implementation contains several base behavior nodes: Composites, Decorators and Actions.

* Actions: Action nodes are nodes, which perform actual tasks in the simulator, such as a LaneChange.
* Decorators: Decorators allow to decorate other nodes to influence the behavior. As an example, the LaneChange behavior can be decorated with an AdjacentLaneFree condition decorator. This would prevent the execution of the LaneChange in situations in which the LaneChange not possible.
* Composites: Composites allow to compose other behaviors to greater behaviors. A Sequence composite may combine a FollowLane after a LaneChange in sequential manner.

!["Provided nodes and inheritance structure"](doc/figures/uml_class_diagram.png?raw=true )

### Scoped BlackBoard Container

Often it is necessary to share data among several nodes within the tree.
For this purpose the behavior nodes contain a scoped blackboard container to declare and look up data.
The following example demonstrates how two symbols (veh_1 and veh_2) are declared at a certain node.
This can then be accessed by sub nodes, such as the LaneChange nodes, which require access.

!["Example of symbol declaration"](doc/figures/symbol_propagation.png?raw=true )


### Extensions:

For the behavior tree usage within specific simulation tools it is often necessary to add further custom methods to nodes.
Such functionality can be added via composition with the Extension template.
In the unit test "test_extension.cpp" it is exemplary shown how a custom method serializeToFormatXyz() can be added to all nodes.



## Types of Behavior Nodes:

### Actions:
Actions must be defined for the specific use case and simulation environment.  
Therefore predefined actions are not part of this simulator agnostic package.
The existing utility actions are only for the purpose of unit testing.



### Decorators:
The package already provides some base decorators such as the simple inverter decorator.
This node inverts the node status from Failure to Success and vice versa.
Other important decorators are the ConstraintNode and the ServiceNode, which allow condition checking and providing services for subnodes.
The SymbolDeclarationNode and the SymbolProxyNode decorator allow furthermore advanced variable scoping within the tree.



### Composites:

The package provides three base composites:

* Sequence: This composite allows to execute behaviors in sequence, e.G. a FollowLane after a LaneChange.
* Parallel: This composite allows the parallel execution of behaviors e.G. two vehicles follow a lane in parallel.
* Selector: This composite allows the situation based, event based or interruption based execution of behavior. E.G. a selector can try a LaneChange, but if this is not possible, it will perform a FollowLane behavior instead.  



## Build and run tests:
### Dependencies:
* make
* cmake
* gtest
### How to build:
Create a subfolder "build" and build it:  
``` shell
mkdir build && cd build
cmake ../your/path/to/agnostic_behavior_tree && make && ./agnostic_behavior_tree_test
```

## Usage:
### Tips on how to use:

* Use predefined composites/ decorators as much as possible - Relying on them reduces compatibility problems with future versions in a great extent.
* Define Tasks of Actions with onInit(), tick() and onTerminate() method. 

## Other References:
The following references provide deeper understanding of BehaviorTrees, which are the state of the art for behavior modeling in the gaming/ robotic industry.

* https://www.gameaipro.com/GameAIPro/GameAIPro_Chapter06_The_Behavior_Tree_Starter_Kit.pdf
* https://arxiv.org/abs/1709.00084
* https://www.behaviortree.dev/
* https://docs.unrealengine.com/en-US/InteractiveExperiences/ArtificialIntelligence/BehaviorTrees/index.html