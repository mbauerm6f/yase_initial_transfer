/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef AGNOSTIC_BEHAVIOR_TREE_COMPOSITION_SEQUENCE_H
#define AGNOSTIC_BEHAVIOR_TREE_COMPOSITION_SEQUENCE_H

#include "agnostic_behavior_tree/composite_node.h"

namespace yase {
// The Sequence node
//
// The Sequence executes all children once in sequence.
// If a child behavior returns kRunning, it is executed in the next step again.
// Once it returns kSuccess, the next child is executed immediately in the same step.
// No child behavior is allowed to fail, if this happens, the whole Sequence fails
class SequenceNode : public CompositeNode {
 public:
  explicit SequenceNode(const std::string& name = "Unnamed") : SequenceNode(name, nullptr){};

  SequenceNode(Extension::Ptr extension_ptr) : SequenceNode("Unnamed", std::move(extension_ptr)){};

  SequenceNode(const std::string& name, Extension::Ptr extension_ptr)
      : CompositeNode(std::string("Sequence::").append(name), std::move(extension_ptr)){};

  virtual ~SequenceNode() override = default;

  void onInit() override {
    m_last_running_child = 0;
    this->child(m_last_running_child)->onInit();
  }

 private:
  NodeStatus tick() final {
    // Loop over all children
    for (size_t index = m_last_running_child; index < childrenCount(); index++) {
      BehaviorNode::Ptr child = this->child(index);

      const NodeStatus child_status = child->executeTick();

      switch (child_status) {
        case NodeStatus::kRunning: {
          return child_status;
        }
        case NodeStatus::kFailure: {
          child->onTerminate();
          return child_status;
        }
        case NodeStatus::kSuccess: {
          // Terminate current child and init subsequent one
          child->onTerminate();
          m_last_running_child++;
          if (m_last_running_child < childrenCount()) {
            this->child(m_last_running_child)->onInit();
          }
          break;
        }
        default: {
          std::string error_msg = "The child node [";
          error_msg.append(child->name());
          error_msg.append("] returned unkown NodeStatus.");
          throw std::invalid_argument(error_msg);
        }
      }
    }
    return NodeStatus::kSuccess;
  }

  size_t m_last_running_child{0};
};

}  // namespace yase

#endif  // AGNOSTIC_BEHAVIOR_TREE_COMPOSITION_SEQUENCE_H
