/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef AGNOSTIC_BEHAVIOR_TREE_COMPOSITION_PARALLEL_H
#define AGNOSTIC_BEHAVIOR_TREE_COMPOSITION_PARALLEL_H

#include "agnostic_behavior_tree/composite_node.h"

#include <set>

namespace yase {
/// The parallel composite
//
// The parallel node ticks all children until all of then return kSuccess.
// Once a child returns a kFailure. the parallel node is also set to kFailure.
class ParallelNode : public CompositeNode {
 public:
  explicit ParallelNode(const std::string& name = "Unnamed") : ParallelNode(name, nullptr){};

  ParallelNode(Extension::Ptr extension_ptr) : ParallelNode("Unnamed", std::move(extension_ptr)){};

  ParallelNode(const std::string& name, Extension::Ptr extension_ptr)
      : CompositeNode(std::string("Parallel::").append(name), std::move(extension_ptr)){};

  virtual ~ParallelNode() = default;

  void onInit() override {
    m_finished_children.clear();
    for (auto& child : m_children_nodes) {
      child->onInit();
    }
  };

 private:
  NodeStatus tick() final {
    NodeStatus return_status = NodeStatus::kSuccess;

    // Loop over all children
    for (size_t index = 0; index < childrenCount(); index++) {
      if (m_finished_children.find(index) == m_finished_children.end()) {
        BehaviorNode::Ptr child = this->child(index);
        const NodeStatus child_status = child->executeTick();

        switch (child_status) {
          case NodeStatus::kRunning: {
            return_status = NodeStatus::kRunning;
            break;
          }
          case NodeStatus::kFailure: {
            // Stop immediately
            child->onTerminate();
            m_finished_children.insert(index);
            return NodeStatus::kFailure;
          }
          case NodeStatus::kSuccess: {
            child->onTerminate();
            m_finished_children.insert(index);
            break;
          }
          default: {
            std::string error_msg = "The child node [";
            error_msg.append(child->name());
            error_msg.append("] returned unkown NodeStatus.");
            throw std::invalid_argument(error_msg);
          }
        }
      }
    }

    return return_status;
  }

  // Finished children
  std::set<size_t> m_finished_children;
};

}  // namespace yase

#endif  // AGNOSTIC_BEHAVIOR_TREE_COMPOSITION_PARALLEL_H
