/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef AGNOSTIC_BEHAVIOR_TREE_COMPOSITION_SELECTOR_H
#define AGNOSTIC_BEHAVIOR_TREE_COMPOSITION_SELECTOR_H

#include "agnostic_behavior_tree/composite_node.h"

namespace yase {
/// The Selector node
//
// The selector (Priority selector / Fallback) composite tries all children in sequence until one returns
// kRunning or kSuccess. If the child returns kFailure, the next child is ticked.
// If all children fail, the Selector is set also to kFailure.
class SelectorNode : public CompositeNode {
 public:
  explicit SelectorNode(const std::string& name = "Unnamed") : SelectorNode(name, nullptr){};

  SelectorNode(Extension::Ptr extension_ptr) : SelectorNode("Unamed", std::move(extension_ptr)){};

  SelectorNode(const std::string& name, Extension::Ptr extension_ptr)
      : CompositeNode(std::string("Selector::").append(name), std::move(extension_ptr)){};

  virtual ~SelectorNode() override = default;

  void onInit() override { m_last_running_child = -1; }

 private:
  NodeStatus tick() final {
    // Loop over all children
    for (int index = 0; index < childrenCount(); index++) {
      BehaviorNode::Ptr child = this->child(index);

      // Initialize child before executing it
      if (index != m_last_running_child) {
        child->onInit();
      }
      const NodeStatus child_status = child->executeTick();

      switch (child_status) {
        case NodeStatus::kRunning: {
          if (index != m_last_running_child) {
            if (m_last_running_child >= 0) {
              this->child(m_last_running_child)->onTerminate();
            }
            m_last_running_child = index;
          }
          return child_status;
        }
        case NodeStatus::kFailure: {
          child->onTerminate();
          if (index == m_last_running_child) {
            m_last_running_child = -1;
          }
          break;
        }
        case NodeStatus::kSuccess: {
          child->onTerminate();
          m_last_running_child = -1;
          return child_status;
        }
        default: {
          std::string error_msg = "The child node [";
          error_msg.append(child->name());
          error_msg.append("] returned unkown NodeStatus.");
          throw std::invalid_argument(error_msg);
        }
      }
    }

    // None of the children behavior can handle the situation
    return NodeStatus::kFailure;
  }

  // Index of last running child - negative index indicates none
  int m_last_running_child{-1};
};

}  // namespace yase

#endif  // AGNOSTIC_BEHAVIOR_TREE_COMPOSITION_SELECTOR_H
