/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef AGNOSTIC_BEHAVIOR_TREE_ACTION_NODE_H
#define AGNOSTIC_BEHAVIOR_TREE_ACTION_NODE_H

#include "agnostic_behavior_tree/behavior_node.h"

namespace yase {

// The base for all action nodes
class ActionNode : public BehaviorNode {
 public:
  explicit ActionNode(const std::string& name) : ActionNode(name, nullptr){};

  ActionNode(const std::string& name, Extension::Ptr extension_ptr)
      : BehaviorNode(std::string("Action::").append(name), std::move(extension_ptr)){};

  virtual ~ActionNode() override = default;
};

}  // namespace yase

#endif  // AGNOSTIC_BEHAVIOR_TREE_ACTION_NODE_H
