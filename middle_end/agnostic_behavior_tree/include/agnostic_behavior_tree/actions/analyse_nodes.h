/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef AGNOSTIC_BEHAVIOR_TREE_ACTIONS_ANALYSE_NODES_H
#define AGNOSTIC_BEHAVIOR_TREE_ACTIONS_ANALYSE_NODES_H

#include <agnostic_behavior_tree/action_node.h>

#include <iostream>
#include <string>

namespace yase {
// Returns N Times until it returns preffered NodeStatus. Tracks the method accesses
class AnalyseNode : public ActionNode {
 public:
  explicit AnalyseNode(const size_t max_repeat_ticks,
                       const NodeStatus return_status = NodeStatus::kSuccess,
                       Extension::Ptr extension_ptr = nullptr)
      : AnalyseNode("", max_repeat_ticks, return_status, extension_ptr){};

  AnalyseNode(const std::string name,
              const size_t max_repeat_ticks,
              const NodeStatus return_status = NodeStatus::kSuccess,
              Extension::Ptr extension_ptr = nullptr)
      : ActionNode(std::string("AnalyseNode").append(name), std::move(extension_ptr)),
        m_max_repeat_ticks(max_repeat_ticks),
        m_return_status(return_status){};

  virtual ~AnalyseNode() override = default;

  bool isInitialised() const { return m_initialised; }

  size_t overallTicks() const { return m_overall_ticks; }

  size_t ticksSinceInit() const { return m_ticks_since_init; }

  size_t onInitCalls() const { return m_on_init_calls; }

  size_t onTerminateCalls() const { return m_on_terminate_calls; }

 protected:
  NodeStatus tick() final {
    m_overall_ticks++;
    m_ticks_since_init++;
    if (m_repeat_counter < m_max_repeat_ticks) {
      m_repeat_counter++;
      executionInfo(std::string("Ticked ")
                        .append(std::to_string(m_repeat_counter))
                        .append(" of ")
                        .append(std::to_string(m_max_repeat_ticks))
                        .append(" times"));
      return NodeStatus::kRunning;
    }
    return m_return_status;
  };

  void onInit() override {
    m_repeat_counter = 0;
    m_initialised = true;
    m_ticks_since_init = 0;
    m_on_init_calls++;
  }

  void onTerminate() override {
    m_initialised = false;
    m_on_terminate_calls++;
  }

 private:
  const size_t m_max_repeat_ticks;
  const NodeStatus m_return_status;

  // Variables to be reset at onInit
  bool m_initialised{false};
  size_t m_repeat_counter{0};

  // Variables which will not be reset:
  size_t m_overall_ticks{0};
  size_t m_on_init_calls{0};
  size_t m_ticks_since_init{0};
  size_t m_on_terminate_calls{0};
};

// Returns always a Running
class AlwaysRunning : public AnalyseNode {
 public:
  AlwaysRunning(Extension::Ptr extension_ptr = nullptr)
      : AnalyseNode("AlwaysRunning", 0, NodeStatus::kRunning, std::move(extension_ptr)){};

  virtual ~AlwaysRunning() override = default;
};

// Returns always a Success
class AlwaysSuccess : public AnalyseNode {
 public:
  AlwaysSuccess(Extension::Ptr extension_ptr = nullptr)
      : AnalyseNode("AlwaysSuccess", 0, NodeStatus::kSuccess, std::move(extension_ptr)){};

  ~AlwaysSuccess() override = default;
};

// Returns always a Failure
class AlwaysFailure : public AnalyseNode {
 public:
  AlwaysFailure(Extension::Ptr extension_ptr = nullptr)
      : AnalyseNode("AlwaysFailure", 0, NodeStatus::kFailure, std::move(extension_ptr)){};

  ~AlwaysFailure() override = default;
};

}  // namespace yase

#endif  // AGNOSTIC_BEHAVIOR_TREE_ACTIONS_ANALYSE_NODES_H
