/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef AGNOSTIC_BEHAVIOR_TREE_ACTIONS_FUNCTOR_ACTION_NODE_H
#define AGNOSTIC_BEHAVIOR_TREE_ACTIONS_FUNCTOR_ACTION_NODE_H

#include <agnostic_behavior_tree/action_node.h>

#include <functional>

namespace yase {
// A simple action node which allows to express logic via a lambda.
// This node eases to execute generic logic with a Functor.
class FunctorActionNode : public ActionNode {
 public:
  using TickFunctor = std::function<NodeStatus()>;

  FunctorActionNode(const std::string& name,
                    FunctorActionNode::TickFunctor tick_functor,
                    Extension::Ptr extension_ptr = nullptr)
      : ActionNode(name, extension_ptr), m_tick_functor(std::move(tick_functor)){};

  explicit FunctorActionNode(FunctorActionNode::TickFunctor tick_functor, Extension::Ptr extension_ptr = nullptr)
      : FunctorActionNode("UnnamedFunctorAction", std::move(tick_functor), extension_ptr){};

  virtual ~FunctorActionNode() override = default;

  void onInit() override{};

 protected:
  NodeStatus tick() final { return m_tick_functor(); };

  TickFunctor m_tick_functor;
};

}  // namespace yase

#endif  // AGNOSTIC_BEHAVIOR_TREE_ACTIONS_FUNCTOR_ACTION_NODE_H
