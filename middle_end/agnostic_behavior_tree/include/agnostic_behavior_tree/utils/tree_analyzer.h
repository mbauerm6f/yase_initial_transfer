/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef AGNOSTIC_BEHAVIOR_TREE_UTILS_TREE_ANALYZER_H
#define AGNOSTIC_BEHAVIOR_TREE_UTILS_TREE_ANALYZER_H

#include <agnostic_behavior_tree/behavior_node.h>
#include <agnostic_behavior_tree/utils/visitors.h>

#include <vector>

namespace yase {
// Check if all nodes contain a valid extension (not NULL), which can be casted to type T
template <class T>
bool extensionsOfTreeDefined(const BehaviorNode::Ptr root_node) {
  bool all_nodes_have_valid_extension{true};
  auto countFunctor = [&all_nodes_have_valid_extension](const BehaviorNode::Ptr node) mutable {
    if (!node->extensionExists()) {
      all_nodes_have_valid_extension = false;
      return;
    }
    if (auto extension_castable = std::dynamic_pointer_cast<const T>(node->extension())) {
      all_nodes_have_valid_extension &= true;
    }
  };

  applyVisitor(root_node, countFunctor);
  return all_nodes_have_valid_extension;
}

// Counts all nodes including the root
size_t countNodes(const BehaviorNode::Ptr root_node);

// Get all failing nodes of the tree
std::vector<BehaviorNode::Ptr> getAllFailingNodes(const BehaviorNode::Ptr& root_node);

}  // namespace yase

#endif  // AGNOSTIC_BEHAVIOR_TREE_UTILS_TREE_ANALYZER_H
