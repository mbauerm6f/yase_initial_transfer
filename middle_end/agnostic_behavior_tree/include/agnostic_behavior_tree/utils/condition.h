/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef AGNOSTIC_BEHAVIOR_TREE_UTILS_CONDITION_H
#define AGNOSTIC_BEHAVIOR_TREE_UTILS_CONDITION_H

#include <agnostic_behavior_tree/scoped_blackboard.h>

#include <algorithm>
#include <functional>
#include <string>
#include <vector>

namespace yase {
// The Condition interface
//
// Look up required symbols within the Blackboard and evaluate condition
class Condition {
 public:
  using UPtr = std::unique_ptr<Condition>;

  explicit Condition(const std::string& condition_name) : name(condition_name){};
  virtual ~Condition() = default;
  Condition(const Condition& copy_from) = delete;
  Condition& operator=(const Condition& copy_from) = delete;
  Condition(Condition&&) = default;
  Condition& operator=(Condition&&) = delete;

  // Look up required symbols for condition evaluation.
  // Executed once when the symbols within the behavior tree are resolved.
  virtual void lookUpSharedSymbols(const BlackboardReader& /*blackboard*/){};

  // Called before first evaluation - MUST RESET THE CONDITION!
  virtual void onInit() = 0;

  // Evaluate condition
  virtual bool evaluate() = 0;

  const std::string name;
};

class ConditionFunctor : public Condition {
 public:
  using EvaluationFunction = std::function<bool()>;

  ConditionFunctor(const std::string& condition_name, EvaluationFunction function)
      : Condition(condition_name), m_function(std::move(function)){};

  // Nothing to reset
  void onInit() final{};

  // Evaluate condition
  bool evaluate() final { return m_function(); };

 private:
  const EvaluationFunction m_function;
};

// Inverts child condition
class NotCondition : public Condition {
 public:
  explicit NotCondition(Condition::UPtr condition)
      : Condition(getConditionName(*condition)), m_condition(std::move(condition)){};

  // Look up required symbols for condition evaluation.
  // Executed once when the symbols within the behavior tree are resolved.
  void lookUpSharedSymbols(const BlackboardReader& blackboard) final { m_condition->lookUpSharedSymbols(blackboard); };

  // Called before first evaluation - MUST RESET THE CONDITION!
  void onInit() final { m_condition->onInit(); }

  // Evaluate condition
  bool evaluate() final { return !m_condition->evaluate(); };

 private:
  // Creates condition name with given condition
  static std::string getConditionName(const Condition& condition) {
    std::string name = "Not[";
    return name.append(condition.name).append("]");
  }

  Condition::UPtr m_condition;
};

// Evaluates multiple conditions --> all must evaluate to true
class AllOfConditions : public Condition {
 public:
  explicit AllOfConditions(std::vector<Condition::UPtr> conditions)
      : Condition(getConditionName(conditions)), m_conditions(std::move(conditions)){};

  // Look up required symbols for condition evaluation.
  // Executed once when the symbols within the behavior tree are resolved.
  void lookUpSharedSymbols(const BlackboardReader& blackboard) final {
    for (const auto& condition : m_conditions) {
      condition->lookUpSharedSymbols(blackboard);
    }
  };

  // Called before first evaluation - MUST RESET THE CONDITION!
  void onInit() final {
    for (const auto& condition : m_conditions) {
      condition->onInit();
    }
  }

  // Evaluate condition
  bool evaluate() final {
    return (std::all_of(m_conditions.begin(), m_conditions.end(),
                        [](Condition::UPtr& conidtion) { return conidtion->evaluate(); }));
  };

 private:
  // Creates condition name with given conditions
  static std::string getConditionName(const std::vector<Condition::UPtr>& conditions) {
    std::string name = "AllOf[";
    // Add first condition without delimiter in front
    if (conditions.size() > 1) {
      name.append(conditions[0]->name);
    }
    for (size_t idx = 1; idx < conditions.size(); idx++) {
      name.append("&&").append(conditions[idx]->name);
    }
    return name.append("]");
  }

  std::vector<Condition::UPtr> m_conditions;
};

// Evaluates multiple conditions --> any of the conditions must evaluate to true
class AnyOfConditions : public Condition {
 public:
  explicit AnyOfConditions(std::vector<Condition::UPtr> conditions)
      : Condition(getConditionName(conditions)), m_conditions(std::move(conditions)){};

  // Look up required symbols for condition evaluation.
  // Executed once when the symbols within the behavior tree are resolved.
  void lookUpSharedSymbols(const BlackboardReader& blackboard) final {
    for (const auto& condition : m_conditions) {
      condition->lookUpSharedSymbols(blackboard);
    }
  };

  // Called before first evaluation - MUST RESET THE CONDITION!
  void onInit() final {
    for (const auto& condition : m_conditions) {
      condition->onInit();
    }
  }

  // Evaluate condition
  bool evaluate() final {
    return (std::any_of(m_conditions.begin(), m_conditions.end(),
                        [](Condition::UPtr& conidtion) { return conidtion->evaluate(); }));
  };

 private:
  // Creates condition name with given conditions
  static std::string getConditionName(const std::vector<Condition::UPtr>& conditions) {
    std::string name = "AnyOf[";
    // Add first condition without delimiter in front
    if (conditions.size() > 1) {
      name.append(conditions[0]->name);
    }
    for (size_t idx = 1; idx < conditions.size(); idx++) {
      name.append("||").append(conditions[idx]->name);
    }
    return name.append("]");
  }

  std::vector<Condition::UPtr> m_conditions;
};

}  // namespace yase

#endif  // AGNOSTIC_BEHAVIOR_TREE_UTILS_CONDITION_H
