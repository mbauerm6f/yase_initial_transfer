/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef AGNOSTIC_BEHAVIOR_TREE_UTILS_TREE_PRINT_H
#define AGNOSTIC_BEHAVIOR_TREE_UTILS_TREE_PRINT_H

#include <agnostic_behavior_tree/behavior_node.h>
#include <agnostic_behavior_tree/utils/visitors.h>

namespace yase {
// Converts the node into an output string
std::string nodeToStr(const BehaviorNode::Ptr node, const bool ansi_escape_code = true, const bool print_ticks = false);

// Prints a tree with status from the given tree node on
void printTree(const BehaviorNode::Ptr root_node);

// Prints a tree with status from the given tree nodeon
void printTreeWithSymbols(const BehaviorNode::Ptr root_node);

// Prints tree with states and execution info
void printTreeWithStates(const BehaviorNode::Ptr root_node);

// Prints tree with failure traceback (also with captured failures, which did not cause root failure!)
void printTreeWithFailureTraces(const BehaviorNode::Ptr root_node);

}  // namespace yase

#endif  // AGNOSTIC_BEHAVIOR_TREE_UTILS_TREE_PRINT_H
