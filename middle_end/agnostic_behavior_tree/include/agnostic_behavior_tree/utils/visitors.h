/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef AGNOSTIC_BEHAVIOR_TREE_UTILS_VISITORS_H
#define AGNOSTIC_BEHAVIOR_TREE_UTILS_VISITORS_H

#include <agnostic_behavior_tree/behavior_node.h>

#include <functional>
#include <memory>

namespace yase {

// Allows to execute a generic Functor on every node
void applyVisitor(const BehaviorNode::Ptr root_node, const std::function<void(const BehaviorNode::Ptr)>& visitor);

// Prints a tree with status from the given tree nodeon
void applyPrintVisitor(
    const BehaviorNode::Ptr root_node,
    const std::function<void(
        const BehaviorNode::Ptr, const std::string& start_prefix, const std::string& follow_prefix)>& print_visitor);

}  // namespace yase

#endif  // AGNOSTIC_BEHAVIOR_TREE_UTILS_VISITORS_H
