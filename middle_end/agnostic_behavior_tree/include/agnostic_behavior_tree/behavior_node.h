/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef AGNOSTIC_BEHAVIOR_TREE_BEHAVIOR_NODE_H
#define AGNOSTIC_BEHAVIOR_TREE_BEHAVIOR_NODE_H

#include "agnostic_behavior_tree/scoped_blackboard.h"

#include <exception>
#include <string>

namespace yase {

// Execution states of a node
enum class NodeStatus { kIdle = 0, kRunning, kSuccess, kFailure };

// Convert the status into an ouput string
std::string toStr(NodeStatus status, bool use_ansi_escape_code = false);

// Simulator specific Extensions can be inherited and composed to behavior nodes
class Extension {
 public:
  Extension() = default;
  virtual ~Extension() = default;

  using Ptr = std::shared_ptr<Extension>;
};

// Abstract base class for Behavior Tree Nodes.
class BehaviorNode {
 public:
  virtual ~BehaviorNode() { m_blackboard = nullptr; };

  using Ptr = std::shared_ptr<BehaviorNode>;

  // Method to invoke tick from the outside while ensuring the consistency of the node status and tick cycle tracking
  NodeStatus executeTick() {
    // Track current tick cycle
    if (m_parent_node != nullptr) {
      if (m_tick_cycle >= m_parent_node->m_tick_cycle) {
        std::string error_msg = "Error while executing tick() of behavior node [";
        error_msg.append(name());
        error_msg.append("]: Tick cycle [")
            .append(std::to_string(m_tick_cycle))
            .append("] of this node is greater/equal then its parent tick cycle [");
        error_msg.append(std::to_string(m_parent_node->m_tick_cycle))
            .append("] - A child is not allowed to proceed faster then its parent");
        throw std::logic_error(error_msg);
      }
      m_tick_cycle = m_parent_node->m_tick_cycle;
    } else {
      (m_tick_cycle)++;
    }
    // Clear execution info before step
    m_execution_info.clear();
    m_status = tick();
    if (m_status == NodeStatus::kIdle) {
      std::string error_msg = "Error while executing tick() of behavior node [";
      error_msg.append(name());
      error_msg.append("]: Returned invalid status NodeStatus::kIdle!");
      throw std::logic_error(error_msg);
    }
    return status();
  };

  // Called once right before behavior is ticked the first time.
  // THE CALL MUST RESET THE BEHAVIOR with all its necessary values!
  virtual void onInit() = 0;

  // Called after behavior is called the last time
  virtual void onTerminate(){};

  // Get node status
  NodeStatus status() const { return m_status; };

  // Get detailed execution info
  std::string executionInfo() const { return m_execution_info; };

  // Get tree node name
  const std::string& name() const { return m_name; };

  // Get BlackboardBase to examine symbols
  BlackboardBase::Ptr blackboardBase() const { return m_blackboard; };

  // Init the sub tree (node with sub nodes)
  virtual void initTree() {
    m_blackboard->clearLocal();
    declareSymbolsInBlackBoard(*(m_blackboard));
    lookUpSymbolsInBlackBoard(*(m_blackboard));
  };

  // Get current tick cycle of node
  unsigned int tickCycle() const { return m_tick_cycle; }

  // Checks if node was ticked in current tick cycle
  bool tickedInCurrentCycle() const {
    if (m_parent_node == nullptr) {
      return true;
    }
    if (m_parent_node->tickedInCurrentCycle()) {
      return (m_parent_node->tickCycle() == m_tick_cycle);
    }
    return false;
  }

  // Check if it is safe to access the extension
  bool extensionExists() const { return (m_extension != nullptr); }

  // Get Extension, will throw if null
  Extension::Ptr extension() const {
    if (!extensionExists()) {
      std::string exception_msg = "Trying to access the extension in node [";
      exception_msg.append(name());
      exception_msg.append("] which is NULL.");
      throw std::runtime_error(exception_msg);
    }
    return m_extension;
  };

  template <class T>
  const T& extensionCast() const {
    return dynamic_cast<const T&>(*extension());
  }

  template <class T>
  T& extensionCast() {
    return dynamic_cast<T&>(*extension());
  }

 protected:
  // Method which defines behavior logic of node
  virtual NodeStatus tick() = 0;

  // Allows to declare symbols in blackboard (called by initTree()) - can be called multiple times
  virtual void declareSymbolsInBlackBoard(BlackboardWriter& /*blackboard*/){};

  // Allows to lookUp symbols in blackboard (called by initTree() and after declaring of symbols) - can be called
  // multiple times
  virtual void lookUpSymbolsInBlackBoard(const BlackboardReader& /*blackboard*/){};

  // Set detailed execution info
  void executionInfo(const std::string& new_execution_info) { m_execution_info = new_execution_info; };

 private:
  // BehaviorNode constructor with simulator specifc extension
  // Ctor is private and only accessible to friend classes: Action, Decorator and Composites
  BehaviorNode(const std::string& name, Extension::Ptr extension = nullptr)
      : m_name(name), m_blackboard(std::make_shared<Blackboard>(name)), m_extension(std::move(extension)){};

  // Node name
  const std::string m_name;

  // Additional information about the execution process
  std::string m_execution_info;

  // Node execution state
  NodeStatus m_status{NodeStatus::kIdle};

  // Tracks the last tick cycle
  unsigned int m_tick_cycle{0};

  // Pointer to parent node (if existent)
  BehaviorNode* m_parent_node{nullptr};

  // Local blackboard
  Blackboard::Ptr m_blackboard{nullptr};

  // Simulator specific functionality extension
  const Extension::Ptr m_extension{nullptr};

  // Friend classes, which need full access to blackboard
  friend class CompositeNode;
  friend class DecoratorNode;
  friend class ActionNode;
  friend class SymbolProxyNode;
};

}  // namespace yase

#endif  // AGNOSTIC_BEHAVIOR_TREE_BEHAVIOR_NODE_H
