/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef AGNOSTIC_BEHAVIOR_TREE_COMPOSITE_NODE_H
#define AGNOSTIC_BEHAVIOR_TREE_COMPOSITE_NODE_H

#include "agnostic_behavior_tree/behavior_node.h"

#include <algorithm>
#include <vector>

namespace yase {
// The abstract composite node
//
// Composites contain children behaviors, which they call with certain strategies.
class CompositeNode : public BehaviorNode {
 public:
  // CompositeNode constructor - it is mandatory to name the node
  explicit CompositeNode(const std::string& name) : CompositeNode(name, nullptr){};

  // Ctor with extension
  CompositeNode(const std::string& name, Extension::Ptr extension_ptr)
      : BehaviorNode(std::string("Composite::").append(name), std::move(extension_ptr)){};

  virtual ~CompositeNode() override = default;

  using Ptr = std::shared_ptr<CompositeNode>;

  // Add an additional child
  void addChild(BehaviorNode::Ptr child) {
    if (!child) {
      std::string error_msg = "CompositeNode [";
      error_msg.append(name());
      error_msg.append("]: Added child behavior is NULL.");
      throw std::invalid_argument(error_msg);
    }
    child->m_parent_node = this;
    child->m_blackboard->setParentBlackboard(m_blackboard);
    m_children_nodes.push_back(child);
  };

  // Get the count of children of this composite
  size_t childrenCount() const { return m_children_nodes.size(); };

  // Get all children
  const std::vector<BehaviorNode::Ptr>& children() const { return m_children_nodes; };

  // Get certain child with the index
  const BehaviorNode::Ptr child(size_t index) const {
    if (index >= childrenCount()) {
      std::string error_msg = "Error: Trying to access child of composite [";
      error_msg.append(name());
      error_msg.append("] with index [");
      error_msg.append(std::to_string(index));
      error_msg.append("] but it has only [");
      error_msg.append(std::to_string(childrenCount()));
      error_msg.append("] child behaviors.");
      throw std::runtime_error(error_msg);
    }
    return children().at(index);
  }

  // Resolve all symbols locally and for children
  void initTree() final {
    m_blackboard->clearLocal();
    declareSymbolsInBlackBoard(*(m_blackboard));
    lookUpSymbolsInBlackBoard(*(m_blackboard));

    std::for_each(m_children_nodes.begin(), m_children_nodes.end(), [](BehaviorNode::Ptr child) { child->initTree(); });
  };

  // Terminates all other children
  void onTerminate() final {
    for (auto& child : m_children_nodes) {
      child->onTerminate();
    }
  };

 protected:
  // The stored children nodes
  std::vector<BehaviorNode::Ptr> m_children_nodes;
};

}  // namespace yase

#endif  // AGNOSTIC_BEHAVIOR_TREE_COMPOSITE_NODE_H
