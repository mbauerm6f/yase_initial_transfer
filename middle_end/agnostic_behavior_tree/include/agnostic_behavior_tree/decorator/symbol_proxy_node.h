/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef AGNOSTIC_BEHAVIOR_TREE_DECORATOR_SYMBOL_PROXY_NODE_H
#define AGNOSTIC_BEHAVIOR_TREE_DECORATOR_SYMBOL_PROXY_NODE_H

#include "agnostic_behavior_tree/decorator/symbol_declaration_node.h"
#include "agnostic_behavior_tree/decorator_node.h"

#include <vector>

namespace yase {
// The ProxyArguments interface
//
// One can create an individual argument declaration by inheriting from this interface.
// It allows to proxy symbols with same or different keys and declare additional symbols.
class ProxyArguments {
 public:
  using Ptr = std::shared_ptr<ProxyArguments>;

  // Provides a proxy table with keys and declare additional symbols.
  virtual Blackboard::ProxyTable createProxyTableAndDeclareSharedSymbols(BlackboardWriter& /*blackboard*/) {
    return Blackboard::ProxyTable();
  };

  // Look up symbols
  virtual void lookUpSharedSymbols(const BlackboardReader& /*blackboard*/){};
};

// The SymbolProxyNode
//
// Allows to proxy symbols and additional symbols as local symbol.
// All keys which are not proxied are not accessible in the sub tree
//
// This decorator acts like the function argument list in func2:
// Passed arguments are remmapped, the rest is set to default
// The rest of the symbols from the outer scope are not visible/ accessible!
//
// void func1() {
//    int a = 1;
//    int b = 2;
//    func2(a);
// }
//
// void func2(int arg_a, int arg_b = 5) {
//    // legal to use, will print "11", as arg_a == a
//    std::cout << std::to_string(arg_a + 10) << std::endl;
//    // legal to use, will print "15", as arg_b == default value 5
//    std::cout << std::to_string(arg_b + 10) << std::endl;
//    // Illegal, as b is not accessible (is in outer scope and not remapped)
//    std::cout << std::to_string(b + 10) << std::endl;
// }
class SymbolProxyNode : public DecoratorNode {
 public:
  // Ctor for proxy argument
  SymbolProxyNode(const std::string& name,
                  const ProxyArguments::Ptr proxy_arguments,
                  Extension::Ptr extension_ptr = nullptr)
      : DecoratorNode(std::string("SymbolProxyNode::").append(name), std::move(extension_ptr)),
        m_proxy_arguments(std::move(proxy_arguments)){};

  virtual ~SymbolProxyNode() override = default;

 private:
  // Ticks the decorated child node directly
  NodeStatus tick() override { return m_child_node->executeTick(); };

  // Declare local symbols and add argument proxy
  void declareSymbolsInBlackBoard(BlackboardWriter& blackboard) final {
    Blackboard::ProxyTable remapped_keys = m_proxy_arguments->createProxyTableAndDeclareSharedSymbols(blackboard);
    m_blackboard->setProxyTable(remapped_keys);
  };

  // LookUp symbols
  void lookUpSymbolsInBlackBoard(const BlackboardReader& blackboard) final {
    m_proxy_arguments->lookUpSharedSymbols(blackboard);
  };

  // The managed proxy_arguments
  ProxyArguments::Ptr m_proxy_arguments;
};

}  // namespace yase

#endif  // AGNOSTIC_BEHAVIOR_TREE_DECORATOR_SYMBOL_PROXY_NODE_H
