/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef AGNOSTIC_BEHAVIOR_TREE_DECORATOR_WAIT_NODE_H
#define AGNOSTIC_BEHAVIOR_TREE_DECORATOR_WAIT_NODE_H

#include "agnostic_behavior_tree/decorator_node.h"

#include <agnostic_behavior_tree/utils/condition.h>

namespace yase {

// Waits with sub child execution until condition is met first time
class Wait : public DecoratorNode {
 public:
  Wait(Condition::UPtr condition, Extension::Ptr extension_ptr = nullptr)
      : DecoratorNode(std::string("Wait::").append(condition->name), std::move(extension_ptr)),
        m_condition(std::move(condition)){};

  virtual ~Wait() override = default;

 private:
  // Waits until condition is evaluated once to true
  NodeStatus tick() final {
    if (!m_done) {
      if (!m_condition->evaluate()) {
        return NodeStatus::kRunning;
      } else {
        m_done = true;
        m_child_node->onInit();
      }
    }
    return m_child_node->executeTick();
  };

  // onInit() is called is called directly before first evlauation - resets the behavior
  void onInit() final {
    m_done = false;
    m_condition->onInit();
  };

  // LookUp all symbols of the services
  void lookUpSymbolsInBlackBoard(const BlackboardReader& blackboard) final {
    m_condition->lookUpSharedSymbols(blackboard);
  };

  // The condition to evaluate
  Condition::UPtr m_condition;

  // Indicates if already evaluated to true
  bool m_done{false};
};

}  // namespace yase

#endif  // AGNOSTIC_BEHAVIOR_TREE_DECORATOR_WAIT_NODE_H
