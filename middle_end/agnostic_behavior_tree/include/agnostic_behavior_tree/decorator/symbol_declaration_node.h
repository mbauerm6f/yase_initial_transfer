/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef AGNOSTIC_BEHAVIOR_TREE_DECORATOR_SYMBOL_DECLARATION_NODE_H
#define AGNOSTIC_BEHAVIOR_TREE_DECORATOR_SYMBOL_DECLARATION_NODE_H

#include "agnostic_behavior_tree/decorator_node.h"

#include <vector>

namespace yase {

// The generic SymbolDeclarer interface
//
// One can inherit form this interface for one or more individual symbol declarations.
class SymbolDeclarer {
 public:
  using Ptr = std::shared_ptr<SymbolDeclarer>;

  // Declare shared symbols for the child nodes.
  virtual void declareSharedSymbols(BlackboardWriter& blackboard) = 0;

  // Allows to look up additional symbols
  virtual void lookUpSharedSymbols(const BlackboardReader& /*blackboard*/){};
};

// The specifc TypeDeclarer
//
// Delacres a value of specifc type and key
template <typename T>
class TypeDeclarer : public SymbolDeclarer {
 public:
  TypeDeclarer(const std::string& key, std::shared_ptr<T> m_valueptr) : m_key(key), m_value(std::move(m_valueptr)){};
  explicit TypeDeclarer(const std::string& key) : m_key(key), m_value(std::make_shared<T>()){};

  // Declare shared symbol
  void declareSharedSymbols(BlackboardWriter& blackboard) final { blackboard.set<std::shared_ptr<T>>(m_key, m_value); };

 private:
  const std::string m_key;
  const std::shared_ptr<T> m_value;
};

/// The SymbolDeclarationNode
//
// This node allows to declare an additional symbol for the sub tree.
//
// This acts like the following example with nested functions:
// declared symbols are visible in this scope and deeper nested scopes.
//
// void func1() {
//    // scope 1
//    int a = 1;
//
//    { // scope_2 - We declare an additional symbol
//    int b = 2;
//
//      { // scope_3
//          // LEGAL:  The declared symbols a and b are visible here
//          a = 5;
//          b = 10;
//      }
//    }
//
//    // ILLEGAL: b is not visible /accessable in this scope!
//    b = 5;
//
// }
class SymbolDeclarationNode : public DecoratorNode {
 public:
  // Ctor for named node and multiple managed declarations
  SymbolDeclarationNode(const std::string& name,
                        const std::vector<SymbolDeclarer::Ptr> managed_declarations,
                        Extension::Ptr extension_ptr = nullptr)
      : DecoratorNode(std::string("SymbolDeclarationNode::").append(name), std::move(extension_ptr)),
        m_managed_declarations(std::move(managed_declarations)){};

  // Convenience ctor for only one managed declarations
  SymbolDeclarationNode(const std::string& name,
                        const SymbolDeclarer::Ptr managed_declaration,
                        Extension::Ptr extension_ptr = nullptr)
      : SymbolDeclarationNode(name,
                              std::move(std::vector<SymbolDeclarer::Ptr>{std::move(managed_declaration)}),
                              std::move(extension_ptr)){};

  virtual ~SymbolDeclarationNode() override = default;

 private:
  // Ticks the decorated child node direct
  NodeStatus tick() final { return m_child_node->executeTick(); };

  // Insert all symbols of the declarations
  void declareSymbolsInBlackBoard(BlackboardWriter& blackboard) final {
    for (auto& declaration : m_managed_declarations) {
      declaration->declareSharedSymbols(blackboard);
    }
  };

  // LookUp all symbols of the declarations
  void lookUpSymbolsInBlackBoard(const BlackboardReader& blackboard) final {
    for (auto& declaration : m_managed_declarations) {
      declaration->lookUpSharedSymbols(blackboard);
    }
  };

  // The managed declarations
  std::vector<SymbolDeclarer::Ptr> m_managed_declarations;
};

}  // namespace yase

#endif  // AGNOSTIC_BEHAVIOR_TREE_DECORATOR_SYMBOL_DECLARATION_NODE_H
