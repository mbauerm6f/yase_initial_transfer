/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef AGNOSTIC_BEHAVIOR_TREE_DECORATOR_CONSTRAINT_NODE_H
#define AGNOSTIC_BEHAVIOR_TREE_DECORATOR_CONSTRAINT_NODE_H

#include "agnostic_behavior_tree/decorator_node.h"

#include <agnostic_behavior_tree/utils/condition.h>

#include <algorithm>
#include <exception>

namespace yase {

// Evaluation phase of a constraint:
//
// kInit: Constraint is checked once when tree is initialized
// kPre: Constraint is checked once, before sub behavior is executed
// kRuntime: Constraint is checked always before sub behavior is executed
// kPost: Constraint is checked after sub behavior returned Success
enum class EvaluationPhase { kInit, kPre, kRuntime, kPost };

std::string toStr(const EvaluationPhase& phase) {
  switch (phase) {
    case EvaluationPhase::kInit:
      return "Init";
    case EvaluationPhase::kPre:
      return "Pre";
    case EvaluationPhase::kRuntime:
      return "Runtime";
    case EvaluationPhase::kPost:
      return "Post";
    default:
      return "UnkownEvaluationPhase";
  }
}

using TimedConstraint = std::pair<EvaluationPhase, Condition::UPtr>;

// This decorator checks constraints for a subtree.
class ConstraintNode : public DecoratorNode {
 public:
  // Ctor for one timed constraints
  ConstraintNode(TimedConstraint condition, Extension::Ptr extension_ptr = nullptr)
      : DecoratorNode(
            std::string("ConstraintNode::").append(toStr(condition.first)).append("::").append(condition.second->name),
            std::move(extension_ptr)) {
    switch (condition.first) {
      case EvaluationPhase::kInit:
        m_init_constraint = std::move(condition.second);
        break;
      case EvaluationPhase::kPre:
        m_pre_constraint = std::move(condition.second);
        break;
      case EvaluationPhase::kRuntime:
        m_runtime_constraint = std::move(condition.second);
        break;
      case EvaluationPhase::kPost:
        m_post_constraint = std::move(condition.second);
        break;
      default:
        break;
    }
  };

  virtual ~ConstraintNode() override = default;

 private:
  // Override to prevent too soon initialisation of child -> should only initialised after pre constraint is fullfiled
  void onInit() final { m_first_call = true; };

  // Evaluate the necessary constraints before executing the decorated child node
  NodeStatus tick() final {
    // Evaluate pre check
    if (m_first_call) {
      if (m_pre_constraint != nullptr) {
        if (!m_pre_constraint->evaluate()) {
          executionInfo(createErrorMsg("pre evaluation", m_pre_constraint->name));
          return NodeStatus::kFailure;
        }
      }
      m_first_call = false;
      m_child_node->onInit();
    }

    // Check runtime condition
    if (m_runtime_constraint) {
      if (!m_runtime_constraint->evaluate()) {
        executionInfo(createErrorMsg("runtime evaluation", m_runtime_constraint->name));
        return NodeStatus::kFailure;
      }
    }

    // Execute the child behavior
    NodeStatus child_status = m_child_node->executeTick();

    // If successfully finished --> post checks
    if (child_status == NodeStatus::kSuccess) {
      if (m_post_constraint) {
        if (!m_post_constraint->evaluate()) {
          executionInfo(createErrorMsg("post evaluation", m_post_constraint->name));
          return NodeStatus::kFailure;
        }
      }
    }

    // All constraints fulfilled --> return child status like there were no condition evaluation
    return child_status;
  };

  // LookUp all symbols for all constraints
  void lookUpSymbolsInBlackBoard(const BlackboardReader& blackboard) final {
    if (m_init_constraint != nullptr) {
      m_init_constraint->lookUpSharedSymbols(blackboard);
      if (!m_init_constraint->evaluate()) {
        throw std::runtime_error(createErrorMsg("init time", m_init_constraint->name));
      }
    }
    if (m_pre_constraint != nullptr) {
      m_pre_constraint->lookUpSharedSymbols(blackboard);
    }
    if (m_runtime_constraint != nullptr) {
      m_runtime_constraint->lookUpSharedSymbols(blackboard);
    }
    if (m_post_constraint != nullptr) {
      m_post_constraint->lookUpSharedSymbols(blackboard);
    }
  };

  // Convenience method
  std::string createErrorMsg(const std::string& constraint_time, const std::string& constraint_info) {
    std::string error_msg = "Violation of constraint [";
    error_msg.append(constraint_info);
    error_msg.append("] at ");
    error_msg.append(constraint_time);
    error_msg.append(".");
    return error_msg;
  };

  // Managed constraints
  Condition::UPtr m_init_constraint;
  Condition::UPtr m_pre_constraint;
  Condition::UPtr m_runtime_constraint;
  Condition::UPtr m_post_constraint;

  bool m_first_call{true};
};

}  // namespace yase

#endif  // AGNOSTIC_BEHAVIOR_TREE_DECORATOR_CONSTRAINT_NODE_H
