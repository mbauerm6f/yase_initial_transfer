/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef AGNOSTIC_BEHAVIOR_TREE_DECORATOR_UNTIL_NODE_H
#define AGNOSTIC_BEHAVIOR_TREE_DECORATOR_UNTIL_NODE_H

#include "agnostic_behavior_tree/decorator_node.h"

#include <agnostic_behavior_tree/utils/condition.h>

namespace yase {

// Executes child behavior until condition is met.
//
// - If child behavior finishes earlier, it will also finish
class Until : public DecoratorNode {
 public:
  Until(Condition::UPtr condition, Extension::Ptr extension_ptr = nullptr)
      : DecoratorNode(std::string("Until::").append(condition->name), std::move(extension_ptr)),
        m_condition(std::move(condition)){};

  virtual ~Until() override = default;

 private:
  // Executes child behavior until condition is evaluated to true
  NodeStatus tick() final {
    if (m_condition->evaluate()) {
      return NodeStatus::kSuccess;
    }
    return m_child_node->executeTick();
  };

  // LookUp all symbols of the services
  void lookUpSymbolsInBlackBoard(const BlackboardReader& blackboard) final {
    m_condition->lookUpSharedSymbols(blackboard);
  };

  // onInit() is called is called directly before first evlauation - resets the behavior
  void onInit() final {
    m_condition->onInit();
    DecoratorNode::onInit();
  };

  // The condition to evaluate
  Condition::UPtr m_condition;
};

}  // namespace yase

#endif  // AGNOSTIC_BEHAVIOR_TREE_DECORATOR_UNTIL_NODE_H
