/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef AGNOSTIC_BEHAVIOR_TREE_DECORATOR_REPEAT_NODE_H
#define AGNOSTIC_BEHAVIOR_TREE_DECORATOR_REPEAT_NODE_H

#include "agnostic_behavior_tree/decorator_node.h"

namespace yase {

// Repeats a behavior n times
class Repeat : public DecoratorNode {
 public:
  Repeat(const size_t repeat_times, Extension::Ptr extension_ptr = nullptr)
      : DecoratorNode(createNodeName(repeat_times), std::move(extension_ptr)), m_desired_repeats(repeat_times){};

  virtual ~Repeat() override = default;

  // onInit() is called is called directly before first evaluation - resets the behavior
  void onInit() final {
    m_child_initialised = false;
    m_counter = 0;
  };

  static std::string createNodeName(const size_t repeat_times) {
    std::string node_name = ("Repeat[");
    if (repeat_times == 0) {
      node_name.append("Infinite");
    } else {
      node_name.append(std::to_string(repeat_times));
    }
    node_name.append("]Times");
    return node_name;
  }

  const size_t repeatTimes() const { return m_desired_repeats; }

 private:
  // Waits until condition is evaluated once to true
  NodeStatus tick() final {
    if (m_counter >= m_desired_repeats && m_desired_repeats != 0) {
      addExecutionInfo();
      return NodeStatus::kSuccess;
    }

    if (!m_child_initialised) {
      m_child_node->onInit();
      m_child_initialised = true;
    }

    NodeStatus child_status = m_child_node->executeTick();

    if (child_status == NodeStatus::kSuccess) {
      m_child_node->onTerminate();
      m_child_initialised = false;
      m_counter++;
      if (m_counter < m_desired_repeats) {
        child_status = NodeStatus::kRunning;
      }
    }

    addExecutionInfo();
    return child_status;
  };

  // Adds execution info about current repeat state
  void addExecutionInfo() {
    std::string info = ("Repeated child behavior ");
    info.append(std::to_string(m_counter));
    if (m_desired_repeats != 0) {
      info.append(" out of ").append(std::to_string(m_desired_repeats));
    }
    info.append(" times");
    executionInfo(info);
  }

  // Indicates if child was already indicated before
  bool m_child_initialised{false};

  // Indicates if already evaluated to true
  size_t m_counter{0};

  // Desired of successfull repeats
  size_t m_desired_repeats;
};

}  // namespace yase

#endif  // AGNOSTIC_BEHAVIOR_TREE_DECORATOR_REPEAT_NODE_H
