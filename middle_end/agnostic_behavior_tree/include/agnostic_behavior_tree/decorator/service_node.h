/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef AGNOSTIC_BEHAVIOR_TREE_DECORATOR_SERVICE_NODE_H
#define AGNOSTIC_BEHAVIOR_TREE_DECORATOR_SERVICE_NODE_H

#include "agnostic_behavior_tree/decorator_node.h"

#include <vector>

namespace yase {
// Provides one or more services accessible for the subtree.
// Services are updated before and after the tick of the decoratorated child.
//
// Examples:
// 1) Events evaluation such as "Determine closest vehicle around ego" for all child nodes
// 2) Utils services such as "Calculate lane occupation for all vehicles". for all child nodes
// 3) Integration of co simulators
class ServiceNode : public DecoratorNode {
 public:
  // The Service interface
  class Service {
   public:
    using Ptr = std::shared_ptr<ServiceNode::Service>;

    Service() = default;
    virtual ~Service() = default;

    // Inits & resets the service
    virtual void onInit(){};

    // Declare the managed symbols of the service
    virtual void declareSharedSymbols(BlackboardWriter& blackboard) = 0;

    // Look up required symbols
    virtual void lookUpSharedSymbols(const BlackboardReader& /*blackboard*/){};

    // Pre update method of the managed service, called before decorated child is ticked
    virtual void preUpdate(){};

    // Post update method of the managed service, called after decorated child was ticked
    virtual void postUpdate(){};
  };

  // Ctor for named node and multiple managed services
  ServiceNode(const std::string& name,
              const std::vector<ServiceNode::Service::Ptr> managed_services,
              Extension::Ptr extension_ptr = nullptr)
      : DecoratorNode(std::string("ServiceNode::").append(name), std::move(extension_ptr)),
        m_managed_services(std::move(managed_services)) {
    for (const auto& service : m_managed_services) {
      if (service == nullptr) {
        std::string error_msg = "Error in service decorator node [";
        error_msg.append(this->name()).append("]: Service is nullptr!");
        throw std::invalid_argument(error_msg);
      }
    }
  };

  // Convenience ctor for only one managed service
  ServiceNode(const std::string& name,
              const ServiceNode::Service::Ptr managed_service,
              Extension::Ptr extension_ptr = nullptr)
      : ServiceNode(name,
                    std::move(std::vector<ServiceNode::Service::Ptr>{std::move(managed_service)}),
                    std::move(extension_ptr)){};

  virtual ~ServiceNode() override = default;

  // Inits & resets all services and the underlying child
  void onInit() override {
    for (const auto& service : m_managed_services) {
      service->onInit();
    }
    DecoratorNode::onInit();
  };

 private:
  // Updates all services before executing the decorated child node
  NodeStatus tick() final {
    // Update services
    for (auto& service : m_managed_services) {
      service->preUpdate();
    }
    // Tick decorated child
    NodeStatus child_status = m_child_node->executeTick();

    // Update services
    for (auto& service : m_managed_services) {
      service->postUpdate();
    }
    return child_status;
  };

  // Insert all symbols of the services
  void declareSymbolsInBlackBoard(BlackboardWriter& blackboard) final {
    for (auto& service : m_managed_services) {
      service->declareSharedSymbols(blackboard);
    }
  };

  // LookUp all symbols of the services
  void lookUpSymbolsInBlackBoard(const BlackboardReader& blackboard) final {
    for (auto& service : m_managed_services) {
      service->lookUpSharedSymbols(blackboard);
    }
  };

  // The managed services
  std::vector<ServiceNode::Service::Ptr> m_managed_services;
};

}  // namespace yase

#endif  // AGNOSTIC_BEHAVIOR_TREE_DECORATOR_SERVICE_NODE_H
