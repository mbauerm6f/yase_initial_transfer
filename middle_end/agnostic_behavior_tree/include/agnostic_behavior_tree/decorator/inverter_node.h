/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef AGNOSTIC_BEHAVIOR_TREE_DECORATOR_INVERTER_NODE_H
#define AGNOSTIC_BEHAVIOR_TREE_DECORATOR_INVERTER_NODE_H

#include "agnostic_behavior_tree/decorator_node.h"

#include <exception>

namespace yase {
// The InverterNode inverts the execution state of the child behavior
class InverterNode : public DecoratorNode {
 public:
  InverterNode(Extension::Ptr extension_ptr = nullptr) : DecoratorNode("Inverter", std::move(extension_ptr)){};

  virtual ~InverterNode() override = default;

 private:
  NodeStatus tick() final {
    const NodeStatus child_state = m_child_node->executeTick();

    switch (child_state) {
      case NodeStatus::kSuccess: {
        return NodeStatus::kFailure;
      }
      case NodeStatus::kFailure: {
        return NodeStatus::kSuccess;
      }
      case NodeStatus::kRunning: {
        return NodeStatus::kRunning;
      }
      default: {
        std::string error_msg = "The child node [";
        error_msg.append(m_child_node->name());
        error_msg.append("] returned unkown NodeStatus.");
        throw std::invalid_argument(error_msg);
      }
    }
  }
};

}  // namespace yase

#endif  // AGNOSTIC_BEHAVIOR_TREE_DECORATOR_INVERTER_NODE_H
