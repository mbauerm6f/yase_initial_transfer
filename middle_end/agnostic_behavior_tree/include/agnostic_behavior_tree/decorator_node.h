/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef AGNOSTIC_BEHAVIOR_TREE_DECORATORNODE_H
#define AGNOSTIC_BEHAVIOR_TREE_DECORATORNODE_H

#include "agnostic_behavior_tree/behavior_node.h"

#include <string>

namespace yase {
// The abstract decorator node
//
// Decorators decorate ONE child behavior
class DecoratorNode : public BehaviorNode {
 public:
  // Ctor - it is mandatory to name the node
  explicit DecoratorNode(const std::string& name) : DecoratorNode(name, nullptr){};

  // Ctor with extension and name
  DecoratorNode(const std::string& name, Extension::Ptr extension_ptr)
      : BehaviorNode(std::string("Decorator::").append(name), std::move(extension_ptr)){};

  virtual ~DecoratorNode() override = default;

  using Ptr = std::shared_ptr<DecoratorNode>;

  // Set child behavior to decorate
  // Only one child is allowed & child must not be null!
  void setChild(BehaviorNode::Ptr child) {
    if (!child) {
      std::string error_msg = "DecoratorNode [";
      error_msg.append(name());
      error_msg.append("]: Added child behavior is NULL.");
      throw std::invalid_argument(error_msg);
    }
    if (m_child_node) {
      std::string error_msg = "Duplicate child insertion: DecoratorNode [";
      error_msg.append(name());
      error_msg.append("] has already a child assigned.");
      throw std::invalid_argument(error_msg);
    }
    child->m_parent_node = this;
    child->m_blackboard->setParentBlackboard(m_blackboard);
    m_child_node = child;
  };

  // Get decorated child
  const BehaviorNode::Ptr child() const { return m_child_node; };

  // Resolve all symbols locally and for decorated child
  void initTree() final {
    m_blackboard->clearLocal();
    declareSymbolsInBlackBoard(*(m_blackboard));
    lookUpSymbolsInBlackBoard(*(m_blackboard));
    if (m_child_node != nullptr) {
      m_child_node->initTree();
    }
  };

  void onInit() override {
    if (m_child_node != nullptr) {
      m_child_node->onInit();
    }
  };

  void onTerminate() override {
    if (m_child_node != nullptr) {
      m_child_node->onTerminate();
    }
  };

 protected:
  // Child behavior to decorate
  BehaviorNode::Ptr m_child_node{nullptr};
};

}  // namespace yase

#endif  // AGNOSTIC_BEHAVIOR_TREE_DECORATORNODE_H
