/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef AGNOSTIC_BEHAVIOR_TREE_SCOPED_BLACKBOARD_H
#define AGNOSTIC_BEHAVIOR_TREE_SCOPED_BLACKBOARD_H

#include <agnostic_behavior_tree/third_party/any.hpp>

#include <exception>
#include <iostream>
#include <memory>
#include <string>
#include <unordered_map>

namespace yase {
// The BlackboardBase
//
// This base class allows to examine symbols and scopes.
// It allows to check for specific types and allows to examine what symbols exist at which scope.
class BlackboardBase {
 public:
  using Ptr = typename std::shared_ptr<BlackboardBase>;

  // Map to remap keys
  // Structure: < key_to_proxy, key_for_sub_tree >
  using ProxyTable = typename std::unordered_map<std::string, std::string>;

  // Create new blackboard without parent
  explicit BlackboardBase(const std::string& name);

  // Create new blackboard without parent
  BlackboardBase(const std::string& name, BlackboardBase::Ptr parent);

  virtual ~BlackboardBase() = default;

  // Checks if key already exists
  bool exists(const std::string& key) const noexcept;

  // Searches key and checks if the stored shared pointer has type <T>
  template <typename T>
  bool exists(const std::string& key) const noexcept {
    return isType<T>(get(key));
  }

  // Prints all accessable symbols (with parent variables of parent scopes)
  void printAllAccessibleSymbols(std::string prefix = std::string()) const noexcept;

  // Prints local symbols and remapping
  void printLocalSymbols(std::string prefix = std::string()) const noexcept;

  // Name of the blackboard / scope
  const std::string scope_name{"Unnamed"};

 protected:
  // Internal LookUp method to find an any for the given key.
  // This is private, as the handling of "any" should be invisible from outside
  const linb::any* get(const std::string& key) const noexcept;

  // Check if any type is same as template type
  template <typename T>
  bool isType(const linb::any* symbol_any) const noexcept {
    if (symbol_any == nullptr) {
      return false;
    }
    try {
      linb::any_cast<T>(*symbol_any);
    } catch (...) {
      return false;
    }
    return true;
  }

  // Returns if a parent exists
  bool existsParentBlackboard() const noexcept;

  // Local variables
  std::unordered_map<std::string, linb::any> m_local_symbols;

  // Parent blackboard / scope
  BlackboardBase::Ptr m_parent_blackboard{nullptr};

  // Optional proxy table for remapping of symbol keys
  std::unique_ptr<ProxyTable> m_proxy_table{nullptr};
};

// The BlackboardReader allows to look up symbols
class BlackboardReader : virtual public BlackboardBase {
 public:
  BlackboardReader();

  // LookUp a symbol with key and given type - will throw if type or key wrong!
  template <typename T>
  T get(const std::string& key) const {
    const linb::any* symbol_any = BlackboardBase::get(key);
    if (symbol_any == nullptr) {
      std::string exception_msg = "Requested key [";
      exception_msg.append(key);
      exception_msg.append("] is not accessable in blackboard.");
      throw std::invalid_argument(exception_msg);
    }
    if (!isType<T>(symbol_any)) {
      std::string exception_msg = "Requested key [";
      exception_msg.append(key);
      exception_msg.append("] is of type [");
      exception_msg.append(symbol_any->type().name());
      exception_msg.append("] which differs to the requested type [");
      exception_msg.append(typeid(T).name());
      exception_msg.append("].");
      throw std::invalid_argument(exception_msg);
    }
    return linb::any_cast<T>(*symbol_any);
  }
};

// The BlackboardWriter allows to declare symbols
class BlackboardWriter : virtual public BlackboardBase {
 public:
  BlackboardWriter();

  // Declare a value
  template <typename T>
  void set(const std::string& key, T value) {
    if (exists(key)) {
      std::string exception_msg = "Key [";
      exception_msg.append(key);
      exception_msg.append("] cannot be declareed in blackboard, as it already exists.");
      throw std::runtime_error(exception_msg);
    }
    m_local_symbols.insert({std::string(key), linb::any{std::move(value)}});
  }
};

// The Blackboard allows to delcare and lookup symbols
class Blackboard : virtual public BlackboardReader, virtual public BlackboardWriter {
 public:
  using Ptr = typename std::shared_ptr<Blackboard>;

  Blackboard(const std::string& name);
  Blackboard(const std::string& name, BlackboardBase::Ptr parent);

  // Clear local symbols and proxy table
  void clearLocal();

  // Set parent blackboard - Will throw if already set or nullptr
  void setParentBlackboard(Blackboard::Ptr new_parent_blackboard);

  // Set Proxy table - Will throw if already set
  void setProxyTable(ProxyTable new_proxy_table);
};

}  // namespace yase

#endif  // AGNOSTIC_BEHAVIOR_TREE_SCOPED_BLACKBOARD_H
