# The Yase package

## About Yase:
The **YASE (Yet Agnostic Scenario Engine / Another Scenario Engine)** package is a simulator agnostic scenario engine.
It is agnostic of the underlying simulator as well as of the used scenario input file format. 

The architecture is hereby orientated on programming language compilers. Within these compilers, programming languages are translated into an intermediate representation by a Frontend. From here, several Backends can translate the intermediate representation into different target architectures.
!["Example of LLVM compiler"](doc/figures/compiler_example.png?raw=true)

This modular and flexible way is used within YASE. It contains an simulator and scenario file format agnostic Middleend. This Middleend can be filled by different Frontends, reading in different scenario formats. On the other side, different Backends can connect this with different simulators.
The following figure shows a **potential** expansion with potential simulation backends/ scenario formats frontends.
!["Example for a potential YASE setup"](doc/figures/scenario_compiler.png?raw=true)


## Current YASE content:

The current YASE project consist of the MiddleEnd packages:

**MiddleEnd**
* The **agnostic_behavior_tree** package with an agnostic behavior tree implementation. It allows to build up any scenario behavior with the help self implemented actions.
* The **agnostic_type_system** package with an a type system, which can be adapted to any simulator.
